<?php
require_once "/home1/checkly1/public_html/library/cls.mysqli_database.php";
require_once "/home1/checkly1/public_html/library/cls.account.php";

/**
 * @author Cone Code Development
 * @version 1.2
 *
 * @property string contains sql filter string
 */
class Account_Admin extends Account
{
	private $_filter;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function __construct()
	{
		parent::__construct();
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * confirm the account has admin status
	 *
	 * @return boolean
	 * @access public
	 */
	function confirmAdmin()
	{
		if (!parent::confirmSession()) {
			return false;
		}
		if ($_SESSION["account_status"] < 2) {
			return false;
		}

		return true;
	}

	// SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * sets the filter property to an sql "where" string
	 *
	 * @param string $filter
	 * @return string
	 * @access public
	 */
	function setFilter($filter = "")
	{
		if ($filter == "") {
			$this->_filter = "";
		} else {
			$tmp  = "WHERE account_username LIKE '%" . $filter . "%'";
			$tmp .= " OR account_email LIKE '%" . $filter . "%'";
			$tmp .= " OR display_name LIKE '%" . $filter . "%'";
			$this->_filter = $tmp;
		}
		
		return $this->_filter;
	}

	/**
	 * get multiple records
	 *
	 * @param string $sort order by field
	 * @param string $direct DESC or ASC
	 * @return array
	 * @access public
	 */
	function getList($sort = "account_id", $direct = "ASC", $page = 0)
	{
		$order = "ORDER BY ". $sort . " " . $direct;

		if ($page <> 0) {
			$start = ($page * 20) - 20;
			$limit = "LIMIT " . $start . ", 20";
		} else {
			$limit = "";
		}

		$sql = "SELECT
					*
				FROM
					accounts
				" . $this->_filter . "
				" . $order . "
				" . $limit;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]["last_login_dt"] = strtotime($row["last_login_dt"]);
			$return[$i]["deleted_dt"] = strtotime($row["deleted_dt"]);
			$return[$i]["created_dt"] = strtotime($row["created_dt"]);
			$return[$i]["modified_dt"] = strtotime($row["modified_dt"]);
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * count the number of accounts
	 *
	 * @param boolean $active true returns only active accounts
	 * @return integer
	 * @@access public
	 */
	function getCount($active = true)
	{
		if ($active) {
			$filter = "WHERE account_status > 0";
		} else {
			$filter = $this->_filter;
		}

		$sql = "SELECT
					COUNT(account_id) as account_count
				FROM
					accounts
				" . $filter;

		$stmt = $this->mysqli->prepare($sql);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_result($count);
		$stmt->fetch();
		$stmt->close();

		return $count;
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update the status of an account
	 *
	 * @param integer $accountId
	 * @param integer $status
	 * @return boolean
	 * @access public
	 */
	function updateStatus($accountId, $status)
	{
		$sql = "UPDATE
					accounts
				SET
					account_status = " . (int)$status;

		if ($status == 0) {
			$sql .= ", deleted_dt = (NOW())";
		} else {
			$sql .= ", deleted_dt = NULL, modified_dt = (NOW())";
		}

		$sql .= " WHERE account_id = " . $accountId;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// if you change your own status, update the session
		if ($_SESSION["account_id"] == $accountId) {
			$_SESSION["account_status"] = $status;
		}

		return true;
	}
}