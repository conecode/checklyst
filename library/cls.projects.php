<?php
require_once '/home1/checkly1/public_html/library/cls.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class Projects
{
	/**
	 * property contains the database object
	 * @var object
	 */
	var $mysqli;

	/**
	 * property contains the account identifier
	 * @var integer
	 */
	var $account_id;

	/**
	 * property contains the project identifier
	 * @var integer
	 */
	var $project_id;

	/**
	 * property contains the users permission for the specified project
	 * @var string
	 */
	var $permission = 'NONE';

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 *
	 * @param integer $account_id
	 */
	function __construct($account_id = 0)
	{
		$this->mysqli = new Mysqli_Database();
		$this->account_id = (int)$account_id;
	}

	// PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * verify an account has permission to access a project
	 *
	 * @return boolean
	 */
	private function _verifyPermission()
	{
		// set the default
		$this->permission = 'NONE';

		// query the database
		$sql = "SELECT
					permission
				FROM
					project_access
				WHERE
					project_id = ?
				AND
					account_id = ?
				LIMIT 1";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param("ii", $this->project_id, $this->account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->store_result();

		// check if any permission results were returned
		if ($stmt->num_rows == 0) {
			catchErr("Access to this project is denied.");
			return false;
		}

		$stmt->bind_result($access);
		$stmt->fetch();
		$stmt->close();

		// check if the permission value exists
		if (empty($access)) {
			catchErr("Access to this project is denied.");
			return false;
		}

		// set the permission
		$this->permission = $access;

		return $access;
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function setAccount($account_id)
	{
		$this->account_id = (int)$account_id;
		if (!empty($this->project)) {
			$this->_verifyPermission();
		} else {
			$this->permission = 'NONE';
		}
	}

	function setProject($project_id)
	{
		$this->project_id = (int)$project_id;
		$this->_verifyPermission();
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 * 
	 * @param string $project_name
	 * @return integer
	 * @access public
	 */
	function create($project_name)
	{
		// create a new project in the database
		$sql = "INSERT INTO projects (
					project_name,
					owner_id,
					number,
					created_dt,
					modified_dt
				) VALUES (
					?,
					?,
					CONCAT('W', owner_id, '.', UNIX_TIMESTAMP(NOW())),
					NOW(),
					NOW()
				)";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('si', $project_name, $this->account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		// get the new id and set permission to ADMIN to the project
		$this->project_id = mysqli_insert_id($this->mysqli);
		$this->addPermission('ADMIN');

		return $this->project_id;
	}

	/**
	 * add permission for an account to access a project
	 *
	 * @param string $permission [READ, WRITE, ADMIN]
	 * @return boolean
	 * @access public
	 */
	function addPermission($permission, $project_id = 0)
	{
		if (empty($project_id)) {
			$project_id = $this->project_id;
		}

		$sql = "INSERT INTO project_access (
					project_id,
					account_id,
					permission
				) values (
					?,
					?,
					?
				)";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('iis', $project_id, $this->account_id, $permission);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		$this->permission = $permission;
		return true;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record
	 *
	 * @return array
	 * @access public
	 */
	function get()
	{
		return $this->mysqli->row_get('projects', 'project_id', $this->project_id);
	}

	/**
	 * get information from an array created by the read function
	 *
	 * @param array $projects
	 * @return array
	 * @access public
	 */
	function getFromArray(&$projects) {
		$return = array();
		foreach($projects as $project) {
			if ($project['project_id'] == $this->project_id) {
				$return = $project;
			}
		}
		return $return;
	}

	/**
	 * get the id for the project that was last accessed by this account
	 *
	 * @return array
	 */
	function getLastAccess()
	{
		$sql = "SELECT
					project_id
				FROM
					projects
				INNER JOIN
					project_access USING(project_id)
				WHERE
					deleted = 0
				AND
					account_id = $this->account_id
				ORDER BY
					access_dt DESC
				LIMIT 1";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		if ($result->num_rows > 0) {
			$row = $this->mysqli->compose($result);
		} else {
			return false;
		}
		$result->close();

		return $row['project_id'];
	}

	/**
	 * read multiple records
	 *
	 * @param string $order order by field
	 * @param string $filter ALL, DELETED, ACTIVE, or ADMIN
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function read($order = 'project_name', $filter = 'ACTIVE', $since_dt = '')
	{
		switch ($filter) {
			case 'ALL':
				$field = "deleted, deleted_dt,";
				$where = "";
				break;
			case 'DELETED':
				$field = "deleted, deleted_dt,";
				$where = "AND deleted = 1";
				break;
			case 'ADMIN':
				$field = "";
				$where = "AND permission = 'ADMIN' AND deleted = 0";
				break;
			default:
				$field = "";
				$where = "AND deleted = 0";
		}
		
		if ($since_dt != '') {
			$where .= " AND UNIX_TIMESTAMP(modified_dt) > " . $since_dt;
		}

		$sql = "SELECT
					{$field}
					project_id,
					project_name,
					owner_id,
					created_dt,
					modified_dt,
					permission,
					access_dt
				FROM
					projects
				INNER JOIN
					project_access USING (project_id)
				WHERE
					account_id = {$this->account_id}
				AND
					permission IS NOT NULL
					{$where}
				ORDER BY
					{$order}";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]['project_name'] = stripslashes($row['project_name']);
			if (!empty($row['deleted_dt'])) {
				$return[$i]['deleted_dt'] = strtotime($row['deleted_dt']);
			}
			$return[$i]['created_dt'] = strtotime($row['created_dt']);
			$return[$i]['modified_dt'] = strtotime($row['modified_dt']);
			++$i;
		}
		$result->close();

		return $return;
	}

    /**
     * gets a count of the number of items completed and remaining
     *
	 * @param integer $project_id project identifier
     * @return array (total, remaining, completed)
     * @access public
     */
    function itemsCompleted($project_id) {

        // query database
        $sql = "SELECT
					strike,
					COUNT(*) AS number
				FROM
					project_items
				WHERE
					project_id = {$project_id}
				GROUP BY
					strike";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

		$return = array('total' => 0, 'remaining' => 0, 'completed' => 0);
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			if ($row['strike'] == 0) {
				$return['remaining'] = $row['number'];
			} else {
				$return['completed'] = $row['number'];
			}
			++$i;
		}
		$result->close();
		
		$return['total'] = $return['remaining'] + $return['completed'];

		return $return;
    }

	/**
	 * count the total number of records
	 *
	 * @return integer
	 * @access public
	 */
	function total()
	{
		return $this->mysqli->row_count('projects', 'project_id');
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing record
	 *
	 * @param string $project_name
	 * @return boolean
	 * @access public
	 */
	function update($project_name)
	{
		// verify permission to update a project name
		if ($this->permission != 'ADMIN') {
			catchErr("Access to modify this project is denied.");
			return false;
		}

		$sql = "UPDATE
					projects
				SET 
					project_name = ?,
					modified_dt = NOW()
				WHERE
					project_id = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
        }
		$stmt->bind_param('si', $project_name, $this->project_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * sets the date and time a project was last accessed by and account
	 *
	 * @return boolean
	 */
	function accessTimeStamp()
	{
		$sql = "UPDATE
					project_access
				SET
					access_dt = NOW()
				WHERE
					project_id = ?
				AND
					account_id = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
        }
		$stmt->bind_param('ii', $this->project_id, $this->account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * toggle on or off the deleted flag on a record
	 *
	 * @param boolean $toggle
	 * @return boolean
	 * @access public
	 */
	function flag($toggle = true)
	{
		// verify permission to update a project name
		if ($this->permission != 'ADMIN') {
			catchErr("Access to modify this project is denied.");
			return false;
		}

		if ($toggle) {
			$value = 1;
			$date  = 'NOW()';
		} else {
			$value = 0;
			$date  = 'NULL';
		}

		$sql = "UPDATE
					projects
				SET
					deleted = {$value},
					deleted_dt = {$date}
				WHERE
					project_id = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('i', $this->project_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	// DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * permanently removes a project, permissions, and lists
	 *
	 * @return integer
	 * @access public
	 */
	function remove()
	{
		// verify permission to update a project name
		if ($this->permission != 'ADMIN') {
			catchErr("Access to modify this project is denied.");
			return false;
		}

		// delete the project, permissions, and list items
		if (!$this->mysqli->row_delete('projects', 'project_id', $this->project_id)) {
			catchErr("Unable to remove the project.");
			return false;
		}
		if (!$this->mysqli->row_delete('project_access', 'project_id', $this->project_id)) {
			catchErr("Unable to remove the project permissions.");
			return false;
		}
		if (!$this->mysqli->row_delete('project_items', 'project_id', $this->project_id)) {
			catchErr("Unable to remove the project lists.");
			return false;
		}

		return $this->project_id;
	}

	/**
	 * removes all projects flagged as deleted
	 *
	 * @return string
	 * @access public
	 */
	function emptyArchive()
	{
		$projects = $this->read('deleted_dt', 'DELETED');
		$return   = '';

		foreach ($projects as $project) {
			$this->setProject($project['project_id']);
			if (!$this->remove()) {
				catchErr("Project name: " . $project['project_name']);
				return false;
			}
			$return .= $project['project_id'] . ',';
		}

		return substr($return, 0, -1);
	}
}