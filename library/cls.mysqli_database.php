<?php

/**
 * Creates a mysqli connection using the variables set in the config file
 *
 * @author Cone Code Development
 * @version 3.0
 */
class Mysqli_Database extends mysqli
{
	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	public function __construct()
	{
		// create a connection between PHP and a MySQL database
		parent::__construct(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

		// check if the connection was not successful
		if (mysqli_connect_error()) {

			if (DISPLAY_ERRORS) {

				// developer setting - display the actual error
				die("<p>Connection error.</p>");
			} else {

				// live web site setting - display an error page
				header("Location: " . SITE_ROOT . "/error");
			}
		}
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * escape the values in an array or string
	 *
	 * @param mixed $args string or array
	 * @return mixed
	 * @access public
	 */
	function escape(&$args)
	{
		if (is_array($args)) {
			foreach ($args as $key => $value) {
				$args[$key] = $this->real_escape_string($value);
			}
		} else {
			$args = $this->real_escape_string($args);
		}
		return $args;
	}

	/**
	 * strip slashes from strings and convert date strings to a Unix timestamp
	 *
	 * @param query $result
	 * @return array
	 */
	function compose(&$result)
	{
		$row = $result->fetch_assoc();
		while ($finfo = $result->fetch_field()) {
			switch ($finfo->type) {
				case 10:
				case 12:
					if ($row[$finfo->name] == '0000-00-00 00:00:00' || empty($row[$finfo->name])) {
						$row[$finfo->name] = false;
					} else {
						$row[$finfo->name] = strtotime($row[$finfo->name]);
					}
					break;
				case 252:
				case 253:
					$row[$finfo->name] = stripslashes($row[$finfo->name]);
					break;
			}
		}
		return $row;
	}

	// alias for the function row_get.
	// used for compatibilty with older versions of this class
	function one($t, $c, $v) { return $this->row_get($t, $c, $v); }

	/**
	 * get one row from a table
	 *
	 * @param string $table
	 * @param string $column
	 * @param mixed $value
	 * @return array
	 * @access public
	 */
	function row_get($table, $column, $value)
	{
		$sql = "SELECT
					*
				FROM
					$table
				WHERE
					$column = $value
				LIMIT 1";

		$result = $this->query($sql);
		if (!$result) {
			catchSQL($sql, $this);
			return false;
		}

		if ($result->num_rows > 0) {
			$row = $this->compose($result);
		} else {
			return false;
		}
		$result->close();

		return $row;
	}

	/**
	 * count the number of rows in a table
	 *
	 * @param string $table
	 * @param string $column
	 * @return integer
	 * @@access public
	 */
	function row_count($table, $column)
	{
		$sql = "SELECT
					COUNT($column) as record_count
				FROM
					$table";

		if (!$stmt = $this->prepare($sql)) {
			catchSQL($sql, $this);
			return false;
        }
		if (!$stmt->execute()) {
			catchSQL($sql, $this);
			return false;
		}
		$stmt->bind_result($count);
		$stmt->fetch();
		$stmt->close();

		return $count;
	}

	/**
	 * count the number of rows in a table
	 *
	 * @param string $table
	 * @param string $column
	 * @return integer
	 * @@access public
	 */
	function row_count_where($table, $column, $where)
	{
		$sql = "SELECT
					COUNT($column) as record_count
				FROM
					$table
				WHERE
					$column = $where";

		if (!$stmt = $this->prepare($sql)) {
			catchSQL($sql, $this);
			return false;
        }
		if (!$stmt->execute()) {
			catchSQL($sql, $this);
			return false;
		}
		$stmt->bind_result($count);
		$stmt->fetch();
		$stmt->close();

		return $count;
	}

	/**
	 * delete a row from a table using the row id
	 *
	 * @param string $table
	 * @param string $column
	 * @param integer $id
	 * @return boolean
	 * @access public
	 */
	function row_delete($table, $column, $id)
	{
		$sql = "DELETE FROM
					$table
				WHERE
					$column = ?";

		if (!$stmt = $this->prepare($sql)) {
			catchSQL($sql, $this);
			return false;
        }
		$stmt->bind_param('i', $id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this);
			return false;
		}
		$stmt->close();

		return true;
	}
}