<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.apns.php');
require_once('library/fnc.handlers.php');

$args=$_GET;

$apns = new APNS($args['deviceuid']);

if ($apns->exists()) {
	$apns->update($args);
	echo "Device Updated";
} else {
	$apns->create($args);
	echo "Device Added";
}
?>
