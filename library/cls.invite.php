<?php
require_once '/home1/checkly1/public_html/library/cls.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class Invite
{
	/**
	 * property contains the database object
	 * @private object
	 */
	private $_mysqli;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function __construct()
	{
		$this->_mysqli = new Mysqli_Database();
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 *
	 * @param string $name
	 * @param string $email
	 * @param string $code
	 * @param string $permissions
	 * @return integer
	 * @access public
	 */
	function create($name, $email, $code, $permissions, $account_id)
	{
		$sql = "INSERT INTO invites (
					name,
					email,
					code,
					permissions,
					created_by,
					invite_dt
				) values (
					?,
					?,
					?,
					?,
					?,
					NOW()
				)";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->bind_param('ssssi', $name, $email, $code, $permissions, $account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->close();

		// get the new id
		$invite_id = mysqli_insert_id($this->_mysqli);

		return $invite_id;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record by code
	 *
	 * @param string $code
	 * @return array
	 * @access public
	 */
	function get($code)
	{
		$this->_mysqli->escape($code);
		return $this->_mysqli->row_get('invites', 'code', "'$code'");
	}

	/**
	 * get a single record by id
	 *
	 * @param integer $invite_id
	 * @return array
	 * @access public
	 */
	function getById($invite_id)
	{
		return $this->_mysqli->row_get('invites', 'invite_id', (int)$invite_id);
	}

	/**
	 * read multiple records created by a specified account
	 *
	 * @param integer $account_id
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function read($account_id, $since_dt = '')
	{
		if ($since_dt != '') {
			$where = " AND UNIX_TIMESTAMP(invite_dt) > " . $since_dt;
		} else {
			$where = '';
		}
		
		$sql = "SELECT
					invite_id,
					name,
					email,
					code,
					permissions,
					invite_dt
				FROM
					invites
				WHERE
					created_by = " . (int)$account_id . $where;

		$result = $this->_mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]["invite_dt"] = strtotime($row["invite_dt"]);
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * count the total number of records
	 *
	 * @return integer
	 * @access public
	 */
	function total()
	{
		return $this->_mysqli->row_count('invites', 'invite_id');
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing record
	 *
	 * @param integer $invite_id
	 * @param string $permissions
	 * @return boolean
	 * @access public
	 */
	function update($invite_id, $permissions)
	{
		$sql = "UPDATE
					invites
				SET
					permissions = ?
				WHERE
					invite_id = ?";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this->_mysqli);
			return false;
        }
		$stmt->bind_param('si', $permissions, $invite_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	// DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * permanently removes a record
	 *
	 * @param integer $invite_id
	 * @return boolean
	 * @access public
	 */
	function remove($invite_id)
	{
		return $this->_mysqli->row_delete('invites', 'invite_id', (int)$invite_id);
	}
}