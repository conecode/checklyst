<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projectlist.php');

// prevent direct access
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header('Location: /');
}

// get the specified ajax action
$action = getValue('act');

// verify user is logged in
$account = new Account();
if (!$account->confirmSession()) {
	catchErr('Could not acquire account information. <a href="/">Login</a>');
	$action = 'err';
}

// get the account and project ids
$account_id = (int)$_SESSION['account_id'];
$project_id = (int)getValue('prj', 0);
$project = new ProjectList($account_id, $project_id);

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * run the commands needed to create or update a project
 *
 * @global ProjectList $project
 * @param array $post
 * @param string $action ['CREATE' or 'UPDATE']
 * @return string
 */
function ajxSaveProject($post, $action)
{
	global $project;

	// check if the name is valid
	$name = getValue('nam', '', $post);
	if (empty($name)) {
		catchErr('The project name cannot be empty');
		return false;
	}
	// fix any html entities
	$name = cleanString($name);

	if ($action == 'CREATE') {
		// create a new project in the database
		if (!$project->create($name)) {
			return false;
		}
		$project_id = getValue('cur', 0);
		if ($project_id == 0) {
			$project->accessTimeStamp();
		}
	} else {
		// verify the project id is valid
		if ($project->project_id == 0) {
			catchErr('A project identifier could not be acquired.');
			return false;
		}

		// update the database
		if (!$project->update($name)) {
			return false;
		}
		$project_id = $project->project_id;
	}

	// get the new results from the database
	$projectlist = $project->read();
	if (!$projectlist) {
		return false;
	}

	// get the html to be inserted into the page
	ob_start();
	showProjects($projectlist, $project_id);
	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

/**
 * run the command needed to switch the deleted flag
 *
 * @global ProjectList $project
 * @return string
 */
function ajxFlagProject()
{
	global $project;

	// verify the project id is valid
	if ($project->project_id == 0) {
		catchErr('A project identifier could not be acquired.');
		return false;
	}

	// update the database
	if (!$project->flag(true)) {
		return false;
	}

	return $project->project_id;
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($action) {
	case 'cp': // create a new project
		$rslt = ajxSaveProject($_POST, 'CREATE');
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'up': // update a project
		$rslt = ajxSaveProject($_POST, 'UPDATE');
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'tp': // toggle a project flag
		$rslt = ajxFlagProject();
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'err':
		echo writeErrors();
		break;
	default:
		catchErr('An action could not be acquired.');
		echo writeErrors();
}
