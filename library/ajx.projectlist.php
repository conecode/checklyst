<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projectlist.php');

// prevent direct access
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header('Location: /');
}

// get the specified ajax action
$action = getValue('act');

// verify user is logged in
$account = new Account();
if (!$account->confirmSession()) {
	catchErr('Could not acquire account information. <a href="/">Login</a>');
	$action = 'err';
}

// get the account and project ids
$account_id = (int)$_SESSION['account_id'];
$project_id = (int)getValue('prj', 0);

// verify the project id is valid
if ($project_id == 0) {
	catchErr('A project identifier could not be acquired.');
	$action = 'err';
}

$project = new ProjectList($account_id, $project_id);

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * run the commands needed to create or update a list item
 *
 * @global ProjectList $project
 * @param array $post
 * @param string $action
 * @return string
 */
function ajxSaveItem(&$post, $action)
{
	global $project;

	// get the item values
	$args['item_text']   = cleanString(getValue('txt', '', $post));
	$args['assigned_to'] = getValue('ass', '', $post);
	$duedate = trim(getValue('due', '', $post));
	if (strtoupper($duedate) == 'NONE' || empty($duedate)) {
		$args['due_dt'] = '';
	} else {
		$tmp_time = strtotime($duedate);
		if ($tmp_time) {
			$args['due_dt'] = date('Y-m-d', strtotime($duedate));
		} else {
			$args['due_dt'] = '';
		}
	}

	// verify the item text is valid
	if (empty($args['item_text'])) {
		catchErr('The task item text cannot be empty');
		return false;
	}

	if ($action == 'CREATE') {
		// create a new item in the database
		$item_id = $project->createListItem($args);
		if (!$item_id) {
			return false;
		}
		$include_tr = true;
	} else {
		// verify the item id is valid
		$item_id = (int)getValue('itm', 0, $post);
		if ($item_id == 0) {
			catchErr('A list item identifier could not be acquired.');
			return false;
		}

		// update the database
		if (!$project->updateListItem($item_id, $args)) {
			return false;
		}
		$include_tr = false;
	}

	$item[0] = $project->getListItem($item_id);

	// get the html to be inserted into the page
	ob_start();
	showListItems($item, $project->permission, $include_tr);
	$html = ob_get_contents();
	ob_end_clean();

	if ($include_tr) {
		return json_encode(
			array('id'=>$item_id,
				  'pr'=>$item[0]['item_priority'],
				  'ht'=>$html));
	} else {
		return $html;
	}
}

/**
 * run the command needed to remove a list item
 *
 * @global ProjectList $project
 * @param integer $item_id
 * @return integer
 */
function ajxRemoveItem($item_id)
{
	global $project;

	// verify the item id is valid
	if (empty($item_id)) {
		catchErr('A list item identifier could not be acquired.');
		return false;
	}

	// update the database
	if (!$project->removeListItem($item_id)) {
		return false;
	}

	return $item_id;
}

/**
 * run the command needed to flag a list item as completed
 *
 * @global ProjectList $project
 * @param integer $item_id
 * @param boolean $strike
 * @return integer
 */
function ajxStrikeItem($item_id, $strike)
{
	global $project;

	// verify the item id is valid
	if (empty($item_id)) {
		catchErr('A list item identifier could not be acquired.');
		return false;
	}

	// verify a strike value is set
	if ($strike == '') {
		catchErr('A complete status could not be acquired.');
		return false;
	} else {
		$strike = ($strike == 1) ? true : false;
	}

	// update the database
	if (!$project->strikeListItem($item_id, $strike)) {
		return false;
	}

	return $item_id;
}

/**
 * run the command needed to move a list items priority
 *
 * @global ProjectList $project
 * @param array $post
 * @return string
 */
function ajxMoveItem($post)
{
	global $project;

	// verify the item id is valid
	$item_id = (int)getValue('itm', 0, $post);
	if ($item_id == 0) {
		catchErr('A list item identifier could not be acquired.');
		return false;
	}

	// verify a strike value is set
	$old = (int)getValue('old', 0, $post);
	if ($old == 0) {
		catchErr('The original position of the item could not be acquired.');
		return false;
	}

	$new = (int)getValue('new', 0, $post);
	if ($new == 0) {
		catchErr('The new position of the item could not be acquired.');
		return false;
	}

	// update the database
	if (!$project->updatePriority($item_id, $old, $new)) {
		return false;
	}

	// get the new priority list
	$priorities = $project->readPriorities();

	return $priorities;
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($action) {
	case 'ci': // create a new list item
		$rslt = ajxSaveItem($_POST, 'CREATE');
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'ri': // remove a list item
		$rslt = ajxRemoveItem(getValue('itm'));
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'ui': // update a list item
		$rslt = ajxSaveItem($_POST, 'UPDATE');
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'si': // strike or restore a list item
		$rslt = ajxStrikeItem(getValue('itm'), getValue('set'));
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'mi': // move a list item
		$rslt = ajxMoveItem($_POST);
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'err':
		echo writeErrors();
		break;
	default:
		catchErr('An action could not be acquired.');
		echo writeErrors();
}