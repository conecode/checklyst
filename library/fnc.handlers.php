<?php
/**
 * Collection of error and variable functions
 *
 * @author Cone Code Development
 * @version 2.1
 */

/**
 * catch MySQL errors (display to developers)
 *
 * @param string $sql query string
 * @param $mysqli php mysqli object
 * @access public
 */
function catchSQL($sql, $mysqli) {
	$err = $mysqli->error;
//	echo $sql;
	catchErr("SQL Error: ".$err);

	if (!$mysqli->ping()) {
		$msg = "<p>Connection with database has been lost.</p>";
	} else {
		$msg = "<p>" . $err . "<br />" . $sql . "</p>";
	}

	if (DISPLAY_ERRORS) {
		echo $msg;
	}
}

/**
 * catch page error (display to users)
 *
 * @param string $msg message to be sent to user
 * @access public
 */
function catchErr($msg) {
    global $ERRORS;
    array_push($ERRORS, $msg);
}

/**
 * displays error messages (display to users)
 *
 * @access public
 */
function writeErrors(){
	global $ERRORS;

	if(count($ERRORS)) {

		$msg = "<div id=\"errors\"><strong>Error:</strong><ul>";

		foreach ($ERRORS as &$value) {
			$msg .= '<li>' . $value . '</li>';
		}
		$msg .= "</ul><span onclick=\"this.parentNode.style.display = 'none'\";'>" .
					"close</span></div>\n";
		echo $msg;
	}
}

function mobileErrors(){
	global $ERRORS;

	if(count($ERRORS)) {

		$msg = "Error: ";

		foreach ($ERRORS as &$value) {
			$msg .= $value;
		}
		echo $msg;
	}
}

/**
 * verify a variable is defined
 *
 * @param string $key
 * @param string $default
 * @param mixed  $array
 * @return mixed
 * @access public
 */
function getValue($key, $default = "", &$array = "REQUEST")
{
	if ($array == "REQUEST") {
		return (isset($_REQUEST[$key])) ? $_REQUEST[$key] : $default;
	} elseif ($array == "SESSION") {
		return (isset($_SESSION[$key])) ? $_SESSION[$key] : $default;
	} elseif (is_array($array)) {
		return (isset($array[$key])) ? $array[$key] : $default;
	} else {
		return $default;
	}
}

/**
 * removes white space and converts characters to HTML entities
 *
 * @param string $text
 * @return string
 * @access public
 */
function cleanString($text)
{
	$text = nl2br(htmlentities(trim($text), ENT_QUOTES));
	return preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.-]*(\?\S+)?)?)?)@',
			'<a href="$1">$1</a>', $text);
}