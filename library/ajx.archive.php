<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projectlist.php');

// prevent direct access
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header('Location: /');
}

// get the specified ajax action
$action = getValue('act');

// verify user is logged in
$account = new Account();
if (!$account->confirmSession()) {
	catchErr('Could not acquire account information. <a href="/">Login</a>');
	$action = 'err';
}

// get the account and project ids
$account_id = (int)$_SESSION['account_id'];
$project_id = (int)getValue('prj', 0);
$project = new ProjectList($account_id, $project_id);

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * get the dialog box HTML
 *
 * @global Projects $project
 * @return string
 */
function ajxGetDialogBox()
{
	global $project;
	// get a list of projects for the current account
	$projects = $project->read('deleted_dt', 'DELETED');

	ob_start();
?>
	<div id="dia-head">
		<p id="dia-head-title">Trash</p>
		<a href="javascript:dialog.hide()" id="dia-close"></a>
		<div id="dia-head-cap"></div>
	</div>
	<div id="dia-body">
		<div id="dia-body-fill">
<?php
	if (sizeOf($projects) == 0) { ?>
			<p style="padding: 30px;text-align: center;">Trash is currently empty.</p>
<?php
	} else { ?>
			<table id="trash">
				<colgroup>
					<col><col class="col_date"><col class="col_btns">
				</colgroup>
				<thead>
					<tr><th>Name</th><th>Deleted</th><th>Action</th></tr>
				</thead>
				<tbody id="trashBody">
<?php	foreach ($projects as $thisProj) {
				$id = $thisProj['project_id'];?>
					<tr id="trsh<?php echo $id ?>">
						<td><p><?php echo $thisProj['project_name'] ?></p></td>
						<td><p><?php echo date('M d, Y g:i A', $thisProj['deleted_dt']) ?></p></td>
						<td>
<?php		if ($thisProj['permission'] == 'ADMIN') { ?>
							<button class="button" onclick="trash.restore(<?php echo $id ?>)"><span class="action-l"><span class="action-r">Restore</span></span></button>
							<button class="button" onclick="trash.remove(<?php echo $id ?>)"><span class="action-l"><span class="action-r">Delete</span></span></button>
<?php		} ?>
						</td>
					</tr>
<?php	} ?>
				</tbody>
			</table>
			<div class="btn_spacer">
				<button class="button" onclick="trash.remove('ALL')"><span class="default-l"><span class="default-r">Empty Trash</span></span></button>
			</div>
<?php
	} ?>
		</div>
	</div><!-- #dia-body -->
	<div id="dia-foot">
		<div id="dia-foot-cap"></div>
	</div>

<?php
	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

/**
 * run the command needed to switch the deleted flag
 *
 * @global ProjectList $project
 * @param id $current the project identifier for the currently selected project
 * @return string
 */
function ajxFlagProject($current)
{
	global $project;

	// verify the project id is valid
	if ($project->project_id == 0) {
		catchErr('A project identifier could not be acquired.');
		return false;
	}

	// update the database
	if (!$project->flag(false)) {
		return false;
	}

	// get the new results from the database
	$projectlist = $project->read();
	if (!$projectlist) {
		return false;
	}

	// get the html to be inserted into the page
	ob_start();
	showProjects($projectlist, $current);
	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

/**
 * run the command to permanently removes a project and it's list items
 *
 * @global ProjectList $project
 * @return mixed
 */
function ajxDeleteProject($sum = 'ONE')
{
	global $project;

	// verify the project id is valid
	if ($sum == 'ONE') {
		if ($project->project_id == 0) {
			catchErr('A project identifier could not be acquired.');
			return false;
		}
		$return = $project->remove();
	} else {
		$return = $project->emptyArchive();
	}

	return $return;
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($action) {
	case 'gt': // get the dialog box
		$rslt = ajxGetDialogBox();
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'tp': // toggle a project flag
		$rslt = ajxFlagProject(getValue('cur', 0));
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'op': // deletes a project
		$rslt = ajxDeleteProject(getValue('sum'));
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'err':
		echo writeErrors();
		break;
	default:
		catchErr('An action could not be acquired.');
		echo writeErrors();
}
?>