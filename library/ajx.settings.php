<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/fnc.validate.php');

// prevent direct access
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header('Location: /');
}

// get the specified ajax action
$action = getValue('act');

// verify user is logged in
$account = new Account();
if (!$account->confirmSession()) {
	catchErr('Could not acquire account information. <a href="/">Login</a>');
	$action = 'err';
}

// get the account id
$account_id = (int)$_SESSION['account_id'];

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * get the dialog box HTML
 *
 * @global Account $account
 * @return string
 */
function ajxGetDialogBox($post)
{
	global $account;

	$user = $account->get();

	ob_start();
?>
	<div id="dia-head">
		<p id="dia-head-title">Settings</p>
		<a href="javascript:dialog.hide()" id="dia-close"></a>
		<div id="dia-head-cap"></div>
	</div>
	<div id="dia-body">
		<div id="dia-body-fill">
			<div id="dia-tabbar">
				<a id="tab-1" class="selected" href="javascript:settings.form(1)"><span>General</span></a>
				<a id="tab-2" href="javascript:settings.form(2)"><span>Password</span></a>
			</div>
			<form id="set-1" action="/account/edit.php" method="post">
				<div class="inputgrp">
					<label class="inputlbl" for="username">Username:</label>
					<input class="input" id="username" maxlength="50" name="username" type="text" value="<?php echo getValue('account_username', '', $user) ?>" />
					<p>Your username can't have any spaces and must be unique.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="email">Email Address:</label>
					<input class="input" id="email" maxlength="50" name="email" type="text" value="<?php echo getValue('account_email', '', $user) ?>" />
					<p>Please use a real email address to confirm your account.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="your_name">Your Name:</label>
					<input class="input" id="your_name" maxlength="50" name="your_name" type="text" value="<?php echo getValue('display_name', '', $user) ?>" />
					<p>Your name will only be seen by your friends.</p>
				</div>
				<div class="buttongrp">
					<input name="formtype" type="hidden" value="Account" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Save Changes</span></span></button>
				</div>
			</form>
			<form style="display:none" id="set-2" action="/account/edit.php" method="post">
				<div class="inputgrp">
					<label class="inputlbl" for="password">Password:</label>
					<input class="input" id="password" name="password" maxlength="32" type="password" />
					<p>Your password must have a minimum of 6 characters.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="confirm">Confirm Password:</label>
					<input class="input" id="confirm" name="confirm" maxlength="32" type="password" />
					<p>Re-type your password to verify there were no mistakes.</p>
				</div>
				<div class="buttongrp">
					<input name="formtype" type="hidden" value="Password" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Save Password</span></span></button>
				</div>
			</form>
		</div>
	</div><!-- #dia-body -->
	<div id="dia-foot">
		<div id="dia-foot-cap"></div>
	</div>
<?php
	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($action) {
	case 'get': // get the dialog box
		$rslt = ajxGetDialogBox($_POST);
		echo (!$rslt) ? writeErrors() : $rslt;
		break;
	case 'err':
		echo writeErrors();
		break;
	default:
		catchErr('An action could not be acquired.');
		echo writeErrors();
}
