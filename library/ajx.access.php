<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.access.php');

// prevent direct access
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header('Location: /');
}

// get the specified ajax action
$action = getValue('act');

// verify user is logged in
$account = new Account();
if (!$account->confirmSession()) {
	catchErr('Could not acquire account information. <a href="/">Login</a>');
	$action = 'err';
}

// get the account and project ids
$account_id = (int)$_SESSION['account_id'];
$project_id = (int)getValue('prj', 0);

// verify the project id is valid
if ($project_id == 0) {
	catchErr('A project identifier could not be acquired.');
	$action = 'err';
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($action == 'ra') { // remove project access
	$access = new Access();

	if (!$access->remove($project_id, $account_id)) {
		echo writeErrors();
	} else {
		echo $project_id;
	}
} else {
	catchErr('An action could not be acquired.');
	echo writeErrors();
}