<?php
require_once '/home1/checkly1/public_html/library/cls.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class Access
{
	/**
	 * property contains the database object
	 * @private object
	 */
	private $_mysqli;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function __construct()
	{
		$this->_mysqli = new Mysqli_Database();
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 *
	 * @param string $permission [READ, WRITE, ADMIN]
	 * @param integer $project_id
	 * @param integer $account_id
	 * @return boolean
	 * @access public
	 */
	function create($permission, $project_id, $account_id)
	{
		$sql = "INSERT INTO project_access (
					project_id,
					account_id,
					permission
				) values (
					?,
					?,
					?
				)";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->bind_param('iis', $project_id, $account_id, $permission);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * create a new link between two accounts
	 *
	 * @param integer $account_id
	 * @param integer $partner_id
	 * @return integer
	 * @access public
	 */
	function createLink($account_id, $partner_id)
	{
		$sql = "INSERT INTO people_link (
					account_id,
					partner_id,
					created_dt
				) values (
					?,
					?,
					NOW()
				)";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->bind_param('ii', $account_id, $partner_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->close();

		// get the new id
		$people_link_id = mysqli_insert_id($this->_mysqli);

		return $people_link_id;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record
	 *
	 * @param integer $access_id
	 * @return array
	 * @access public
	 */
	function get($access_id)
	{
		return $this->_mysqli->row_get('access', 'access_id', (int)$access_id);
	}

	/**
	 * check if a link already exists
	 *
	 * @param integer $account_id
	 * @param integer $partner_id
	 * @return boolean
	 */
	function checkLink($account_id, $partner_id)
	{
		$sql = "SELECT
					account_id,
					partner_id
				FROM
					people_link
				WHERE
					account_id = ?
				AND
					partner_id = ?
				LIMIT 1";

		$stmt = $this->_mysqli->prepare($sql);
		$stmt->bind_param("ii", $account_id, $partner_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->store_result();

		// check if the record exists
		if ($stmt->num_rows == 0) {
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * read all the people links for an account
	 *
	 * @param integer $account_id
	 * @param string $order order by field
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function read($account_id, $order = "display_name", $since_dt = '')
	{
		if ($since_dt != '') {
			$where = "AND UNIX_TIMESTAMP(p.created_dt) > " . $since_dt;
		} else {
			$where = '';
		}
		
		$sql = "SELECT
					people_link_id,
					partner_id AS account_id,
					display_name
				FROM
					people_link AS p
				LEFT JOIN
					accounts AS a
				ON
					p.partner_id = a.account_id
				WHERE
					p.account_id = " . (int)$account_id . "
					{$where}
				ORDER BY
					{$order}";

		$result = $this->_mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * get a list of all project permissions for an account
	 *
	 * @param integer $account_id
	 * @return array
	 * @access public
	 */
	function readAccess($account_id)
	{
		$sql = "SELECT
					project_id,
					permission
				FROM
					projects
				INNER JOIN
					project_access USING (project_id)
				WHERE
					account_id = " . (int)$account_id . "
				ORDER BY
					project_name";

		$result = $this->_mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$row['project_id']] = $row['permission'];
			++$i;
		}
		$result->close();

		return $return;
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing record
	 *
	 * @param string $permission
	 * @param integer $project_id
	 * @param integer $account_id
	 * @return boolean
	 * @access public
	 */
	function update($permission, $project_id, $account_id)
	{
		$sql = "UPDATE
					project_access
				SET 
					permission = ?
				WHERE
					account_id = ?
				AND
					project_id = ?";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this->_mysqli);
			return false;
        }
		$stmt->bind_param('sii', $permission, $account_id, $project_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	// DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * removes an accounts permission to access a project
	 *
	 * @param integer $project_id
	 * @param integer $account_id
	 * @return boolean
	 * @access public
	 */
	function remove($project_id, $account_id)
	{
		$sql = "DELETE FROM
					project_access
				WHERE
					project_id = ?
				AND
					account_id = ?";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this);
			return false;
        }
		$stmt->bind_param('ii', $project_id, $account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * permanently removes a link record
	 *
	 * @param integer $account_id
	 * @param integer $partner_id
	 * @return boolean
	 * @access public
	 */
	function removeLink($account_id, $partner_id)
	{
		// remove the link
		$sql = "DELETE FROM
					people_link
				WHERE
					account_id = ?
				AND
					partner_id = ?";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this);
			return false;
        }
		$stmt->bind_param('ii', $account_id, $partner_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this);
			return false;
		}
		$stmt->close();

		// remove the project access
		$sql = "DELETE
					p.*
				FROM
					project_access p
				LEFT JOIN
					project_access pp USING(project_id)
				WHERE
					(pp.permission = 'ADMIN' AND pp.account_id = ?)
				AND
					p.account_id = ?";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this);
			return false;
        }
		$stmt->bind_param('ii', $account_id, $partner_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this);
			return false;
		}
		$stmt->close();

		return true;
	}
}