<?php

require_once '/home1/checkly1/public_html/library/cls.projects.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class ProjectList extends Projects {
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     *
     * @param integer $account_id
     * @param integer $project_id
     */
    function __construct($account_id = 0, $project_id = 0) {
        parent::__construct($account_id);

        if ($project_id != 0) {
            $this->setProject($project_id);
        }
    }

    // CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * create a new list record
     *
     * @param array $args
     * @return integer
     * @access public
     */
    function createListItem($args) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        $sql = "INSERT INTO project_items (
					item_priority,
					project_id,
					item_text,
					due_dt,
					assigned_to,
					created_by,
					created_dt,
					modified_dt
				) SELECT 1 + COALESCE((SELECT MAX(item_priority) FROM
						project_items WHERE project_id = ?), 0),
					?, ?, ?, ?, ?,
					NOW(),
					NOW()";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('iissii', $this->project_id, $this->project_id, $args['item_text'], $args['due_dt'], $this->account_id, $this->account_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        // get the new id
        $item_id = mysqli_insert_id($this->mysqli);

        return $item_id;
    }

    // READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * get a single record
     *
     * @param integer $item_id
     * @return array
     * @access public
     */
    function getListItem($item_id) {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }

        return $this->mysqli->row_get('project_items', 'item_id', (int) $item_id);
    }

    /**
     * read multiple records
     *
     * @param string $order order by field
	 * @param string $since_dt
     * @return array (item list, total rows, total completed, js priority list)
     * @access public
     */
    function readList($order = "item_priority", $since_dt = '') {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }
		
		if ($since_dt != '') {
			$where = "AND UNIX_TIMESTAMP(modified_dt) > " . $since_dt;
		} else {
			$where = '';
		}

        // query database
        $sql = "SELECT
					item_id,
					project_id,
					item_text,
					item_priority,
					assigned_to,
					due_dt,
					strike,
					strike_dt,
					created_dt,
					modified_dt
				FROM
					project_items
				WHERE
					project_id = {$this->project_id}
					{$where}
				ORDER BY
					{$order}";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $completed = 0;
        $priorities = array();
        $checklist = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            // count completed tasks
            if ($row['strike'] == 1) {
                ++$completed;
            }
            // build an array of priorities
            $priorities[$row['item_id']] = $row['item_priority'];
            // build the return array
            $checklist[$i] = $row;
            $checklist[$i]['item_text'] = stripslashes($row['item_text']);
            if ($row['due_dt'] == '0000-00-00 00:00:00' || empty($row['due_dt'])) {
                $checklist[$i]['due_dt'] = false;
            } else {
                $checklist[$i]['due_dt'] = strtotime($row['due_dt']);
            }
			$checklist[$i]['created_dt'] = strtotime($row['created_dt']);
			$checklist[$i]['modified_dt'] = strtotime($row['modified_dt']);
			$checklist[$i]['strike_dt'] = strtotime($row['strike_dt']);
            ++$i;
        }
        $result->close();

        // update access date
        $this->accessTimeStamp();

        return array($checklist, $i, $completed, json_encode($priorities));
    }

    /**
     * read multiple records
     *
     * @param string $order order by field
     * @return array (item list, total rows, total completed, js priority list)
     * @access public
     */
    function readIncomplete($order = "item_priority") {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }

        // query database
        $sql = "SELECT
					item_id,
					project_id,
					item_text,
					item_priority,
					assigned_to,
					due_dt,
					strike,
					strike_dt,
					created_dt,
					modified_dt
				FROM
					project_items
				WHERE
					project_id = {$this->project_id}
				AND
					strike = 0
				ORDER BY
					{$order}";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $completed = 0;
        $priorities = array();
        $checklist = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            // count completed tasks
            if ($row['strike'] == 1) {
                ++$completed;
            }
            // build an array of priorities
            $priorities[$row['item_id']] = $row['item_priority'];
            // build the return array
            $checklist[$i] = $row;
            $checklist[$i]['item_text'] = stripslashes($row['item_text']);
            if ($row['due_dt'] == '0000-00-00 00:00:00' || empty($row['due_dt'])) {
                $checklist[$i]['due_dt'] = false;
            } else {
                $checklist[$i]['due_dt'] = strtotime($row['due_dt']);
            }
            ++$i;
        }
        $result->close();

        // update access date
        $this->accessTimeStamp();

        return array($checklist, $i, $completed, json_encode($priorities));
    }

    /**
     * read multiple records
     *
     * @param string $order order by field
     * @return array (item list, total rows, total completed, js priority list)
     * @access public
     */
    function readComplete($order = "item_priority") {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }

        // query database
        $sql = "SELECT
					item_id,
					project_id,
					item_text,
					item_priority,
					assigned_to,
					due_dt,
					strike,
					strike_dt,
					created_dt,
					modified_dt
				FROM
					project_items
				WHERE
					project_id = {$this->project_id}
				AND
					strike = 1
				ORDER BY
					{$order}";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $completed = 0;
        $priorities = array();
        $checklist = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            // count completed tasks
            if ($row['strike'] == 1) {
                ++$completed;
            }
            // build an array of priorities
            $priorities[$row['item_id']] = $row['item_priority'];
            // build the return array
            $checklist[$i] = $row;
            $checklist[$i]['item_text'] = stripslashes($row['item_text']);
            if ($row['due_dt'] == '0000-00-00 00:00:00' || empty($row['due_dt'])) {
                $checklist[$i]['due_dt'] = false;
            } else {
                $checklist[$i]['due_dt'] = strtotime($row['due_dt']);
            }
            ++$i;
        }
        $result->close();

        // update access date
        $this->accessTimeStamp();

        return array($checklist, $i, $completed, json_encode($priorities));
    }

    /**
     * returns the percentage of completed items
     *
	 * @param integer $project_id project identifier
     * @return array (complete percentage)
     * @access public
     */
    function completePercent($project_id) {

        // query database
        $sql = "SELECT
					ROUND((SELECT COUNT(*) FROM project_items
						WHERE project_id = {$project_id}
						AND strike = 1) / COUNT(*) * 100) AS CompPercent
				FROM
					project_items
				WHERE
					project_id = {$project_id}";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $row = $result->fetch_assoc();

        if ($row["CompPercent"] == "") {
            $row["CompPercent"] = 0;
        }

        $result->close();

        return $row;
    }

    /**
     * creates a javascript list of priorities for the specified project
     *
     * @return string
     */
    function readPriorities() {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }

        // query database
        $sql = "SELECT
					item_id,
					item_priority
				FROM
					project_items
				WHERE
					project_id = {$this->project_id}";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $return = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $return[$row['item_id']] = $row['item_priority'];
            ++$i;
        }
        $result->close();

        return json_encode($return);
    }

    /**
     * get a list of people that have access to the current project
     *
     * @return array
     * @access public
     */
    function readPeople() {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }

        $sql = "SELECT
					account_id,
					permission,
					display_name
				FROM
					project_access
				LEFT JOIN
					accounts
				USING (
					account_id)
				WHERE
					project_id = {$this->project_id}
				ORDER BY
					display_name";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        // loop result and build return array
        $return = array();
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $return[$i] = $row;
            ++$i;
        }
        $result->close();

        return $return;
    }

    /**
     * count the total number of records for a project
     *
     * @return integer
     * @access public
     */
    function totalItems() {
        $sql = "SELECT
					COUNT(item_id) as record_count
				FROM
					project_items
				WHERE
					project_id = ?";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('i', $this->project_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->close();

        return $count;
    }

    // UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * update an existing record
     *
     * @param integer $item_id
     * @param array $args
     * @return boolean
     * @access public
     */
    function updateListItem($item_id, $args) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        $sql = "UPDATE
					project_items
				SET
					project_id = ?,
					item_text = ?,
					due_dt = ?,
					assigned_to = ?,
					modified_by = ?,
					modified_dt = NOW()
				WHERE
					item_id = ?";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('issiii', $this->project_id, $args['item_text'], $args['due_dt'], $args['assigned_to'], $this->account_id, $item_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        return true;
    }

    /**
     * adjusts the priority of items that are affected by a move
     *
     * @param integer $item_id
     * @param integer $old old priority
     * @param integer $new new priority
     * @return boolean
     */
    function updatePriority($item_id, $old, $new) {
        // make sure the parameters are integers
        $old = (int) $old;
        $new = (int) $new;

        // get a list of items that need to be updated
        $sql = 'SELECT item_id FROM project_items
					WHERE project_id = ' . $this->project_id;

        if ($new < $old) {
            // moving down
            $sql .= ' AND (item_priority >= ' . $new .
                    ' AND item_priority < ' . $old . ')
					ORDER BY item_priority';
            $i = $new + 1;
        } else {
            // moving up
            $sql .= ' AND (item_priority <= ' . $new .
                    ' AND item_priority > ' . $old . ')
					ORDER BY item_priority';
            $i = $old;
        }

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        // update the priority for each effected item
        $sql = 'UPDATE
					project_items
				SET
					item_priority = ?
				WHERE
					item_id = ?';

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        while ($row = $result->fetch_assoc()) {
            $stmt->bind_param('ii', $i, $row['item_id']);
            if (!$stmt->execute()) {
                catchSQL($sql, $this->mysqli);
                return false;
            }
            $i++;
        }
        $result->close();

        // update the moved item
        $stmt->bind_param('ii', $new, $item_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        return true;
    }

    /**
     * mark a list item as completed or restore
     *
     * @param integer $item_id
     * @param boolean $strike_id
     * @return boolean
     */
    function strikeListItem($item_id, $strike = true) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        if ($strike) {
            $value = 1;
            $date = 'NOW()';
        } else {
            $value = 0;
            $date = 'NULL';
        }

        $sql = "UPDATE
					project_items
				SET
					strike = {$value},
					strike_dt = {$date},
					modified_dt = NOW()
				WHERE
					item_id = ?";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('i', $item_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        return true;
    }

    // DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * permanently removes a record
     *
     * @param integer $item_id
     * @return boolean
     * @access public
     */
    function removeListItem($item_id) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        return $this->mysqli->row_delete('project_items', 'item_id', (int) $item_id);
    }

}