<?php

require_once "/home1/checkly1/public_html/library/cls.mysqli_database.php";

/**
 * @author Cone Code Development
 * @version 3.3
 */
class Device {

	/**
	 * property contains unique account identifier
	 * @var integer
	 */
	var $account_id;

	/**
	 * property contains unique device identifier
	 * @var string
	 */
	var $account_udid;

	/**
	 * property contains unique login code
	 * @var string
	 */
	var $login_code;

	/**
	 * property contains the database object
	 * @var object
	 */
	var $mysqli;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function __construct($udid = 'X', $accountId = 0, $lcode = 'X') {
		$this->mysqli = new Mysqli_Database();

		if ($accountId != '') {
			$this->account_id = (int) $accountId;
		}

		if ($udid != '') {
			$this->account_udid = $udid;
		}

		if ($lcode != '') {
			$this->login_code = $lcode;
		}
	}

	// PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * send mail notification to user
	 *
	 * @param string $body email message
	 * @param string $recipient email address
	 * @return boolean
	 * @access private
	 */
	private function _sendEmail($body, $recipient) {
		// build email headers
		$header = "From: Checklyst <no-reply@checklyst.com>\r\n";
		$header .= "Reply-To: no-reply@checklyst.com\r\n";
		$header .= "X-Mailer: PHP/" . phpversion() . "\r\n";

		$subject = SITE_NAME . " Account Notification";

		// try to send mail
		$send = mail($recipient, $subject, $body, $header);
		if (!$send) {
			catchErr("Notification email could not be sent.");
			return false;
		}

		return true;
	}

	/**
	 * update user login values
	 *
	 * @return boolean
	 * @access private
	 */
	private function _updateLogin() {
		// get remote user ip address
		$ip = $_SERVER["REMOTE_ADDR"];

		// lookup host name if available
		$host = gethostbyaddr($ip);

		$sql = "UPDATE
				accounts
			SET
				display_name = '" . $this->display_name . "',
				last_login_dt = NOW(),
				last_login_ip = '" . $ip . "',
				last_login_host = '" . $host . "'
			WHERE
				account_id = " . $this->account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$prehash = $this->account_udid . date('m-d-Y');
		$login_code = substr(md5($prehash . PASSWORD_SALT), 4, 16);
		$this->login_code = $login_code;

		// lock the tables
		$result = $this->mysqli->query("LOCK TABLES device_login WRITE");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$sql = "UPDATE
                device_login
            SET
                login_code = '$this->login_code',
                account_id = $this->account_id
            WHERE
                account_udid = '$this->account_udid'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// unlock the tables
		$result = $this->mysqli->query("UNLOCK TABLES");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		return true;
	}

	/**
	 * update user login values
	 *
	 * @return boolean
	 * @access private
	 */
	private function _createLogin() {
		$prehash = $this->account_udid . date('m-d-Y');
		$login_code = substr(md5($prehash . PASSWORD_SALT), 4, 16);
		$this->login_code = $login_code;

		$sql = "INSERT INTO device_login (
				account_udid,
				login_code,
				account_id
			) VALUES (
				'" . $this->account_udid . "',
				'" . $this->login_code . "',
				" . $this->account_id . "
			)";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		return true;
	}

	/**
	 * verfiy that a username and password are correct
	 *
	 * @param string $username
	 * @param string $encryped encrypted password
	 * @return array session values
	 * @access private
	 */
	private function _validateUser() {
		$sql = "SELECT
                account_id,
                account_email,
                account_status,
                display_name
            FROM
                accounts
            WHERE
                account_udid = ?
            LIMIT 1";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("s", $this->account_udid);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->store_result();

		// check if the account exists
		if ($stmt->num_rows == 0) {
			catchErr("New");
			return false;
		}

		$stmt->bind_result($id, $email, $status, $name);
		$stmt->fetch();

		// check if the account has been deleted
		if ($status < 1) {
			catchErr("The user account you entered has been deactivated.");
			return false;
		}

		$return["account_id"] = $id;
		$return["account_status"] = $status;
		$return["display_name"] = $name;

		$stmt->close();
		return $return;
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * verify unique email address
	 *
	 * @param string $email email address
	 * @return boolean true if email exists
	 * @access public
	 */
	function emailExists($email) {
		$sql = "SELECT
                account_id
            FROM
                accounts
            WHERE
                account_email = ?
            AND
                account_status > 0
            LIMIT 1";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("s", $email);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->store_result();

		if ($stmt->num_rows > 0) {
			$return = true;
		} else {
			$return = false;
		}

		$stmt->close();
		return $return;
	}

	/**
	 * verify unique username
	 *
	 * @param string $username
	 * @return boolean true if username exists
	 * @access public
	 */
	function usernameExists($username) {
		$sql = "SELECT
                account_id
            FROM
                accounts
            WHERE
                account_username = ?
            AND
                account_status > 0
            LIMIT 1";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("s", $username);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->store_result();

		if ($stmt->num_rows > 0) {
			$return = true;
		} else {
			$return = false;
		}

		$stmt->close();
		return $return;
	}

	/**
	 * email an account password to the associated user
	 *
	 * @param string $email
	 * @return boolean
	 * @access public
	 */
	function sendReminder($username) {
		$this->mysqli->escape($username);

		// get account info from username
		$sql = "SELECT
                account_email,
                account_remind,
                account_username
            FROM
                accounts
            WHERE
                account_username = '" . $username . "'
            AND
                account_status > 0";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		if ($result->num_rows < 1) {
			catchErr("Your account information could not be found.");
			return false;
		}

		$obj = $result->fetch_object();

		$body = SITE_NAME . " has received a request for your account login ";
		$body .= "information.\r\n\r\n";
		$body .= "Your Username: " . $obj->account_username . "\r\n";
		$body .= "Your Password: " . $obj->account_remind;

		$result->close();

		// send email
		return $this->_sendEmail($body, $obj->account_email);
	}

	// SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record
	 *
	 * @param integer $accountId
	 * @return array
	 * @access public
	 */
	function get() {
		// get account values
		$sql = "SELECT
				dl.login_code AS login_code,
				dl.account_id AS login_id,
				a.account_id AS user_id,
				a2.account_id AS your_id,
				a2.account_username AS your_username,
				a2.account_email AS your_email,
				a2.account_udid AS your_udid,
				a2.display_name AS your_name
			FROM
				device_login dl
			LEFT JOIN
				accounts a
			ON
				a.account_udid = dl.account_udid
			AND
				a.account_id = dl.account_id
			LEFT JOIN
				accounts a2
			ON
				a2.account_udid = dl.account_udid
			WHERE
				dl.account_udid = '" . $this->account_udid . "'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$row = $result->fetch_assoc();

		if (!empty($row)) {
			//check if the account they are trying to view is logged in
			if ($this->login_code == "X") {
				$this->account_id = $row['your_id'];
				$this->login_code = "";
				if ($this->_updateLogin()) {
					$return['status'] = 1;
					$return['id'] = $row['your_id'];
					$return['login_code'] = $this->login_code;
					$return['username'] = $row['your_username'];
					$return['email'] = $row['your_email'];
					$return['udid'] = $row['your_udid'];
					$return['name'] = $row['your_name'];
				} else {
					$return['status'] = 0;
					$return['id'] = 0;
					$return['login_code'] = 0;
					$return['username'] = "";
					$return['email'] = "";
					$return['udid'] = "";
					$return['name'] = "";
				}
			} else {
				$return['status'] = 1;
				$return['id'] = $this->account_id;
				$return['login_code'] = $this->login_code;
				$return['username'] = $row['your_username'];
				$return['email'] = $row['your_email'];
				$return['udid'] = $row['your_udid'];
				$return['name'] = $row['your_name'];
			}
		} else {
			$args["account_udid"] = $this->account_udid;
			$args["display_name"] = "";
			if ($this->create($args)) {
				$return['status'] = 1;
				$return['id'] = $this->account_id;
				$return['login_code'] = $this->login_code;
				$return['username'] = "";
				$return['email'] = "";
				$return['udid'] = "";
				$return['name'] = "";
			} else {
				$return['status'] = 0;
				$return['id'] = 0;
				$return['login_code'] = 0;
				$return['username'] = "";
				$return['email'] = "";
				$return['udid'] = "";
				$return['name'] = "";
			}
		}

		$result->close();

		return $return;
	}

	/**
	 * get a single record
	 *
	 * @param integer $accountId
	 * @return array
	 * @access public
	 */
	function getAccount() {
		// get account values
		$sql = "SELECT
				dl.login_code AS login_code,
				dl.account_id AS login_id,
				a.account_id AS your_id,
				a.account_username AS your_username,
				a.account_email AS your_email,
				a.account_udid AS your_udid,
				a.display_name AS your_name
			FROM
				accounts a
			LEFT JOIN
				device_login dl
			ON
				a.account_udid = dl.account_udid
			WHERE
				a.account_id = '" . $this->account_id . "'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$row = $result->fetch_assoc();

		if (!empty($row)) {
			//check if the account they are trying to view is logged in
			if ($this->_updateLogin()) {
				$return['status'] = 1;
				$return['id'] = $this->account_id;
				$return['login_code'] = $this->login_code;
				$return['username'] = $row['your_username'];
				$return['email'] = $row['your_email'];
				$return['udid'] = $this->account_udid;
				$return['name'] = $row['your_name'];
			} else {
				$return['status'] = 0;
				$return['id'] = 0;
				$return['login_code'] = 0;
				$return['username'] = "";
				$return['email'] = "";
				$return['udid'] = "";
				$return['name'] = "";
			}
		} else {
			$return['status'] = 0;
			$return['id'] = 0;
			$return['login_code'] = 0;
			$return['username'] = "";
			$return['email'] = "";
			$return['udid'] = "";
			$return['name'] = "";
		}

		$result->close();

		return $return;
	}

	/**
	 * get a single record
	 *
	 * @return array
	 * @access public
	 */
	function checkDevice() {
		// get account values
		$sql = "SELECT
					account_id,
					COUNT(*) AS found
				FROM
					device_login
				WHERE
					login_code = '" . $this->login_code . "'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$row = $result->fetch_assoc();

		if (!empty($row)) {
			if ($row['found']) {
				$return = $row['account_id'];
			} else {
				$return = false;
			}
		} else {
			$return = false;
		}

		$result->close();

		return $return;
	}

	/**
	 * get a single record
	 *
	 * @return array
	 * @access public
	 */
	function checkLogin($username, $password) {
		$reminder = $password;
		$password = substr(md5($reminder . PASSWORD_SALT), 4, 16);
		// get account values
		$sql = "SELECT
				account_id, display_name
			FROM
				accounts
			WHERE
				account_username = '$username'
			AND
				account_password = '$password'
		";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$row = $result->fetch_assoc();

		if (!empty($row)) {
			if ($row['account_id']) {
				$this->account_id = $row['account_id'];
				if($row['display_name'] != "") {
					$this->display_name = $row['display_name'];
				}
				$return = $row['account_id'];
			} else {
				$return = false;
			}
		} else {
			$return = false;
		}

		$result->close();

		return $return;
	}

	/**
	 * get the account settings
	 *
	 * @return array
	 * @access public
	 */
	function getSettings() {
		$row = $this->mysqli->one("account_settings", "account_udid", $this->account_udid);

		return $row;
	}

	// INSERT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 *
	 * @param array $args [email, password, username, display name]
	 * @return boolean
	 * @access public
	 */
	function create($args) {
		$display = htmlentities($args["display_name"]);

		// lock the tables
		$result = $this->mysqli->query("LOCK TABLES accounts WRITE");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// insert new account record
		$sql = "INSERT INTO accounts (
                account_udid,
                display_name,
                created_dt,
                modified_dt
            ) values (
                ?, ?,
                NOW(),
                NOW()
            )";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("ss", $this->account_udid, $display);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->close();

		// get the account_id
		$account_id = mysqli_insert_id($this->mysqli);
		$this->account_id = $account_id;

		// unlock the tables
		$result = $this->mysqli->query("UNLOCK TABLES");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// update the account table with the login info
		$this->_createLogin();

		return true;
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * add login to udid user
	 *
	 * @param array $args [email, password, username, display name, udid]
	 * @return boolean
	 * @access public
	 */
	function createFromUDID($args) {
		$reminder = $args["account_password"];
		$password = substr(md5($reminder . PASSWORD_SALT), 4, 16);

		$email = $args["account_email"];

		$username = $args["account_username"];

		$display = htmlentities($args["display_name"]);
		if ($display == "") {
			$display = $username;
		}

		// lock the tables
		$result = $this->mysqli->query("LOCK TABLES accounts WRITE");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$sql = "UPDATE accounts
                SET
                   account_email = '$email',
                   account_password = '$password',
                   account_remind = '$reminder',
                   account_username = '$username',
                   display_name = '$display',
                   modified_dt = NOW()
                WHERE
                  account_udid = '".$this->account_udid."'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$this->checkLogin($username, $reminder);

		if ($this->_updateLogin()) {
			$return['status'] = 1;
			$return['id'] = $this->account_id;
			$return['login_code'] = $this->login_code;
			$return['username'] = $username;
			$return['email'] = $email;
			$return['udid'] = $this->account_udid;
			$return['name'] = $display;
		} else {
			$return['status'] = 0;
			$return['id'] = 0;
			$return['login_code'] = 0;
			$return['username'] = "";
			$return['email'] = "";
			$return['udid'] = "";
			$return['name'] = "";
		}

		// send an email message
		$body = "Hello " . $display . "!\r\n";
		$body .= "Thank you for signing up with " . SITE_NAME . ".\r\n\n";
		$body .= "Your Username: " . $username . "\r\n";
		$body .= "Your Password: " . $reminder;

		$this->_sendEmail($body, $email);

		return $return;
	}

	/**
	 * add login to udid user
	 *
	 * @param array $args [email, password, username, display name, udid]
	 * @return boolean
	 * @access public
	 */
	function createFromCode($args) {
		$reminder = $args["account_password"];
		$code = $args["account_code"];
		$password = substr(md5($reminder . PASSWORD_SALT), 4, 16);

		$email = $args["account_email"];
		if ($this->emailExists($email)) {
			catchErr("The email address already exists.");
			return false;
		}

		$username = $args["account_username"];
		if ($this->usernameExists($username)) {
			catchErr("The username already exists.");
			return false;
		}

		$display = htmlentities($args["display_name"]);
		if ($display == "") {
			$display = $username;
		}

		// lock the tables
		$result = $this->mysqli->query("LOCK TABLES accounts WRITE");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// insert new account record
		$sql = "UPDATE accounts (
                SET
                   account_udid = ?,
                   account_email = ?,
                   account_password = ?,
                   account_remind = ?,
                   account_username = ?,
                   display_name = ?,
                   modified_dt = NOW()
                WHERE
                  account_code = ?";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("sssssss", $this->account_udid, $email, $password, $reminder, $username, $display, $code);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->close();

		// unlock the tables
		$result = $this->mysqli->query("UNLOCK TABLES");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// update the account table with the login info
		$this->_updateLogin();

		// send an email message
		$body = "Hello " . $display . "!\r\n";
		$body .= "Thank you for signing up with " . SITE_NAME . ".\r\n\n";
		$body .= "Your Username: " . $username . "\r\n";
		$body .= "Your Password: " . $reminder;

		$this->_sendEmail($body, $email);

		return true;
	}

	/**
	 * add login to udid user
	 *
	 * @param array $args [email, password, username, display name, udid]
	 * @return boolean
	 * @access public
	 */
	function changePassword($newpass,$email) {
		$reminder = $newpass;
		$password = substr(md5($reminder . PASSWORD_SALT), 4, 16);

		// lock the tables
		$result = $this->mysqli->query("LOCK TABLES accounts WRITE");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$sql = "UPDATE accounts
			SET
				account_password = '$password',
				account_remind = '$reminder',
				modified_dt = NOW()
			WHERE
				account_id = '".$this->account_id."'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// send an email message
		$body = "Hello!\r\n";
		$body .= "Your password for " . SITE_NAME . " has been changed.\r\n\n";
		$body .= "Your new password is: " . $reminder;

		$this->_sendEmail($body, $email);

		return $return;
	}

}