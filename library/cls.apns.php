<?php
require_once 'library/cls.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class APNS {

	/**
	 * property contains unique device
	 * @var integer
	 */
	var $deviceuid;
	/**
	 * property contains the database object
	 * @private object
	 */
	var $mysqli;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function __construct($udid='') {
		$this->_mysqli = new Mysqli_Database();

		if ($udid != '') {
			$this->deviceuid = $udid;
		}
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 *
	 * @param string $name
	 * @param string $email
	 * @param string $code
	 * @param string $permissions
	 * @return integer
	 * @access public
	 */
	function create($args) {
		$sql = "INSERT INTO apns_devices (
					appname,
					appversion,
					deviceuid,
					devicetoken,
					devicename,
					devicemodel,
					deviceversion,
					pushbadge,
					pushalert,
					pushsound,
					status,
					created,
					modified
				) values (
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					'active',
					NOW(),
					NOW()
				)";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}

		$stmt->bind_param('ssssssssss', $args['appname'], $args['appversion'], $args['deviceuid'], $args['devicetoken'], $args['devicename'], $args['devicemodel'], $args['deviceversion'], $args['pushbadge'], $args['pushalert'], $args['pushsound']);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record by code
	 *
	 * @param string $code
	 * @return array
	 * @access public
	 */
	function get($code)
	{
		$this->_mysqli->escape($code);
		return $this->_mysqli->row_get('apns_devices', 'deviceuid', "'".$this->deviceuid."'");
	}

	/**
	 * count the total number of records
	 *
	 * @param integer
	 * @return array
	 * @access public
	 */
	function exists() {
		return $this->_mysqli->row_count_where('apns_devices', 'deviceuid', "'".$this->deviceuid."'");
	}

	/**
	 * count the total number of records
	 *
	 * @param integer
	 * @return array
	 * @access public
	 */
	function getUDID($email) {
		$sql = "SELECT
					devicetoken
				FROM
					apns_devices d
				INNER JOIN
					device_login l
				ON
					l.account_udid = d.deviceuid
				INNER JOIN
					accounts a
				ON
					a.account_id = l.account_id
				WHERE
					a.account_email = '".$email."'";
//		echo $sql;

		$result = $this->_mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->_mysqli);
			return 'X';
		}

		$row = $result->fetch_assoc();

		if (!empty($row)) {
			return $row['devicetoken'];
		} else {
			return 'X';
		}
	}

	/**
	 * get a user's Device Token
	 *
	 * @param integer
	 * @return array
	 * @access public
	 */
	function hasApp() {
		return $this->_mysqli->row_count_where('device_login', 'account_udid', "'".$this->deviceuid."'");
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing record
	 *
	 * @param integer $invite_id
	 * @param string $permissions
	 * @return boolean
	 * @access public
	 */
	function update($args) {
		$sql = "UPDATE
					apns_devices
				SET
					appname = ?,
					appversion = ?,
					devicetoken = ?,
					devicename = ?,
					devicemodel = ?,
					deviceversion = ?,
					pushbadge = ?,
					pushalert = ?,
					pushsound = ?,
					modified = NOW()
				WHERE
					deviceuid = ?";

		if (!$stmt = $this->_mysqli->prepare($sql)) {
			catchSQL($sql, $this->_mysqli);
			return false;
        }
		$stmt->bind_param('ssssssssss', $args['appname'], $args['appversion'], $args['devicetoken'], $args['devicename'], $args['devicemodel'], $args['deviceversion'], $args['pushbadge'], $args['pushalert'], $args['pushsound'], $args['deviceuid']);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->_mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	// DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * permanently removes a record
	 *
	 * @param integer $invite_id
	 * @return boolean
	 * @access public
	 */
	function remove($invite_id)
	{
		return $this->_mysqli->row_delete('invites', 'invite_id', (int)$invite_id);
	}
}