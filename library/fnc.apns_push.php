<?php

function push($args) {
// Adjust to your timezone
	date_default_timezone_set('America/Los_Angeles');

// Report all PHP errors
	error_reporting(-1);

// Using Autoload all classes are loaded on-demand
	require_once '/home/checklys/public_html/library/ApnsPHP/Autoload.php';

// Instanciate a new ApnsPHP_Push object
	$push = new ApnsPHP_Push(
				 ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
				 '/home/checklys/public_html/library/ck.pem'
	);

// Connect to the Apple Push Notification Service
	$push->connect();

// Instantiate a new Message with a single recipient
	$message = new ApnsPHP_Message($args['devicetoken']);

// Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
// over a ApnsPHP_Message object retrieved with the getErrors() message.
	$message->setCustomIdentifier("Message-Badge-" . time());

// Set badge icon
	$message->setBadge($args['badges']);

// Set a simple welcome text
	$message->setText($args['alert']);

// Play the default sound
	$message->setSound();

// Set a custom property
	$message->setCustomProperty('pn_type', $args['pn_type']);

// Set 30 second expiration
	$message->setExpiry(30);

// Add the message to the message queue
	$push->add($message);

// Send all messages in the message queue
	$push->send();

// Disconnect from the Apple Push Notification Service
	$push->disconnect();

// Examine the error message container
	$aErrorQueue = $push->getErrors();
	if (!empty($aErrorQueue)) {
		return false;
	}

	return true;
}