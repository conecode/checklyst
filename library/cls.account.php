<?php

require_once "/home1/checkly1/public_html/library/cls.mysqli_database.php";

/**
 * @author Cone Code Development
 * @version 3.3
 */
class Account {

	/**
	 * property contains unique account identifier
	 * @var integer
	 */
	var $account_id;

	/**
	 * property contains the database object
	 * @var object
	 */
	var $mysqli;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function __construct($accountId = 0) {
		$this->mysqli = new Mysqli_Database();

		if ($accountId > 0) {
			$this->account_id = (int) $accountId;
		}
	}

	// PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * send mail notification to user
	 *
	 * @param string $body email message
	 * @param string $recipient email address
	 * @return boolean
	 * @access private
	 */
	private function _sendEmail($body, $recipient) {
		// build email headers
		$header = "From: Checklyst <no-reply@checklyst.com>\r\n";
		$header .= "Reply-To: no-reply@checklyst.com\r\n";
		$header .= "X-Mailer: PHP/" . phpversion() . "\r\n";

		$subject = SITE_NAME . " Account Notification";

		// try to send mail
		$send = mail($recipient, $subject, $body, $header);
		if (!$send) {
			catchErr("Notification email could not be sent.");
			return false;
		}

		return true;
	}

	/**
	 * update user login values
	 *
	 * @return boolean
	 * @access private
	 */
	private function _updateLogin() {
		// get remote user ip address
		$ip = $_SERVER["REMOTE_ADDR"];

		// lookup host name if available
		$host = gethostbyaddr($ip);

		$sql = "UPDATE
                accounts
            SET
                last_login_dt = (NOW()),
                last_login_ip = '" . $ip . "',
                last_login_host = '" . $host . "'
            WHERE
                account_id = " . $this->account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		return true;
	}

	/**
	 * sets the username and password to a long term cookie
	 *
	 * @param string $username
	 * @param string $password encrypted
	 * @access private
	 */
	private function _setCookie($username, $password) {
		// create string from account array values
		$str = $username . "|" . $password;

		// set account session cookie
		setcookie(COOKIE_NAME, $str, time() + 31104000, '/', COOKIE_PATH, '');
	}

	/**
	 * verify that a valid cookie exists
	 *
	 * @return boolean
	 * @access private
	 */
	private function _validateCookie() {
		if (isset($_COOKIE[COOKIE_NAME])) {

			$cookie = explode("|", $_COOKIE[COOKIE_NAME]);

			// check that the cookie matches the database
			if (!$session = $this->_validateUser($cookie[0], $cookie[1])) {

				return false; // cookie was not valid
			}
		} else {

			return false; // there is no cookie!
		}

		// set the session
		$_SESSION = $session;
		$this->account_id = $session["account_id"];
		$this->_updateLogin();

		return true;
	}

	/**
	 * verfiy that a username and password are correct
	 *
	 * @param string $username
	 * @param string $encryped encrypted password
	 * @return array session values
	 * @access private
	 */
	private function _validateUser($username, $encryped) {
		$sql = "SELECT
                account_id,
                account_email,
                account_password,
                account_status,
                display_name
            FROM
                accounts
            WHERE
                account_username = ?
            LIMIT 1";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("s", $username);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->store_result();

		// check if the account exists
		if ($stmt->num_rows == 0) {
			catchErr("The username or password you entered does not match any account in our records.");
			return false;
		}

		$stmt->bind_result($id, $email, $password, $status, $name);
		$stmt->fetch();

		// check if the account has been deleted
		if ($status < 1) {
			catchErr("The user account you entered has been deactivated.");
			return false;
		}

		// check if the account password is correct
		if (strcmp($encryped, $password)) {
			catchErr("The username or password you entered does not match any account in our records.");
			return false;
		}

		$return["account_id"] = $id;
		$return["account_status"] = $status;
		$return["display_name"] = $name;

		$stmt->close();
		return $return;
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * log into a user account
	 *
	 * @param string $username
	 * @param string $password not encrypted
	 * @param boolean $cookie true creates a long term cookie
	 * @return boolean
	 * @access public
	 */
	function login($username, $password, $cookie = false) {
		$password = substr(md5($password . PASSWORD_SALT), 4, 16);

		if (!$session = $this->_validateUser($username, $password)) {

			return false;
		}

		// set the session
		session_set_cookie_params(3600);
		session_start();
		$_SESSION = $session;
		$this->account_id = $session["account_id"];
		$this->_updateLogin();
		if ($cookie) {
			$this->_setCookie($username, $password);
		}
		return true;
	}

	/**
	 * log out of a user account
	 *
	 * @return boolean
	 * @access public
	 */
	function logout() {
		session_start();

		if (isset($_SESSION["account_status"])) {
			unset($_SESSION["account_status"]);
			if (isset($_COOKIE[session_name()])) {
				setcookie(session_name(), "", time() - 1000, "/");
				session_destroy();
			}
		}
		if (isset($_COOKIE[COOKIE_NAME])) {
			setcookie(COOKIE_NAME, "", time() - 1000, "/", COOKIE_PATH, '');
			unset($_COOKIE[COOKIE_NAME]);
		}

		return true;
	}

	/**
	 * confirm existence of a valid account session
	 *
	 * @return boolean
	 * @access public
	 */
	function confirmSession() {
		session_set_cookie_params(3600);
		session_start();
		if (!isset($_SESSION["account_status"])) {

			if (!$this->_validateCookie()) {
				session_destroy();
				return false;
			}
		} else {
			$this->account_id = (int) $_SESSION["account_id"];
		}

		return true;
	}

	/**
	 * verify unique email address
	 *
	 * @param string $email email address
	 * @return boolean true if email exists
	 * @access public
	 */
	function emailExists($email) {
		$sql = "SELECT
                account_id
            FROM
                accounts
            WHERE
                account_email = ?
            AND
                account_status > 0
            LIMIT 1";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("s", $email);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->store_result();

		if ($stmt->num_rows > 0) {
			$return = true;
		} else {
			$return = false;
		}

		$stmt->close();
		return $return;
	}

	/**
	 * verify unique username
	 *
	 * @param string $username
	 * @return boolean true if username exists
	 * @access public
	 */
	function usernameExists($username) {
		$sql = "SELECT
                account_id
            FROM
                accounts
            WHERE
                account_username = ?
            AND
                account_status > 0
            LIMIT 1";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("s", $username);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->store_result();

		if ($stmt->num_rows > 0) {
			$return = true;
		} else {
			$return = false;
		}

		$stmt->close();
		return $return;
	}

	/**
	 * email an account password to the associated user
	 *
	 * @param string $email
	 * @return boolean
	 * @access public
	 */
	function sendReminder($email) {
		$this->mysqli->escape($email);

		// get account info from username
		$sql = "SELECT
                account_email,
                account_remind,
                account_username
            FROM
                accounts
            WHERE
                account_email = '" . $email . "'
            AND
                account_status > 0";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		if ($result->num_rows < 1) {
			catchErr("Your account information could not be found.");
			return false;
		}

		$obj = $result->fetch_object();

		$body = SITE_NAME . " has received a request for your account login ";
		$body .= "information.\r\n\r\n";
		$body .= "Your Username: " . $obj->account_username . "\r\n";
		$body .= "Your Password: " . $obj->account_remind;

		$result->close();

		// send email
		return $this->_sendEmail($body, $email);
	}

	// SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record
	 *
	 * @param integer $accountId
	 * @return array
	 * @access public
	 */
	function get($accountId = 0) {
		if (!$accountId) {
			$accountId = $this->account_id;
		}

		// get account values
		$sql = "SELECT
			*
		FROM
			accounts
		WHERE
			account_id = " . $accountId;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		$row = $result->fetch_assoc();

		if (!empty($row)) {
			// convert string values to times
			$row["last_login_dt"] = strtotime($row["last_login_dt"]);
			$row["deleted_dt"] = strtotime($row["deleted_dt"]);
			$row["created_dt"] = strtotime($row["created_dt"]);
			$row["modified_dt"] = strtotime($row["modified_dt"]);
		}

		$result->close();

		return $row;
	}

	/**
	 * get the account settings
	 *
	 * @return array
	 * @access public
	 */
	function getSettings() {
		$row = $this->mysqli->one("account_settings", "account_id", $this->account_id);

		return $row;
	}

	// INSERT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 *
	 * @param array $args [email, password, username, display name]
	 * @return boolean
	 * @access public
	 */
	function create($args) {
		$reminder = $args["account_password"];
		$password = substr(md5($reminder . PASSWORD_SALT), 4, 16);

		$email = $args["account_email"];
		if ($this->emailExists($email)) {
			catchErr("The email address already exists.");
			return false;
		}

		$username = $args["account_username"];
		if ($this->usernameExists($username)) {
			catchErr("The username already exists.");
			return false;
		}

		$display = htmlentities($args["display_name"]);
		if ($display == "") {
			$display = $username;
		}

		// lock the tables
		$result = $this->mysqli->query("LOCK TABLES accounts WRITE");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// insert new account record
		$sql = "INSERT INTO accounts (
                account_email,
                account_password,
                account_remind,
                account_username,
                display_name,
                created_dt,
                modified_dt
            ) values (
                ?, ?, ?, ?, ?,
                NOW(),
                NOW()
            )";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("sssss", $email, $password, $reminder, $username, $display);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return true;
		}
		$stmt->close();

		// get the account_id
		$this->account_id = mysqli_insert_id($this->mysqli);

		// unlock the tables
		$result = $this->mysqli->query("UNLOCK TABLES");
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// update the account table with the login info
		$this->_updateLogin();

		// set the session
		session_set_cookie_params(3600);
		session_start();
		$_SESSION["account_id"] = $this->account_id;
		$_SESSION["account_status"] = 1;
		$_SESSION["display_name"] = $display;

		// send an email message
		$body = "Hello " . $display . "!\r\n";
		$body .= "Thank you for signing up with " . SITE_NAME . ".\r\n\n";
		$body .= "Your Username: " . $username . "\r\n";
		$body .= "Your Password: " . $reminder;

		$this->_sendEmail($body, $email);

		return true;
	}

	/**
	 * create a new account_addresses record
	 *
	 * @param array $args (address1, address2, city, state, country, zipcode)
	 * @return boolean
	 * @access public
	 */
	function createAddress($args) {
		$sql = "INSERT INTO account_addresses SET
                account_id       = ?,
                address_street_1 = ?,
                address_street_2 = ?,
                address_city     = ?,
                address_state    = ?,
                address_country  = ?,
                address_zipcode  = ?
            ";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("issssss", $this->account_id, $args["address1"], $args["address2"], $args["city"], $args["state"], $args["country"], $args["zipcode"]);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * create a new account_settings record
	 *
	 * @return boolean
	 * @access public
	 */
	function createSettings() {
		$sql = "INSERT INTO account_settings (
                account_id,
                created_dt
            ) VALUES (
                " . $this->account_id . ",
                (NOW())
            )";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		return true;
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing account
	 *
	 * @param array $args (account_email, account_username, account_password, display_name)
	 * @return boolean
	 * @access public
	 */
	function update($args) {
		// get the current email address and display name
		$sql = "SELECT
                account_email,
                display_name
            FROM
                accounts
            WHERE
                account_id = " . $this->account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$current = $result->fetch_object();
		$result->close();

		// build the sql values and email info
		$values = "";
		$body = "";

		// check if we need to update the email address
		if (isset($args["account_email"])) {
			if ($this->emailExists($args["account_email"])) {
				catchErr("That email already exists.");
				return false;
			}
			$values .= "account_email = '" . $args["account_email"] . "'";
			$body .= "Your New Email Address: " . $args["account_email"] . "\r\n";
		}

		// check if we need to update the username
		if (isset($args["account_username"])) {
			if ($this->usernameExists($args["account_username"])) {
				catchErr("That username already exists.");
				return false;
			}
			if (!empty($values)) {
				$values .= ", ";
			}
			$values .= "account_username = '" . $args['account_username'] . "'";
			$body .= "Your New Username: " . $args['account_username'] . "\r\n";
		}

		// check if we need to update the password
		if (isset($args["account_password"])) {
			$password = substr(md5($args["account_password"] . PASSWORD_SALT), 4, 16);

			if (!empty($values)) {
				$values .= ", ";
			}

			$values .= "account_remind = '" . $args['account_password'] . "', ";
			$values .= "account_password = '" . $password . "'";
			$body .= "Your New Password: " . $args['account_password'] . "\r\n";
		}

		// check if we need to update the display name
		if (isset($args["display_name"])) {
			$display = $args["display_name"];
			if (!empty($values)) {
				$values .= ", ";
			}
			$values .= "display_name = '" . htmlentities($args['display_name']) . "'";
		} else {
			$display = $current->display_name;
		}

		if (empty($values)) {
			catchErr("The account information you entered has not changed.");
			return false;
		}

		// update the account record
		$sql = "UPDATE
                accounts
            SET
                " . $values . ",
                modified_dt = (NOW())
            WHERE
                account_id = " . $this->account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// update the session display name
		$_SESSION["display_name"] = $display;

		// send an update email
		if (!empty($body)) {
			$msg = "Hello " . $display . ",\r\n\r\n";
			$msg .= SITE_NAME . " has received a request to update your account ";
			$msg .= "information.\r\n\r\n" . $body;

			$this->_sendEmail($msg, $current->account_email);
		}

		return true;
	}

	/**
	 * update an account_addresses record
	 *
	 * @param array $args (address1, address2, city, state, country, zipcode)
	 * @return boolean
	 * @access public
	 */
	function updateAddress($args) {
		$this->mysqli->escape($args);

		$sql = "UPDATE
                account_addresses
            SET
                address_street     = '" . $args["address1"] . "',
                address_street_ext = '" . $args["address2"] . "',
                address_city       = '" . $args["city"] . "',
                address_state      = '" . $args["state"] . "',
                address_country    = '" . $args["country"] . "',
                address_postal     = '" . $args["zipcode"] . "',
                modified_dt        = NOW()
            WHERE
                account_id = " . $this->account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		return true;
	}

	/**
	 * update settings for an existing account
	 *
	 * @param array $args
	 * @return boolean
	 * @access public
	 */
	function updateSettings($args) {
		// convert the array into sql values
		$values = "";
		foreach (array_keys($args) as $key) {
			$values .= $key . " = " . (int) $args[$key] . ",";
		}

		$sql = "UPDATE
                account_settings
            SET
                " . $values . "
                modified_dt = (NOW())
            WHERE
                account_id = " . $this->account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		return true;
	}

}