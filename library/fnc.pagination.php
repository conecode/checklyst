<?php
/** 
 * renders a pagination element
 * 
 * @param integer $total total number of pages
 * @param integer $page current page number
 * @return string
 */
function pagination($total, $page)
{
	$pages = "";

	if ($total > 1) {

		// start and stop defaults
		$start = 1;
		$stop  = $total;
		
		// shift the start and stop numbers depending on the number of pages
		if ($total > 9) {
			if ($page > 5) {
				$start = $page - 4;
			}
			$stop = $start + 8;
			if ($stop > $total) {
				$start = $total - 8;
				$stop  = $total;
			}
		}

		$pages .= "<ul id=\"pages\">";

		// add the "previous" link
		if ($page > 1) {
			$pages .= "<li onclick=\"gotoPage(" . ($page - 1) . ")\">&lt;&lt;</li>";
		} else {
			$pages .= "<li class=\"dis\">&lt;&lt;</li>";
		}

		// add the first page link
		if ($start > 1) {
			$pages .= "<li onclick=\"gotoPage(1)\">1</li>";
		}
		if ($start > 2) {
			$pages .= "<li class=\"gap\">...</li>";
		}

		// add the start and stop links
		for ($i = $start; $i <= $stop; $i++) {
			if ($i == $page) {
				$pages .= "<li class=\"cur\">" . $i . "</li>";
			} else {
				$pages .= "<li onclick=\"gotoPage(" . $i . ")\">" . $i . "</li>";
			}
		}

		// add the last page link
		if ($stop < ($total - 1)) {
			$pages .= "<li class=\"gap\">...</li>";
		}
		if ($stop < $total) {
			$pages .= "<li onclick=\"gotoPage(" . $total . ")\">" . $total . "</li>";
		}

		// add the "next" link
		if ($page < $total) {
			$pages .= "<li onclick=\"gotoPage(" . ($page + 1) . ")\">&gt;&gt;</li>";
		} else {
			$pages .= "<li class=\"dis\">&gt;&gt;</li>";
		}
		$pages .= "</ul>\n";
	}

	return $pages;
}
?>