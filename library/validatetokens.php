<?php

//connect to the APNS feedback servers
//make sure you're using the right dev/production server & cert combo!
$stream_context = stream_context_create(array('ssl' => array(
	   'local_cert' => 'prod.pem',
	   'passphrase' => 'dave1980'
	   )));

$apns = stream_socket_client('ssl://feedback.push.apple.com:2196', $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $stream_context);

if (!$apns) {
	echo "ERROR $errcode: $errstr\n";
	return;
}


$feedback_tokens = array();
//and read the data on the connection:
while (!feof($apns)) {
	$data = fread($apns, 38);
	if (strlen($data)) {
		$feedback_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
	}
}
fclose($apns);

if (!empty($feedback_tokens)) {
	var_dump($feedback_tokens);
}
