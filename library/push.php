<?php
date_default_timezone_set('Asia/Calcutta');
require_once 'ApnsPHP/Autoload.php';
$push = new ApnsPHP_Push(
ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
'ck.pem'
);

$push->setRootCertificationAuthority('ApnsPHP/entrust_root_certification_authority.pem');
$push->connect();
$message = new ApnsPHP_Message_Custom('e25129f6a24e514756e1c583dcdbc91af89029f14d7849ed0bb15866423bd811');
$message->setCustomIdentifier("Message-Badge-5");
$message->setText('Hello APNs-enabled device!');
$message->setBadge(5);
$message->setSound('default');
$message->setCustomProperty('acme2', array('bang', 'whiz'));
$message->setActionLocKey('Show me!');
$message->setExpiry(30);
$push->add($message);
$push->send();
$push->disconnect();
$aErrorQueue = $push->getErrors();
if (!empty($aErrorQueue)) {
var_dump($aErrorQueue);
}