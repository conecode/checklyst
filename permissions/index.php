<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projects.php');
require_once('/home1/checkly1/public_html/library/cls.access.php');

$my_account = new Account();
if (!$my_account->confirmSession()) {
	header('Location: /');
}

// get the current account
$my_account_id = (int)$_SESSION['account_id'];

// create the project and access objects
$my_project = new Projects($my_account->account_id);
$access  = new Access();

// get the specified id
$usr_account_id = getValue('prsn', 0);
$usr_account = new Account($usr_account_id);

// get a list of permissions for the specified account
if ($usr_account_id === 0) {
	$usr_access = Array();
	$usr_info = Array();
} else {
	$usr_access = $access->readAccess($usr_account_id);
	$usr_account = new Account($usr_account_id);
	$usr_info = $usr_account->get();
}

if ($_POST) {
	if ($_POST['formtype'] === 'Permissions') {
		// get a list of my access permissions
		$my_access = $access->readAccess($my_account_id);

		// loop through the submitted permissions
		foreach($_POST['project'] as &$project_id) {
			$radio = (int)$_POST[$project_id];
			// check if i have permission to edit permissions for the project
			if (($my_access[$project_id] === 'READ' AND $radio <= 1) OR
				($my_access[$project_id] === 'WRITE')) {
				$usr_prj_access = getValue($project_id, '', $usr_access);
				// edit the users permissions
				switch ($radio) {
				  case 0:
					if ($usr_prj_access !== '') {
						$access->remove($project_id, $usr_account_id);
					}
					break;
				  case 1:
					if ($usr_prj_access !== 'READ') {
						if ($usr_prj_access === '') {
							$access->create('READ', $project_id, $usr_account_id);
						} else {
							$access->update('READ', $project_id, $usr_account_id);
						}
					}
					break;
				  case 2:
					if ($usr_prj_access !== 'WRITE') {
						if ($usr_prj_access === '') {
							$access->create('WRITE', $project_id, $usr_account_id);
						} else {
							$access->update('WRITE', $project_id, $usr_account_id);
						}
					}
					break;
				} // end switch
			}
		} // end foreach

		// get an updated list of permissions for the specified account
		$usr_access = $access->readAccess($usr_account_id);

	} elseif ($_POST['formtype'] === 'Remove') {
		// remove the link between these two accounts
		$partner_id = (int)getValue('person');
		$access->removeLink($my_account_id, $partner_id);
		$usr_account_id = 0;
	}
}

// get a list of my projects
if (empty($usr_account_id)) {
	$projectCount = 0;
} else {
	$my_projects = $my_project->read('project_name', 'OWNER');
	$projectCount = count($my_projects);
}

// get a list of people i work with
$my_people = $access->read($my_account_id);

openPage('Manage People'); ?>
<style>
.buttongrp {
	text-align:right;
}
.buttongrp a {
	color:#666;
	font-size:12px;
	margin-right:12px;
}
.col_rad {
	width:100px;
}
.col_prj {
	width:180px;
}
.projectName {
	text-align:left;
}
#clearance {
	margin-bottom:16px;
}
#clearance td {
	border-bottom:1px solid #DDD;
	padding:8px 2px;
	text-align:center;
}
#clearance th {
	border-bottom:1px solid #DDD;
	font-size:12px;
	padding:2px;
	text-align:center;
}
#clearance label {
	display:block;
}
#permissionBox {
	margin-left:200px;
}
#permissionForm {
	background:#FFF;
	border:1px solid #CDCDCD;
	margin-bottom:10px;
	padding:20px;
}
.boxTitle {
	margin-bottom:18px;
}
.section {
	background:#FFF;
	border:1px solid #CDCDCD;
	padding:20px;
}
.section p {
	color:#666;
	margin-bottom:1.3em;
}
</style>
<?php
showBanner();
?>

	<div id="content">
		<div id="sideColumn">
			<!-- people -->
			<div class="sideBox">
				<h3>People</h3>
				<br class="clear" />
				<ul id="people">
<?php
$args = array('select'=>$usr_account_id, 'omit'=>$my_account_id);
showPeople($my_people, $args, false) ?>
				</ul>
			</div>
		</div>
		<div id="permissionBox">
			<h1><?php echo getValue('display_name', 'View People', $usr_info) ?></h1>
<?php
	if ($usr_account_id > 0) { ?>
			<form id="permissionForm" action="/permissions/" method="post">
				<h3 class="boxTitle">Permissions</h3>
				<input name="prsn" type="hidden" value="<?php echo $usr_account_id ?>" />
				<table id="clearance">
					<colgroup><col class="col_prj"><col class="col_rad"><col class="col_rad"><col class="col_rad"></colgroup>
					<thead><tr><th>Project</th><th>None</th><th>Read Only</th><th>Read & Write</th></tr></thead>
					<tbody>
<?php	
		for ($i = 0; $i < $projectCount; $i++) {
			$project_id  = $my_projects[$i]['project_id'];
			$accessvalue = getValue($project_id, '', $usr_access); ?>
					<tr>
						<td>
							<p class="projectName"><?php echo $my_projects[$i]['project_name'] ?></p>
							<input name="project[]" type="hidden" value="<?php echo $project_id ?>" />
						</td>
						<td><label><input <?php
								if ($accessvalue == '') {
									echo 'checked="checked" ';
								} ?>name="<?php echo $project_id ?>" type="radio" value="0" /></label></td>
						<td><label><input <?php
								if ($accessvalue == 'READ') {
									echo 'checked="checked" ';
								} ?>name="<?php echo $project_id ?>" type="radio" value="1" /></label></td>
<?php		if ($my_projects[$i]['permission'] == 'WRITE') { ?>
						<td><label><input <?php
								if ($accessvalue == 'WRITE') {
									echo 'checked="checked" ';
								} ?>name="<?php echo $project_id ?>" type="radio" value="2" /></label></td>
<?php		} else { ?>
						<td>N/A</td>
<?php		} ?>
					</tr>
<?php	} ?>
					</tbody>
				</table>
				<div>
					<input name="formtype" type="hidden" value="Permissions" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Save Changes</span></span></button>
				</div>
			</form>
			<form class="section" action="/permissions/" method="post">
				<h3 class="boxTitle">Remove</h3>
				<p>Click the button below to remove this person from your list of people.</p>
				<div>
					<input name="formtype" type="hidden" value="Remove" />
					<input name="person" type="hidden" value="<?php echo $usr_account_id ?>" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Remove This Person</span></span></button>
				</div>
			</form>
<?php
	} else {
		if (count($my_people) > 0) { ?>
			<div class="whiteBox">
				<img src="/images/arrow.png" width="44" height="47" alt="<--" />
				<p>Select a name on the left to edit permissions for that person.</p>
			</div>
<?php
		} else { ?>
			<div class="whiteBox">
				<img src="/images/icn_info.png" width="50" height="50" alt="Information:" />
				<p>You haven't invited any users to access your projects.<br /><br />
					Click <a href="/invite">here</a> to invite people.</p>
			</div>
<?php
		}
	} ?>
		</div>
	</div><!-- content -->
<?php showFooter(); ?>
<script src="/js/checklyst1_1.js"></script>
<?php closePage(); ?>