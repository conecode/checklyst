/* MarcGrabanski.com */
function popCalendar(){
	var e = $(this).prev('input'),
		d = strToDate(e.val());
	calendar.month = d.getMonth();
	calendar.year  = d.getFullYear();
	calendar.draw(e[0]);
}

var calendar = {
	$div:  null,
	month: null,
	pick:  null,
	today: new Date(),
	year:  null,
	open:  0,

	load: function () {
		var e = $('<div/>').attr({id:'calBox'});
		$('body').append(e);
		calendar.$div = e;
	},

	draw: function (inputObj) {
		var cal = this;

		// start the calendar
		var html = '';
		html = '<a class="popClose" id="calClose">&nbsp;</a><a id="calNow">Set To Now</a>'
			+ '<p id="calMonth"><a id="prevMonth">&lt;</a> ' + getMonthName(cal.month)
			+ ' '+cal.year+' <a id="nextMonth">&gt;</a></p>'
			+ '<table id="calendar"><thead><tr><th>Sun</th><th>Mon</th><th>Tue</th>'
			+ '<th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr></thead><tbody>';
		var daysInMonth = getDaysInMonth(cal.year, cal.month),
			startDay  = getFirstDayOfMonth(cal.year, cal.month),
			numRows   = 6,
			blankDays = (startDay < 6) ? startDay+1 : 0,
			printDate = 1,
			thisDate  = cal.today.getMonth() + "/" + cal.today.getDate() + "/" +
						cal.today.getFullYear();

		// create calendar days
		for (var i = 0; i < numRows; i++) {
			html += (i == numRows) ? '<tr>' : '<tr class="mid">';
			for (var j = 0; j < 7; j++) {
				var tst = cal.month + "/" + printDate + "/" + cal.year;
				html += (tst == thisDate) ? '<td id="calToday">' : '<td>';
				if (blankDays == 0) {
					html += (printDate <= daysInMonth) ? '<a>'+printDate+'</a>' : '&nbsp;';
					printDate++;
				}
				if(blankDays > 0) {
					html += '&nbsp;';
					blankDays--;
				}
				html += '</td>';
			}
			html += '</tr>';
		}
		html += '</tbody></table>';

		// insert the calendar
		cal.$div.html(html);
		cal.position(inputObj);

		// setup the close links
		$(document).bind('mousedown.calendar', cal.close);
		$(document).bind('keyup.calendar', function(e) {
			if (e.keyCode == 27) cal.close();
		});
		$('#calBox').mousedown(function(e){ e.stopPropagation(); });

		// set the month links
		$('#calClose').click(cal.close);
		$('#prevMonth').click(function () {
			cal.month--;
			if (cal.month < 0) {
				cal.month = 11;
				cal.year--;
			}
			cal.draw(inputObj);
		}, false);
		$('#nextMonth').click(function () {
			cal.month++;
			if (cal.month > 11) {
				cal.month = 0;
				cal.year++;
			}
			cal.draw(inputObj);
		}, false);
		$('#calNow').click(function () {
			cal.month = cal.today.getMonth();
			cal.year  = cal.today.getFullYear();
			cal.draw(inputObj);
		}, false);
		cal.events(inputObj);
	},

	close: function() {
		calendar.$div.css({left:-9999, top:0});
		$(document).unbind('.calendar');
	},

	// link events to calendar days
	events: function (inputObj) {
		var cal = this;

		$('a', '#calendar').click(function () {
			cal.$div.css({left:-9999, top:0});
			cal.pick = this.innerHTML;
			if (cal.year !== cal.today.getFullYear()) {
				inputObj.value = formatDate(cal.pick, cal.month, cal.year);
			} else {
				inputObj.value = formatDate(cal.pick, cal.month, false);
			}
		}, false);
	},

	// position the calendar next to the input
	position: function (inputObj) {
		var pos = $(inputObj).offset(),
			hgt = calendar.$div.height(),
			mov = (($(document).height()-pos.top) < hgt+40) ? -hgt : 23;
		calendar.$div.css({left:pos.left-80, top:pos.top+mov});
	}
}

function formatDate(Day, Month, Year) {
	Month = getMonthName(Month, true);
	Day   = (Day < 10) ? ' 0' + Day : ' ' + Day;
	Year  = (Year) ? ', ' + Year : '';
	return Month + Day + Year;
}

function getMonthName(month, abbr) {
	var monthNames = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	if (abbr) {
		return monthNames[month].substr(0,3);
	} else {
		return monthNames[month];
	}
}

function getDaysInMonth(year, month) {
	return 32 - new Date(year, month, 32).getDate();
}

function getFirstDayOfMonth(year, month) {
	return new Date(year, month, 0).getDay();;
}

function strToDate(sDate, r) {
	var d = (sDate == '') ? new Date() : new Date(sDate);
	var s = (d.getFullYear() == 2000) ? "NaN" : d.toString();
	if (s == "NaN" || s == "Invalid Date") {
		var tmp = new Date();
		d = (r) ? tmp : strToDate(sDate + ', ' + tmp.getFullYear(), true);
	}
	return d;
}

// Add calendar events
$(document).ready(calendar.load);
