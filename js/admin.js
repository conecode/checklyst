function account_goTo(num) {
	var frm = document.getElementById("sort_form");
	frm.page.value = num;
	frm.submit();
}
function account_sortColumn(col) {
	var frm = document.getElementById("sort_form");
	frm.column.value = col;
	frm.direct.value = "asc";
	frm.submit();
}
function account_sortDirection(dir) {
	var frm = document.getElementById("sort_form");
	frm.direct.value = dir;
	frm.submit();
}
function account_view(id) {
	window.location = "account_view.php?id=" + id;
}