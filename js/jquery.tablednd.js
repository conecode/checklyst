/**
 * TableDnD plug-in for JQuery, allows you to drag and drop table rows.
 * original code: http://www.isocra.com/2008/02/table-drag-and-drop-jquery-plugin/
 */
(function ($) {
	$.fn.tableDnD = function(settings) {
		var config = {
			onDragClass: 'dragging',
			onStart: null,
			onDrop: null,
			scrollAmount: 8,
			dragHandle: 'cell_drag'
		};

		if (settings) $.extend(config, settings);

		this.each(function() {

			var $table = $(this),

				dragObject  = null,
				mouseOffset = null,
				oldY = null,

				dragId = null,
				dropId = null,
				oldIndex = null,

				makeDraggable = (function() {
					var cells = $("td." + config.dragHandle, $table);
					cells.each(function() {
						$(this).mousedown(function(ev) {
							$table.bind('mousemove.tablednd', mousemove);
							$(document).bind('mouseup.tablednd', mouseup);
							dragObject  = this.parentNode;
							mouseOffset = getMouseOffset(this, ev);
							oldIndex    = $(dragObject).index();
							if (config.onStart) {
								config.onStart($table, this);
							}
							return false;
						});
					})
				})(),

				mouseCoords = function(ev){
					if(ev.pageX || ev.pageY){
						return { x:ev.pageX, y:ev.pageY };
					}
					return {
						x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
						y:ev.clientY + document.body.scrollTop  - document.body.clientTop
					};
				},

				getMouseOffset = function(target, ev) {
					ev = ev || window.event;

					var docPos    = getPosition(target);
					var mousePos  = mouseCoords(ev);
					return { x:mousePos.x - docPos.x, y:mousePos.y - docPos.y };
				},

				getPosition = function(e){
					var left = 0;
					var top  = 0;
					if (e.offsetHeight == 0) {
						e = e.firstChild; // a table cell
					}

					while (e.offsetParent){
						left += e.offsetLeft;
						top  += e.offsetTop;
						e     = e.offsetParent;
					}

					left += e.offsetLeft;
					top  += e.offsetTop;

					return { x:left, y:top };
				},

				mousemove = function(ev) {
					if (dragObject == null) { return false; }

					var $dragObj = $(dragObject);
					var mousePos = mouseCoords(ev);
					var y = mousePos.y - mouseOffset.y;

					var yOffset = window.pageYOffset;
					if (document.all) {

						if (typeof document.compatMode != 'undefined' &&
							document.compatMode != 'BackCompat') {
							yOffset = document.documentElement.scrollTop;
						}
						else if (typeof document.body != 'undefined') {
							yOffset = document.body.scrollTop;
						}

					}

					if (mousePos.y-yOffset < config.scrollAmount) {
						window.scrollBy(0, -config.scrollAmount);
					} else {
						var windowHeight = window.innerHeight ? window.innerHeight
						: document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight;
						if (windowHeight-(mousePos.y-yOffset) < config.scrollAmount) {
							window.scrollBy(0, config.scrollAmount);
						}
					}

					if (y != oldY) {
						var movingDown = y > oldY;

						oldY = y;
						if (config.onDragClass) {
							$dragObj.addClass(config.onDragClass);
						}

						var currentRow = findDropTargetRow($dragObj, y);
						if (currentRow) {
							if ($(currentRow).parent().get(0).tagName.toLowerCase() == dragObject.parentNode.tagName.toLowerCase() &&
								dragObject != currentRow)
								{
								if (movingDown) {
									dragObject.parentNode.insertBefore(dragObject, currentRow.nextSibling);
								} else {
									dragObject.parentNode.insertBefore(dragObject, currentRow);
								}
								dropId = currentRow.id.substr(3);
							}
						}
					}

					return false;
				},

				findDropTargetRow = function(draggedRow, y) {
					var rows = $table[0].rows;
					for (var i=0; i<rows.length; i++) {
						var row = rows[i];
						var rowY = getPosition(row).y;
						var rowHeight = parseInt(row.offsetHeight)/2;
						if (row.offsetHeight == 0) {
							rowY = getPosition(row.firstChild).y;
							rowHeight = parseInt(row.firstChild.offsetHeight)/2;
						}
						if ((y > rowY - rowHeight) && (y < (rowY + rowHeight))) {
							if (row == draggedRow) {
								return null;
							}
							if (config.onAllowDrop) {
								if (config.onAllowDrop(draggedRow, row)) {
									return row;
								} else {
									return null;
								}
							} else {
								var nodrop = $(row).hasClass("nodrop");
								if (! nodrop) {
									return row;
								} else {
									return null;
								}
							}
							return row;
						}
					}
					return null;
				},

				mouseup = function() {
					if (dragObject) {
						var $row = $(dragObject);
						if (config.onDragClass) {
							$row.removeClass(config.onDragClass);
						}
						dragId = dragObject.id.substr(3);
						dragObject = null;
						$table.unbind('.tablednd');
						$(document).unbind('.tablednd');
						if (config.onDrop && oldIndex != $row.index()) {
							config.onDrop(dragId, dropId);
						}
					}
				};

			$table.one('disableDnD', function() {
				$("td." + config.dragHandle, $(this)).unbind('mousedown');
			});
		});
		return this;
	};

	$.fn.disableDnD = function () {
		return this.trigger('disableDnD');
	};
})(jQuery);