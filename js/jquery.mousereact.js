(function($) {

	$.fn.mouseReact = function(settings) {

		var config = { setHover:  '', setActive: '' };

		if (settings) $.extend(config, settings);

		this.each(function() {

			var tag = $(this);

			if (config.setHover !== '') {
				tag.hover(
					function(){tag.addClass(settings.setHover)},
					function(){tag.removeClass(settings.setHover)}
				);
			}

			if (config.setActive !== '') {
				tag.mousedown(function () {
					tag.addClass(settings.setActive)
				});
				tag.mouseup(function () {
					tag.removeClass(settings.setActive)
				});
			}
		});

		return this;
	};
})(jQuery);