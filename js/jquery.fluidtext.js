/*
 * fluidText plugin for jQuery
 * version 1.1
 *
 * $('textarea').fluidText({ timeSpan:300, maxHeight: 500 });
 */
(function($) {
	$.fn.fluidText = function(settings) {
		var config = {
			timeSpan : 0,
			maxHeight: 1000,
			onResize : function(){}
		};

		if (settings) $.extend(config, settings);

		this.each(function() {
			var textarea = $(this),

				// get the height from the textarea as a base starting point
				baseHeight = textarea.height(),

				// keep track of the previous height
				lastHeight = null,

				// define the analog to the textarea
				analog = (function() {
					return $('<textarea/>')
						.attr({ cols:50, rows:1, tabindex:-1 })
						.css({ position:'absolute', left:-9999, top:0,
							   padding:0, 'overflow-y':'hidden',
							   height:textarea.css('height'),
							   lineHeight:textarea.css('lineHeight'),
							   fontFamily:textarea.css('fontFamily'),
							   fontSize:textarea.css('fontSize') })
						.width(textarea.width())
						.insertAfter(textarea);
				})(),

				// update the analog width to match the textarea
				adjust = function() {
					analog.width(textarea.width());
					stretch();
				},

				// stretch the height of the textarea to match the analog
				stretch = function() {

					// get the scroll height of the analog
					analog.height(0).val(textarea.val() + ' _');
					var analogTop = analog[0].scrollHeight;
					var setHeight = Math.max(analogTop, baseHeight);

					// verify the height had changed
					if (lastHeight === setHeight) { return; }
					lastHeight = setHeight;

					// verify the height isn't larger than the specified maximum
					if (setHeight >= config.maxHeight) {
						textarea.css('overflow-y', '');
						return;
					}

					// change the height of the textarea
					if (config.timeSpan > 0) {
						textarea.animate({height:setHeight}, config.timeSpan);
					} else {
						textarea.height(setHeight);
					}

					config.onResize(textarea);
				};

			textarea.one('updateFluidText', function() { adjust() });

			textarea.unbind('.fluidtext')
					.bind('keyup.fluidtext', stretch)
					.bind('keydown.fluidtext', stretch)
					.bind('change.fluidtext', stretch);

			// stretch when initialized
			stretch();
		});
		return this;
	};

	$.fn.updateFluidText = function () {
		return this.trigger('updateFluidText');
	};
})(jQuery);
