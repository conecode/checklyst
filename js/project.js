function ajaxError(rtn) {
	if (rtn.substr(0, 17) == '<div id="errors">') {
		var el = $('#errors');
		if (el.length != 0) el.remove();
		$('body').after(rtn);
		return true;
	}
	return false;
}

/* Project ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
var project = {
	ajaxFile: '/library/ajx.project.php',
	currHTML: '',
	currName: '',
	id: 0,
	create: function () {
		$('#prjform').attr('action', 'javascript:return false;');
		var nam = $('#prjname').val();
		$.post(project.ajaxFile, {'act':'cp', 'cur':project.id, 'nam':nam},
			function(rtn) {
				if (!ajaxError(rtn)) {
					$('#projects').html(rtn);
					var el = $('#nolists');
					if (el.hasClass('hidden') == false) {
						el.addClass('hidden');
						$('#noselect').removeClass('hidden');
					}
				}
			}
		);
		$(document).unbind('.project');
	},
	remove: function () {
		$.post(project.ajaxFile, {'act':'tp', 'prj':project.id},
			function(rtn) {if (!ajaxError(rtn)) window.location = '/lists/'}
		);
	},
	edit: function () {
		var nam = $('#prjname').val();
		if (nam !== project.currName) {
			$.post(project.ajaxFile, {'act':'up', 'prj':project.id, 'nam':nam},
				function (rtn) {project.editrtn(rtn, nam)}
			);
		} else {
			$('#prj' + project.id).html(project.currHTML);
		}
		$(document).unbind('.project');
	},
	editrtn: function (rtn, nam) {
		if (ajaxError(rtn)) {
			$('#prj' + project.id).html(project.currHTML);
		} else {
			$('#projects').html(rtn);
		}
	},
	form: function (act, val) {
		return '<form id="prjform" action="javascript:project.' + act +
			'()" autocomplete="off"><div><input type="text" id="prjname" value="' +
			val + '" maxlength="128" onblur="this.form.submit();" /></div></form>';
	},
	setcreate: function () {
		var frm = '<li id="createproject">' +
			project.form('create', 'Untitled Project') + '</li>';
		if ($('#no_projects').length !== 0) {
			$('#projects').html(frm);
		} else {
			$('#projects li:last-child').after(frm);
		}
		$(document).bind('keyup.project', function(e) {
			if (e.keyCode == 27) {
				$('#prjform').attr('action', 'javascript:return false;');
				$('#createproject').remove();
				if ($('#projects li').length === 0) {
					$('#projects').html('<li id="no_projects">Not available</li>');
				}
				$(document).unbind('.project');
			}
		});
		$('#prjname').select();
	},
	setedit: function () {
		var el = $('#prj' + project.id);
		project.currName = $.trim($('> a', el).html());
		project.currHTML = el.html();
		el.html(project.form('edit', project.currName));
		$(document).bind('keyup.project', function(e) {
			if (e.keyCode == 27) {
				$('#prjform').attr('action', 'javascript:return false;');
				$('#prj' + project.id).html(project.currHTML);
				$(document).unbind('.project');
			}
		});
		$('#prjname').select();
	},
	noaccess: function () {
		var ajaxFile = '/library/ajx.access.php';
		if (confirm ('Are you sure you want to permanently remove your access to this project?')) {
			$.post(ajaxFile, {'act':'ra', 'prj':project.id},
				function(rtn) {if (!ajaxError(rtn)) window.location = '/lists/'}
			);
		}
	}
}
/* List :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
var list = {
	ajaxFile: '/library/ajx.projectlist.php',
	colNames: new Array('','','txt','due'),
	currHTML: '',
	item: null,
	priorities: null,
	adjust: function (nam, val) {
		$('#' + nam).text(function (i, str){return parseInt(str) + val;});
	},
	create: function () {
		var f = $('#newitemForm'),
			s = f.serialize();
		$.post(list.ajaxFile, 'act=ci&prj=' + project.id + '&' + s,
			function(rtn) {
				if (!ajaxError(rtn)) {
					var jsn = $.parseJSON(rtn);
					list.inject(jsn['ht']);
					list.priorities[jsn['id']] = jsn['pr'];
					f[0].reset();
					$('#edt_text_new').keyup().focus();
				}
			}
		);
	},
	remove: function (itm) {
		if (confirm('Are you sure you want to delete this item?')) {
			$.post(list.ajaxFile, {'act':'ri', 'prj':project.id, 'itm':itm},
				function(rtn) {
					if (!ajaxError(rtn)) {
						var r = $('#itm' + rtn),
							n = $('input', r).val(),
							l = $('#listbody');
						r.remove();
						if (l.find('tr').length === 0) {
							$('#listEdit').removeClass('bg_white').addClass('bg_clear');
							$('#list').addClass('hidden');
							$('#noitems').removeClass('hidden');
						}
						list.adjust('total', -1);
						if (n == 1) list.adjust('done', -1);
					}
				}
			);
		}
	},
	edit: function () {
		var f = $('#listEdit'),
			t = $('[name=itm]', f).val(),
			s = f.serialize();
		$.post(list.ajaxFile, 'act=ui&prj=' + project.id + '&' + s,
			function(rtn) {
				if (!ajaxError(rtn)) {
					$('#itm' + t).html(rtn);
					$('#toolbar_row').parent().remove();
					$('#list').tableDnD({onDrop: function(drag, drop) {list.move(drag ,drop)}});
					list.item = null;
				}
			}
		);
	},
	move: function (drag, drop) {
		var o = list.priorities[drag],
			n = list.priorities[drop];
		$.post(list.ajaxFile,
			{'act':'mi', 'prj':project.id, 'itm':drag, 'old':o, 'new':n},
			function(rtn) {
				if (!ajaxError(rtn)) {list.priorities = eval("(" + rtn + ")")}
			}
		);
	},
	form: function (itm) {
		var frm = '';
		$('#itm' + itm + ' td').each(function() {
			var s = '';
			switch($(this).attr('class')) {
				case 'cell_drag':
					frm += '<td></td>';
					break;
				case 'cell_check':
					frm += '<td><input name="itm" value="' + itm + '" type="hidden" /></td>';
					break;
				case 'cell_action':
					frm += '<td class="cell_action"><input value="Save" type="submit" /><button onclick="list.cancel(' + itm + '); return false;">X</button></td>';
					break;
				case 'cell_text':
					s = $(this).children().html();
					s = s.replace(/(<.*?>)/ig, "");
					frm += '<td><div class="edt_box"><textarea id="edt_text" class="edt_text" name="txt" cols="60" rows="1">' + s.replace(/"/g, '&quot;') + '</textarea></div></td>';
					break;
				default:
					frm += '<td><input class="edt_input" name="due" value="' + $(this).children().html() + '" maxlength="18" type="text" /><div class="calButton"></div></td>';
			}
		});
		var frm2 = '<tr><td colspan="4" id="toolbar_row"><button class="button" onclick="list.cancel()" type="button"><span class="action-l"><span class="action-r">Cancel</span></span></button><button class="button" type="submit"><span class="default-l"><span class="default-r">Save</span></span></button></td></tr>';
		return [frm, frm2];
	},
	inject: function (str) {
		var el = $('#noitems'),
			$b = $('#bundle');
		if (el.hasClass('hidden') == false) {
			el.addClass('hidden');
			$('#listEdit').removeClass('bg_clear').addClass('bg_white');
			$('#list').removeClass('hidden');
			$('#listbody').html(str);
		} else {
			$('#listbody tr:last-child').after(str);
			$('#list').tableDnD({onDrop: function(drag, drop) {list.move(drag ,drop)}});
		}
		$b.scrollTop($b.attr('scrollHeight'));
		list.adjust('total', 1)
	},
	cancel: function() {
		$('#itm' + list.item).html(list.currHTML);
		$('#toolbar_row').parent().remove();
		$('#list').tableDnD({onDrop: function(drag, drop) {list.move(drag ,drop)}});
	},
	setedit: function (itm) {
		if (list.item) {
			list.cancel(list.item)
		}
		list.item = itm;

		var $b = $('#bundle'),
			$e = $('#itm' + itm),
			f = list.form(itm),
			p = $e.offset();

		list.currHTML = $e.html();
		$e.html(f[0]).after(f[1]);
		$('.calButton').click(popCalendar);
		$('#edt_text').fluidText();
		$('#list').disableDnD();
		if (p.top + 150 >= $(document).height()) {
			$b.scrollTop($b.attr('scrollHeight'));
		}
	},
	strike: function (el, itm) {
		var btn = $(el),
			inp = btn.prev('input'),
			set = (inp.val() == 1) ? 0 : 1;
		$.post(list.ajaxFile,{'act':'si', 'prj':project.id, 'itm':itm, 'set':set},
			function(rtn) {
				if (!ajaxError(rtn)) {
					var r = $('#itm' + itm);
					if (set == 1) {
						r.addClass('strike');
						inp.val(set);
						list.adjust('done', 1);
					} else {
						r.removeClass('strike');
						inp.val(set);
						list.adjust('done', -1);
					}
				}
			}
		);
	}
}
/* Toggle New Item Form :::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
$('#toggleForm').toggle(
	function () {
		var $b = $('#bundle'),
			$f = $('#newitemForm'),
			$p = $('#push');

		$(this).addClass('btn_form');
		$f.css('display', 'block');
		$f.html($f.html()); // IE7 fix
		$p.css('display', 'block');
		$('#edt_text_new').fluidText({onResize: function(){ 
				$b.scrollTop($b.attr('scrollHeight'));
				var hgt = $f.height() + 5;
				$p.height(hgt);
				$('#listEdit').css('margin-bottom', '-' + hgt + 'px');
			}
		}).focus();
		$('.calButton').click(popCalendar);
		$b.scrollTop($b.attr('scrollHeight'));
	},
	function () {
		$(this).removeClass('btn_form');
		$('#edt_text_new').css('height', 'auto');
		$('#newitemForm').css('display', 'none');
		$('#push').css('display', 'none');
	}
);
/* Actions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
var actions = {
	$box: $('#popBox'),
	view: function (e, i) {
		var r = $(e),
			p = r.position();
		actions.$box.css({
			left: p.left + 160,
			top:  p.top + 14 + (r.height()/2)
		});
		actions.open = i;
		$(document).bind('mousedown.actions', actions.hide);
		$(document).bind('keyup.actions', function(e) {
			if (e.keyCode == 69) actions.prep();
			if (e.keyCode == 68) actions.wipe();
			if (e.keyCode == 27) actions.hide();
		});
		actions.$box.mousedown(function(e){e.stopPropagation();});
		return true;
	},
	hide: function () {
		actions.$box.css({left: -9999, top: 0});
		$(document).unbind('.actions');
	},
	prep: function () {
		actions.hide();
		list.setedit(actions.open);
	},
	wipe: function () {
		actions.hide();
		list.remove(actions.open);
	}
}
/* Menu :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
var menu = {
    open: 0,
	$cur: null,
	click: function (e, i) {
		var opn = menu.open;
		if (opn > 0) {
			menu.hide();
		}
		if (opn != i) {
			menu.$cur = $('#menu' + i);
			menu.show(i);
			if (e.className == 'button') {
				$(e).children().addClass('btn_open');
			}
		}
	},
	close: function (e) {
		var s = (typeof event!=='undefined')? event.srcElement : e.target;
		if (s.tagName == 'SPAN') {s = s.parentNode}
		if ((s.accessKey == null || s.accessKey == '') && menu.open != 0) {
			menu.hide();
		}
	},
	hide: function () {
		menu.$cur.css('visibility', 'hidden');
		$('.btn_open').removeClass('btn_open');
		$(document).unbind('.menu');
		menu.open = 0;
	},
	show: function (i) {
		menu.$cur.css('visibility', 'visible');
		$(document).bind('click.menu', menu.close);
		$(document).bind('keyup.menu', function(e) {
			if (e.keyCode == 27) menu.hide();
		});
		menu.open = i;
	}
}
/* Dialog Box :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
var dialog = {
	$box: $('#dialog'),

	hide: function () {
		dialog.$box.css({left: -9999});
	},
	show: function (htm) {
		if (dialog.$box.length === 0) {
			dialog.$box = $(document.createElement('div'))
				  .attr('id', 'dialog');
			$('#footer').after(dialog.$box);
		}
		var Y = $(window).scrollTop() + 120;
		dialog.$box.html(htm).css({left: '50%', top: Y});
	}
}
/* Trash ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
var trash = {
	ajaxFile: '/library/ajx.archive.php',

	restore: function (prj) {
		$.post(trash.ajaxFile, {'act':'tp', 'prj':prj, 'cur':project.id},
			function(rtn) {
				if (!ajaxError(rtn)) {
					if ($('#trashBody tr').length == 1) {
						trash.show();
					} else {
						$('#trsh' + prj).remove();
					}
					$('#projects').html(rtn);
				}
			}
		);
	},
	remove: function (prj) {
		var sum = '', rslt = false, num = 1;
		if (prj == 'ALL') {
			rslt = confirm ('Are you sure you want to permanently delete all the Projects in the Achive?');
			sum  = 'ALL';
		} else {
			rslt = confirm('Are you sure you want to permanently delete this Project?');
			sum  = 'ONE';
			num  = $('#trashBody tr').length;
		}
		if (rslt) {
			$.post(trash.ajaxFile, {'act':'op', 'prj':prj, 'sum':sum},
				function(rtn) {
					if (!ajaxError(rtn)) {
						if (num == 1) {
							trash.show();
						} else {
							var lst = rtn.split(',');
							for(var i = 0, m = lst.length; i < m; i++) {
								$('#trsh' + lst[i]).remove();
							}
						}
					}
				}
			);
		}
	},
	show: function () {
		$.post(trash.ajaxFile, {'act':'gt'},
			function(rtn) {
				if (!ajaxError(rtn)) {
					dialog.show(rtn);
				}
			}
		);
	}
}
/* Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
var settings = {
	ajaxFile: '/library/ajx.settings.php',

	form: function (n) {
		var s1 = $('#set-1'),
			s2 = $('#set-2');

		if (n === 1) {
			s2.hide();s1.show();
		} else {
			s1.hide();s2.show();
		}

		$('#dia-tabbar a').removeClass('selected');
		$('#tab-' + n).addClass('selected');
	},
	show: function () {
		$.post(settings.ajaxFile, {'act':'get'},
			function(rtn) {
				if (!ajaxError(rtn)) {
					dialog.show(rtn);
				}
			}
		);
	}
}
