<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('templates/tpl.default.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

if ($_POST) {
	$username = $_POST['username'];
	$password = $_POST['password'];
	$cookie = (empty($_POST['cookie'])) ? false : true;

	if (validLoginForm($username, $password)) {

		if ($account->login($username, $password, $cookie)) {
			header('Location: /lists/');
		}
	}
}
$loggedin = $account->confirmSession();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Welcome to Checklyst</title>
    <meta name="apple-itunes-app" content="app-id=986959770, affiliate-data=, app-argument=">
	<meta charset="utf-8" />
	<link rel="stylesheet" href="/templates/front-page.css" />
</head>
<body>
	<div id="header">
		<div class="bundle">
			<h1><a class="sitename" href="/">checklyst</a></h1>
			<h2 class="tagname">Organize your life. Anywhere.</h2>
<?php if (!$loggedin) { ?>
			<a class="boldlink" href="/account/register.php">Create an Account...</a>
			<!-- form to login -->
			<div id="login_form" class="login">
				<form action="index.php" method="post">
					<h3>Login</h3>
					<div class="inputgrp">
						<label class="inputlbl" for="username">Username:</label>
						<input class="input" id="username" maxlength="50" name="username" size="25" type="text" value="<?php 
							echo getValue('username') ?>" />
					</div>
					<div class="inputgrp">
						<label class="inputlbl" for="password">Password:</label>
						<input class="input" id="password" maxlength="32" name="password" size="25" type="password" />
					</div>
					<div class="checkgrp">
						<label class="checklbl"><input <?php
								if (getValue('cookie', false)) {
									echo 'checked="checked" ';
								}
							?>class="check" name="cookie" type="checkbox" />Remember me</label>
					</div>
					<div class="buttongrp">
						<a class="loginlink" href="javascript:gotoRetrieveForm()">Lost Username or Password?</a>
						<button class="button" type="submit"><span class="default-l"><span class="default-r">Login</span></span></button>
					</div>
				</form>
			</div>
			<!-- form to retrieve password -->
			<div id="retrieve_form" class="login">
				<form action="/account/help.php" method="post">
					<h3>Retrieve Password</h3>
					<div class="inputgrp">
						<label class="inputlbl" for="send_email">Email Address:</label>
						<input class="input" id="send_email" maxlength="50" name="send_email" size="25" type="text" value="<?php 
							echo getValue('send_email') ?>" />
					</div>
					<div class="buttongrp">
						<a class="loginlink" href="javascript:gotoLoginForm()">Back To Login</a>
						<button class="button" type="submit"><span class="action-l"><span class="action-r">Send</span></span></button>
					</div>
				</form>
			</div>
<?php } else { ?>
			<div id="welcomeBox">
				<h3>Welcome Back!</h3>
				<a class="boldlink" href="/lists/">Open My Lysts...</a>
			</div>
<?php } ?>
		</div>
	</div>
	<div id="screenshot">
		<img src="/images/screen-shot-main.jpg" width=868"" height="326" />
	</div>
	<div id="moreinfo" class="bundle">
		<div class="feature">
			<h4>Easy To Use</h4>
			<img src="/images/screen-shot-ui.png" width="260" height="160" />
			<p>Checklyst has a clean and simple user interface that's based off the look and feel of many OS X applications.</p>
		</div>
		<div class="feature">
			<h4>Invite People</h4>
			<img src="/images/screen-shot-people.png" width="260" height="160" />
			<p>Invite your colleagues or friends to help you create, build, and complete your projects.</p>
		</div>
		<div class="feature">
			<h4>iPhone App</h4>
			<img src="/images/screen-shot-iphone.png" width="260" height="160" />
			<p>Edit lysts and check tasks on your iPhone with the <a href="http://itunes.apple.com/us/app/checklyst/id449657536?mt=8">Checklyst app</a> that is now available in the Apple App Store.</p>
		</div>
	</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script>
<?php if (!$_POST) { echo "$('#username').focus();\n"; } ?>
function gotoRetrieveForm() {
	$('#login_form').hide();
	$('#retrieve_form').show();
	$('#send_email').focus();
}
function gotoLoginForm() {
	$('#retrieve_form').hide();
	$('#login_form').show();
	$('#username').focus();
}
</script>
<?php closePage(); ?>