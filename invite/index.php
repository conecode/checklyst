<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projects.php');
require_once('/home1/checkly1/public_html/library/cls.invite.php');
require_once('/home1/checkly1/public_html/library/cls.access.php');
require_once('/home1/checkly1/public_html/library/fnc.validate.php');

// get the invite code
$code = getValue('code');

$account = new Account();
$loggedin = $account->confirmSession();

if (empty($code)) {
	if (!$loggedin) {
		header('Location: /');
	} else {
		header('Location: step1');
	}
}

// verify the code is only 8 digits;
$invite = new Invite();
if (validInput($code, 8, 8)) {
	$info = $invite->get($code);
} else {
	catchErr('Error processing invitation code.');
}

if ($_POST) {
	$userOk = FALSE;


	switch ($_POST['formtype']) {
	 case 'register':
		// creating a new account
		if(validRegisterForm($_POST)) {
			$args['account_username'] = $_POST['username'];
			$args['account_password'] = $_POST['password'];
			$args['account_email'] = $_POST['email'];
			$args['display_name'] = $_POST['your_name'];

			if ($account->create($args)) {
				$account->createSettings();
				$userOk = TRUE;
			}
		}
	  break;
	 case 'login':
		// login with an existing account
		$username = $_POST['username'];
		$password = $_POST['password'];

		if (validLoginForm($username, $password)) {

			if ($account->login($username, $password, FALSE)) {
				$userOk = TRUE;
			}
		}
	  break;
	 default:
		// already logged in
		$userOk = TRUE;
	  break;
	}

	if ($userOk) {
		if ($account->account_id == $info['created_by']) {
			catchErr('You cannot accept your own invitation.');
		} else {
			$objAccess = new Access();
			// check if this person is already friends with the inviter
			if (!$objAccess->checkLink($account->account_id, $info['created_by'])) {
				$objAccess->createLink($account->account_id, $info['created_by']);
			}
			// check if the inviter is already friends with this person
			if (!$objAccess->checkLink($info['created_by'], $account->account_id)) {
				$objAccess->createLink($info['created_by'], $account->account_id);
			}
	
			// get a list of my existing permissions
			$my_access  = $objAccess->readAccess($account->account_id);
	
			// explode the list of permissions being granted
			$arrAccess = explode(',', $info['permissions']);
	
			// apply granted permissions to the new account
			foreach ($arrAccess as $strAccess) {
				$project_id = (int)substr($strAccess, 0, -2);
				$permission = getValue($project_id, '', $my_access);
				switch ((int)substr($strAccess, -1)) {
					case 1:
						if ($permission == '') {
							$objAccess->create('READ', $project_id, $account->account_id);
						}
						break;
					case 2:
						if ($permission == '') {
							$objAccess->create('WRITE', $project_id, $account->account_id);
						} elseif ($permission == 'READ') {
							$objAccess->update('WRITE', $project_id, $account->account_id);
						}
						break;
					case 3:
						if ($permission == '') {
							$objAccess->create('ADMIN', $project_id, $account->account_id);
						} else {
							$objAccess->update('ADMIN', $project_id, $account->account_id);
						}
						break;
				}
			}
	
			// remove the used invite code
			$invite->remove($info['invite_id']);
	
			header('Location: /lists/');
		}
	}
}

openPage('Accept Invite Code');
?>
<style>
.buttongrp {
	text-align:right;
}
.help {
	color:#2877D0;
	font-size:11px;
	float:left;
	margin-top:8px;
}
.input, .inputlbl {
	font:13px/16px Arial, Helvetica, sans-serif;
}
.input {
	border:1px solid #999;
	margin-left:10px;
	padding:.35em 6px;
	width:240px;
}
.inputgrp {
	margin:.8em 0;
}
.inputgrp p {
	color:#666;
	font-size:11px;
	margin-left:120px;
}
.inputlbl {
	float:left;
	display:block;
	height:1.7em;
	padding:6px 0 0 0;
	text-align:right;
	width:110px;
}
.required {
	color:green;
}
.form {
	margin:0 auto;
	width:374px;
}
.formBox {
	background:#EEE;
	border:1px solid #DDD;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:1em auto;
	padding:20px 40px;
	width:380px;
}
.formTitle {
	clear:left;
	margin:4px 0 18px 0;
}
#orTitle {
	margin:0 auto;
	text-align:center;
}
</style>
<?php showBanner(); ?>

		<!-- form to login -->
<?php if ($loggedin) { ?>
		<div class="formBox">
			<h2 class="formTitle">Accept Invitation</h2>
			<form class="form" action="/invite/code/<?php echo $code ?>" method="post">
				<p>Click the button below to accept the invitation.</p>
				<div class="buttongrp">
					<input type="hidden" name="formtype" value="accept" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Accept</span></span></button>
				</div>
			</form>
		</div>
<?php } else { ?>
		<!-- form to login -->
		<div class="formBox">
			<h2 class="formTitle">Login</h2>
			<form class="form" action="/invite/code/<?php echo $code ?>" method="post">
				<div class="inputgrp">
					<label class="inputlbl" for="username">Username:</label>
					<input class="input" id="username" maxlength="50" name="username" size="25" type="text" value="<?php
						echo getValue('username') ?>" />
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="password">Password:</label>
					<input class="input" id="password" maxlength="32" name="password" size="25" type="password" />
				</div>
				<div class="buttongrp">
					<input type="hidden" name="formtype" value="login" />
					<a class="help" href="/account/help.php">Lost Username or Password?</a>
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Login</span></span></button>
				</div>
			</form>
		</div>
		<h2 id="orTitle">Or</h2>
		<!-- form to create an account -->
		<div class="formBox">
			<h2 class="formTitle">Create an Account</h2>
			<form class="form" action="/invite/code/<?php echo $code ?>" method="post">
				<div class="inputgrp">
					<label class="inputlbl required" for="username">Username:</label>
					<input class="input" id="username" maxlength="50" name="username" type="text" value="<?php
						echo getValue('username', '', $tmpVals) ?>" />
					<p>Your username can't have any spaces and must be unique.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl required" for="password">Password:</label>
					<input class="input" id="password" maxlength="32" name="password" type="password" />
					<p>Your password must have a minimum of 6 characters.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl required" for="confirm">Confirm Password:</label>
					<input class="input" id="confirm" maxlength="32" name="confirm" type="password" />
					<p>Re-type your password to make sure there were no mistakes.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl required" for="email">Email Address:</label>
					<input class="input" id="email" maxlength="50" name="email" type="text" value="<?php
						echo getValue('email') ?>" />
					<p>Please use a real email address to confirm your account.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl required" for="your_name">Your Name:</label>
					<input class="input" id="your_name" maxlength="50" name="your_name" type="text" value="<?php
						echo getValue('your_name') ?>" />
					<p>Your name will only be seen by your friends.</p>
				</div>
				<div class="buttongrp">
					<input name="formtype" type="hidden" value="register" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Create Account</span></span></button>
				</div>
			</form>
		</div>
<?php } ?>

<div id="footer"></div>
<?php closePage(); ?>

