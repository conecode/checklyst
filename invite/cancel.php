<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');

$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

unset($_SESSION['guest']);
header('Location: /account/');