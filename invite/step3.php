<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projects.php');
require_once('/home1/checkly1/public_html/library/cls.invite.php');
require_once('/home1/checkly1/public_html/library/fnc.validate.php');

$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

// create the invite code
$code = substr(md5(uniqid()), 4, 8);

if (isset($_SESSION['guest']['access'])) {
	$tmpVals = $_SESSION['guest']['access'];
}

// get a list of your projects
$project = new Projects($_SESSION['account_id']);
$projects = $project->read('project_name', 'ADMIN');

// get the permission you've given
$accesslist = '';
$count = 0;
foreach ($projects as $project) {
	$project_id = $project['project_id'];
	$postvalue  = getValue($project_id, $project_id . ':0', $tmpVals);

	switch ($postvalue) {
	 case $project_id . ":1":
		$accesslist .= '   ' . $project['project_name'] . " (Read Only)\n";
		++$count;
		break;
	 case $project_id . ":2":
		$accesslist .= '   ' . $project['project_name'] . " (Read & Write)\n";
		++$count;
		break;
	 case $project_id . ":3":
		$accesslist .= '   ' . $project['project_name'] . " (Admin)\n";
		++$count;
		break;
	}
}

if ($_POST) {
	$code  = $_POST['code'];
	$message = stripslashes(strip_tags($_POST['message']));

	$name  = $_SESSION['guest']['name'];
	$email = $_SESSION['guest']['email'];
	if (isset($_SESSION['guest']['save'])) {
		$permissions = $_SESSION['guest']['save'];
	} else {
		$permissions = '';
	}

	$formOk = true;

	// check if the code is still valid
	if (!validInput($code, 8, 8)) {
		catchErr('The input code is not valid');
		$formOk = false;
	}

	// check if the message is ready to send
	if (strlen($message) > 400) {
		catchErr('The message is too long.');
		$formOk = false;
	}

	// save information to the invite table
	if ($formOk) {
		$invite = new Invite();

		if (!$invite->create($name, $email, $code, $permissions, $account->account_id)) {
			$formOk = false;
		} else {
			require_once('/home1/checkly1/public_html/library/cls.apns.php');
			$apns = new APNS();
			$token = $apns->getUDID($email);
			if ($token != 'X') {
				require_once('/home1/checkly1/public_html/library/fnc.apns_push.php');
				$sender = $account->get($accountId);
				$args['devicetoken'] = $token;
				if ($count > 0) {
					$args['alert'] = $sender['display_name']." has added you as a colaborator and has shared Lysts with you.";
				} else {
					$args['alert'] = $sender['display_name']." has added you as a colaborator.";
				}

				$args['pn_type'] = 'invites';
				$args['badges'] = 1 + $count;

				push($args);
			}
		}
	}

	// send the email
	if ($formOk) {
		$header  = "From: Checklyst <no-reply@checklyst.com>\r\n";
		$header .= "Reply-To: no-reply@checklyst.com\r\n";
		$header .= "X-Mailer: PHP/" . phpversion() . "\r\n";

		$subject = "You've been invited to join Checklyst";

		// try to send mail
		$send = mail($email, $subject, $message, $header);
		if (!$send) {
			$formOk = false;
		}
	}

	// unset temporary array
	if ($formOk) {
		unset($_SESSION['guest']);
		header('Location: /lists/');
	}
}

openPage('Invite A User - Step 3');
?>
<style>
.buttongrp {
	text-align:right;
}
.buttongrp a {
	color:#666;
	font-size:12px;
	margin-right:12px;
}
.inputgrp {
	margin:.8em 0;
}
.formBox {
	background:#EEE;
	border:1px solid #BBB;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:1em auto;
	padding:20px 40px;
	width:520px;
}
.formTitle {
	clear:left;
	margin:4px 0 18px 0;
}
.stepRow {
	border-bottom:1px solid #CCC;
	overflow:hidden;
	margin:0 1px 16px 0;
	padding:0 0 16px 0;
}
.stepCurr, .stepNumb {
	background:transparent url('/images/step_bubble_grey.png') no-repeat center;
	color:#AAA;
	display:block;
	float:left;
	font:18px/27px "Trebuchet MS", "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	height:28px;
	text-align:center;
	width:33.3%;
}
.stepCurr {
	background:transparent url('/images/step_bubble.png') no-repeat center;
	color:#222;
}
</style>
<?php showBanner();
?>

		<div class="formBox">
			<div class="stepRow">
				<span class="stepNumb">1</span>
				<span class="stepNumb">2</span>
				<span class="stepCurr">3</span>
			</div>
			<h2 class="formTitle">Send An Invitation</h2>
			<form action="step3" method="post">
				<div class="inputgrp">
					<input type="hidden" name="code" value="<?php echo $code ?>" />
<textarea name="message" cols="80" rows="12">
DO NOT REPLY TO THIS EMAIL

Dear <?php if (isset($_SESSION['guest'])) {
	echo $_SESSION['guest']['name'];
} ?>,

You've been invited to join checklyst.com!
<?php
if ($count > 1) {
	echo "\nHere are the projects you've been invited to join.\n" . $accesslist;
} elseif ($count == 1) {
	echo "\nHere is the project you've been invited to join.\n" . $accesslist;
} ?>

To accept this invite, go to:
<?php echo SITE_ROOT ?>invite/code/<?php echo $code ?>


<?php echo $_SESSION['display_name']; ?>

</textarea>
				</div>
				<div class="buttongrp">
					<a href="step2">Go Back</a>
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Submit</span></span></button>
				</div>
			</form>
		</div>

<div id="footer"></div>
<?php closePage(); ?>

