<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/fnc.validate.php');

$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

if (isset($_SESSION['guest'])) {
	$tmpVals = $_SESSION['guest'];
}

if ($_POST) {
	$formOk = true;
	$tmpVals = $_POST;

	$name  = $_POST['guestname'];
	$email = $_POST['guestemail'];

	if (!validInput($name, 1, 50)) {
		catchErr("The Guest Name is not valid.");
		$formOk = false;
	}

	if (!validEmail($email)) {
		catchErr("The Email Address is not valid.");
		$formOk = false;
	}

	if ($formOk) {
		$_SESSION['guest']['name']  = $name;
		$_SESSION['guest']['email'] = $email;

		header('Location: step2');
	}
}

openPage('Invite A User - Step 1');
?>
<style>
.buttongrp {
	text-align:right;
}
.buttongrp a {
	color:#666;
	font-size:12px;
	margin-right:12px;
}
.input, .inputlbl {
	font:13px/16px Arial, Helvetica, sans-serif;
}
.input {
	border:1px solid #999;
	margin-left:10px;
	padding:.35em 6px;
	width:240px;
}
.inputlbl {
	float:left;
	display:block;
	height:1.7em;
	padding:6px 0 0 0;
	text-align:right;
	width:110px;
}
.inputgrp {
	margin:.8em 0;
}
.form {
	margin:0 auto;
	width:374px;
}
.formBox {
	background:#EEE;
	border:1px solid #BBB;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:1em auto;
	padding:20px 40px;
	width:480px;
}
.formTitle {
	clear:left;
	margin:4px 0 18px 0;
}
.stepRow {
	border-bottom:1px solid #CCC;
	overflow:hidden;
	margin:0 1px 16px 0;
	padding:0 0 16px 0;
}
.stepCurr, .stepNumb {
	background:transparent url('/images/step_bubble_grey.png') no-repeat center;
	color:#AAA;
	display:block;
	float:left;
	font:18px/27px "Trebuchet MS", "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	height:28px;
	text-align:center;
	width:33.3%;
}
.stepCurr {
	background:transparent url('/images/step_bubble.png') no-repeat center;
	color:#222;
}
</style>
<?php showBanner(); ?>

		<div class="formBox">
			<div class="stepRow">
				<span class="stepCurr">1</span>
				<span class="stepNumb">2</span>
				<span class="stepNumb">3</span>
			</div>
			<h2 class="formTitle">User Details</h2>
			<form class="form" action="step1" method="post">
				<div class="inputgrp">
					<label class="inputlbl" for="guestname">Guests Name:</label>
					<input class="input" id="guestname" maxlength="50" name="guestname" size="40" type="text" value="<?php
						echo getValue('name', '', $tmpVals) ?>" />
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="guestemail">Email Address:</label>
					<input class="input" id="guestemail" maxlength="50" name="guestemail" size="40" type="text" value="<?php
						echo getValue('email', '', $tmpVals) ?>" />
				</div>
				<div class="buttongrp">
					<a href="/lists">Cancel</a>
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Continue</span></span></button>
				</div>
			</form>
		</div>

<div id="footer"></div>
<?php closePage(); ?>

