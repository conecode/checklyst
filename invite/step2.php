<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projects.php');

$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

// get the current account
$account_id = (int)$_SESSION['account_id'];

// get a list of projects for the current account
$project = new Projects($account_id);
$projects = $project->read('project_name', 'ADMIN');

if (isset($_SESSION['guest']['access'])) {
	$tmpVals = $_SESSION['guest']['access'];
}

if ($_POST) {
	if ($_POST['project_count'] > 0) {
		foreach($_POST['project'] as &$project_id) {
			$project->setProject($project_id);
			$radio = (int)$_POST[$project_id];
			if ($project->permission == 'ADMIN') {
				$arrAccess[$project_id] = $project_id . ':' . $radio;
			}
		}
		$_SESSION['guest']['access'] = $arrAccess;
		$_SESSION['guest']['save'] = implode(',', $arrAccess);
	}
	header('Location: step3');
}

openPage('Invite A User - Step 2');
?>
<style>
.buttongrp {
	text-align:right;
}
.buttongrp a {
	color:#666;
	font-size:12px;
	margin-right:12px;
}
.colRad {
	width:90px;
}
.colPrj {
	width:160px;
}
.projectName {
	text-align:left;
}
#clearance {
	margin-bottom:16px;
}
#clearance td {
	border-bottom:1px solid #DDD;
	padding:8px 2px;
	text-align:center;
}
#clearance th {
	border-bottom:1px solid #DDD;
	font-size:12px;
	padding:2px;
	text-align:center;
}
#clearance label {
	display:block;
}
.formBox {
	background:#EEE;
	border:1px solid #BBB;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:1em auto;
	padding:20px 40px;
	width:520px;
}
.formTitle {
	clear:left;
	margin:4px 0 18px 0;
}
.stepRow {
	border-bottom:1px solid #CCC;
	overflow:hidden;
	margin:0 1px 16px 0;
	padding:0 0 16px 0;
}
.stepCurr, .stepNumb {
	background:transparent url('/images/step_bubble_grey.png') no-repeat center;
	color:#AAA;
	display:block;
	float:left;
	font:18px/27px "Trebuchet MS", "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	height:28px;
	text-align:center;
	width:33.3%;
}
.stepCurr {
	background:transparent url('/images/step_bubble.png') no-repeat center;
	color:#222;
}
</style>
<?php showBanner(); ?>

		<div class="formBox">
			<div class="stepRow">
				<span class="stepNumb">1</span>
				<span class="stepCurr">2</span>
				<span class="stepNumb">3</span>
			</div>
			<h2 class="formTitle">Set Permissions</h2>
			<form action="step2" method="post">
<?php
$count = sizeOf($projects); ?>
				<input type="hidden" name="project_count" value="<?php echo $count ?>" />
<?php
if ($count == 0) { ?>
				<p>No projects available.</p>
				<p>You can only set permissions for projects that you've created.</p><br/>
				<p>Click continue to skip this step.</p><br/><br/>
<?php
} else {
?>
				<table id="clearance">
					<colgroup><col class="colPrj"><col class="colRad"><col class="colRad"><col class="colRad"><col class="colRad"></colgroup>
					<thead><tr><th>Project</th><th>None</th><th>Read Only</th><th>Read & Write</th><th>Admin.</th></tr></thead>
					<tbody>
<?php	foreach ($projects as $project) {
			$project_id = $project['project_id'];
			$postvalue  = getValue($project_id, $project_id . ':0', $tmpVals);?>
			<tr>
				<td>
					<p class="projectName"><?php echo $project['project_name'] ?></p>
					<input name="project[]" type="hidden" value="<?php echo $project_id ?>" />
				</td>
				<td><label><input name="<?php echo $project_id ?>" type="radio" value="0" <?php
			if ($postvalue == $project_id . ":0") {
				echo 'checked="checked" ';
			} ?>/></label></td>
				<td><label><input name="<?php echo $project_id ?>" type="radio" value="1" <?php
			if ($postvalue == $project_id . ":1") {
				echo 'checked="checked" ';
			} ?>/></label></td>
<?php		if ($project['permission'] == 'WRITE' || $project['permission'] == 'ADMIN') { ?>
				<td><label><input name="<?php echo $project_id ?>" type="radio" value="2" <?php
				if ($postvalue == $project_id . ":2") {
					echo 'checked="checked" ';
				} ?>/></label></td>
<?php		} else { ?>
				<td>N/A</td>
<?php		}
			if ($project['permission'] == 'ADMIN') { ?>
				<td><label><input name="<?php echo $project_id ?>" type="radio" value="3" <?php
				if ($postvalue == $project_id . ":3") {
					echo 'checked="checked" ';
				} ?>/></label></td>
<?php		} else { ?>
				<td>N/A</td>
<?php		} ?>
			</tr>
<?php	} ?>
					</tbody>
				</table>
<?php
}
?>
				<div class="buttongrp">
					<a href="step1">Go Back</a>
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Continue</span></span></button>
				</div>
			</form>
		</div>

<div id="footer"></div>
<?php closePage(); ?>

