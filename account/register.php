<?php
require_once('templates/tpl.default.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

if ($_POST) {
    if(validRegisterForm($_POST)) {
		$args['account_username'] = $_POST['username'];
		$args['account_password'] = $_POST['password'];
		$args['account_email'] = $_POST['email'];
		$args['display_name'] = $_POST['your_name'];

		if ($account->create($args)) {
			$account->createSettings();
			header('Location: /lists/');
		}
    }
}

openPage('Register');
?>
<style>
.buttongrp {
	text-align:right;
}
.input, .inputlbl {
	font:13px/16px Arial, Helvetica, sans-serif;
}
.input {
	border:1px solid #999;
	margin-left:10px;
	padding:.35em 6px;
	width:240px;
}
.inputgrp {
	margin:.8em 0;
}
.inputgrp p {
	color:#666;
	font-size:11px;
	margin-left:120px;
}
.inputlbl {
	float:left;
	display:block;
	height:1.7em;
	padding:6px 0 0 0;
	text-align:right;
	width:110px;
}
.signin {
	background:#EEE;
	border:1px solid #DDD;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:1em auto;
	padding:20px 40px;
	width:380px;
}
.signin h2 {
	margin:.3em 0 1em 0;
}
</style>
<?php showBanner();?>

		<!-- form to create an account -->
		<form class="signin" action="register.php" method="post">
			<h2>Create an Account</h2>
			<div class="inputgrp">
				<label class="inputlbl" for="username">Username:</label>
				<input class="input" id="username" maxlength="50" name="username" type="text" value="<?php
					echo getValue('username', '', $tmpVals) ?>" />
				<p>Your username can't have any spaces and must be unique.</p>
			</div>
			<div class="inputgrp">
				<label class="inputlbl" for="password">Password:</label>
				<input class="input" id="password" maxlength="32" name="password" type="password" />
				<p>Your password must have a minimum of 6 characters.</p>
			</div>
			<div class="inputgrp">
				<label class="inputlbl" for="confirm">Confirm Password:</label>
				<input class="input" id="confirm" maxlength="32" name="confirm" type="password" />
				<p>Re-type your password to make sure there were no mistakes.</p>
			</div>
			<div class="inputgrp">
				<label class="inputlbl" for="email">Email Address:</label>
				<input class="input" id="email" maxlength="50" name="email" type="text" value="<?php
					echo getValue('email') ?>" />
				<p>Please use a real email address to confirm your account.</p>
			</div>
			<div class="inputgrp">
				<label class="inputlbl" for="your_name">Your Name:</label>
				<input class="input" id="your_name" maxlength="50" name="your_name" type="text" value="<?php
					echo getValue('your_name') ?>" />
				<p>Your name will only be seen by your friends.</p>
			</div>
			<div class="buttongrp">
				<button class="button" type="submit"><span class="default-l"><span class="default-r">Create Account</span></span></button>
			</div>
		</form>
<div id="footer"></div>
<?php

if (!$_POST) { ?>
<script type="text/javascript">
	document.getElementById("username").focus();
</script>
<?php

}
closePage();
?>
