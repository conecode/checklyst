<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/fnc.validate.php');

$account = new Account();

if (!$account->confirmSession()) {
	header('Location: /');
}

$user = $account->get();

if ($_POST) {

	// check that the account form was sent
	if($_POST['formtype'] == 'Account') {

		// check the submitted data against the existing data
		if ($_POST['email'] <> $user['account_email']) {
			$args['account_email'] = $_POST['email'];
		}
		if ($_POST['username'] <> $user['account_username']) {
			$args['account_username'] = $_POST['username'];
		}
		if ($_POST['your_name'] <> $user['display_name']) {
			$args['display_name'] = $_POST['your_name'];
		}

		// if the data has changed
		if (isset($args)) {

			// check that the account information is valid
			if (validAccountForm($_POST)) {

				// update the account
				if ($account->update($args)) {
					header('Location: /lists/');
				}
			}
		} else {
			catchErr('The account information has not changed.');
		}
	} elseif ($_POST['formtype'] == 'Password') {

		// check that the password is valid
		if (validPasswordForm($_POST)) {

			// update the password
			$args['account_password'] = $_POST['password'];
			if ($account->update($args)) {
				header('Location: /lists/');
			}
		}
	}
}

openPage('Edit Account');
?>
<style>
.buttongrp {
	text-align:right;
}
.input, .inputlbl {
	font:13px/16px Arial, Helvetica, sans-serif;
}
.input {
	border:1px solid #999;
	margin-left:10px;
	padding:.35em 6px;
	width:240px;
}
.inputgrp {
	margin:.8em 0;
}
.inputgrp p {
	color:#666;
	font-size:11px;
	margin-left:120px;
}
.inputlbl {
	float:left;
	display:block;
	height:1.7em;
	padding:6px 0 0 0;
	text-align:right;
	width:110px;
}
.required {
	color:green;
}
.signin {
	background:#EEE;
	border:1px solid #BBB;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:1em auto;
	padding:20px 40px;
	width:380px;
}
.signin h2 {
	margin:.3em 0 1em 0;
}
</style>
<?php showBanner(); ?>


			<form action="edit.php" class="signin" method="post">
				<h2>Edit Account Information</h2>
				<div class="inputgrp">
					<label class="inputlbl" for="username">Username:</label>
					<input class="input" id="username" maxlength="50" name="username" type="text" value="<?php echo getValue('account_username', '', $user) ?>" />
					<p>Your username can't have any spaces and must be unique.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="email">Email Address:</label>
					<input class="input" id="email" maxlength="50" name="email" type="text" value="<?php echo getValue('account_email', '', $user) ?>" />
					<p>Please use a real email address to confirm your account.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="your_name">Your Name:</label>
					<input class="input" id="your_name" maxlength="50" name="your_name" type="text" value="<?php echo getValue('display_name', '', $user) ?>" />
					<p>Your name will only be seen by your friends.</p>
				</div>
				<div class="buttongrp">
					<input name="formtype" type="hidden" value="Account" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Save Changes</span></span></button>
				</div>
			</form>
			<form class="signin" action="edit.php" method="post">
				<h2>Change Password</h2>
				<div class="inputgrp">
					<label class="inputlbl" for="password">Password:</label>
					<input class="input" id="password" name="password" maxlength="32" type="password" />
					<p>Your password must have a minimum of 6 characters.</p>
				</div>
				<div class="inputgrp">
					<label class="inputlbl" for="confirm">Confirm Password:</label>
					<input class="input" id="confirm" name="confirm" maxlength="32" type="password" />
					<p>Re-type your password to make sure there were no mistakes.</p>
				</div>
				<div class="buttongrp">
					<input name="formtype" type="hidden" value="Password" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Save Password</span></span></button>
				</div>
			</form>

<div id="footer"></div>
<?php closePage(); ?>
