<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');

$account = new Account();
$account->logout();

openPage('Logout'); ?>
<style>
#infoBox {
	background:#FFF url('/images/sidebar_bg.png') repeat-x;
	border:1px solid #BBB;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:24px auto;
	padding:20px 40px;
	width:400px;
}
#returnLink, #returnLink:visited {
	color:#2975CC;
	display:block;
	font-size:16px;
	margin-top:2em;
	text-align:center;
}
.infoMsg {
	background:url('/images/icn_info.png') no-repeat 10px 11px;
	line-height:24px;
	margin:20px auto;
	padding:10px 0 10px 70px;
	width:240px;
}
.infoTitle {
	clear:left;
	margin:16px 0;
}
</style>
<?php
showBanner();
?>

		<div id="infoBox">
			<h2 class="infoTitle">Logout</h2>
			<div class="infoMsg">
				<p>You're now logged out of checklyst.</p>
				<p>Thank you for visiting.</p>
			</div>
			<a id="returnLink" href="/">Return to login</a>
		</div>


<div id="footer"></div>
<?php closePage(); ?>
