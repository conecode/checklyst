<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/fnc.validate.php');

$account = new Account();

if (!$account->confirmSession()) {
	header('Location: ' . SITE_ROOT);
}

// ------------------------------------------------------------------------

// verify information is correct before proceeding
if ($_POST) {

	// the account class will make sure these are integers
	$args["show_name"] = $_POST["myname"];
	$args["show_email"] = $_POST["myemail"];
	$args["newsletter"] = $_POST["newsletter"];

	if ($account->updateSettings($args)) {
		$result = "Settings Saved";
	}
}

// ------------------------------------------------------------------------

$user = $account->get();
$settings = $account->getSettings();

openPage('Preferences'); ?>
<style>
#account_info {
	margin-bottom:1em;
}
#account_info dt {
	float:left;
	margin-bottom:1em;
	text-align:right;
	width:100px;
}
#account_info dd {
	margin:0 0 1em 110px;
}
#accountBox {
	background:#FFF;
	padding:34px 10px 10px 10px;
}
.link, .link:visited {
	color:#2877D0;
}

#wrap {
	margin:0 auto;
	position:relative;
	text-align:left;
	width:740px;
}
</style>
<?php
showBanner();
?>
<div id="wrap">
	<div id="filter">
		<a class="button" href="/lists/">
			<span class="action-l"><span class="action-r">My Lysts</span></span>
		</a>
	</div>
	<div id="accountBox">
		<h1>Settings</h1>
<?php if (!empty($result)) { ?>
		<p id="form_result"><?php echo $result ?></p>
<?php } ?>
		<h2>Your Account Information</h2>
		<dl id="account_info">
			<dt>Your Name:</dt><dd><?php echo $user['display_name']; ?></dd>
			<dt>Username:</dt><dd><?php echo $user['account_username']; ?></dd>
			<dt>Email Address:</dt><dd><?php echo $user['account_email']; ?></dd>
		</dl>
		<a class="link" href="edit.php">Edit My Account</a>
<!--		<h2>Email Me</h2>
		<form action="settings.php" method="post">
			<div>
				<label>Enable Notifications</label>
			</div>
			<table cellspacing="0">
				<tbody>
					<tr>
						<td>List item is due soon...</td>
						<td class="set_val"><input <?php
							if ($settings['show_name'] == 2) {
								echo 'checked="checked" ';
							} ?>name="myname" type="radio" value="2" /></td>
						<td class="set_val"><input <?php
							if ($settings['show_name'] == 1) {
								echo 'checked="checked" ';
							} ?>name="myname" type="radio" value="1" /></td>
						<td class="set_val"><input <?php
							if ($settings['show_name'] == 0) {
								echo 'checked="checked" ';
							} ?>name="myname" type="radio" value="0" /></td>
					</tr>
					<tr>
						<td>Another person adds an item to my list...</td>
						<td class="set_val"><input <?php
							if ($settings['show_email'] == 2) {
								echo 'checked="checked" ';
							} ?>name="myemail" type="radio" value="2" /></td>
						<td class="set_val"><input <?php
							if ($settings['show_email'] == 1) {
								echo 'checked="checked" ';
							} ?>name="myemail" type="radio" value="1" /></td>
						<td class="set_val"><input <?php
							if ($settings['show_email'] == 0) {
								echo 'checked="checked" ';
							} ?>name="myemail" type="radio" value="0" /></td>
					</tr>
				</tbody>
			</table>
			<div>
				<input name="submit" type="submit" value="Save Settings" />
			</div>
		</form> -->
	</div><!-- account box -->
</div><!-- wrap -->
<div id="footer"></div>
<?php

if (!empty($result)) { ?>
	<script type="text/javascript">
		window.setTimeout(function() {
			document.getElementById("form_result").style.display = "none";
		}, 2000);
	</script>
<?php
}
closePage(); ?>