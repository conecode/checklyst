<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/fnc.validate.php');

$account = new Account();
$email_sent = false;

if ($_POST) {
	$formOk = true;
	if (!validEmail($_POST['send_email'])) {
		catchErr('You entered an invalid email address.');
		$formOk = false;
	}

	if ($formOk) {
		$email_sent = $account->sendReminder($_POST['send_email']);
	}
}

openPage('Password Help');
?>
<style>
.buttongrp {
	text-align:right;
}
.input, .inputlbl {
	font:13px/16px Arial, Helvetica, sans-serif;
}
.input {
	border:1px solid #999;
	margin-left:10px;
	padding:.35em 6px;
	width:240px;
}
.inputgrp {
	margin:.8em 0;
}
.inputlbl {
	float:left;
	display:block;
	height:1.7em;
	padding:6px 0 0 0;
	text-align:right;
	width:110px;
}
.infoMsg {
	background:url('/images/icn_info.png') no-repeat 10px 11px;
	line-height:24px;
	margin:20px auto;
	padding:10px 0 10px 70px;
	width:270px;
}
.form {
	margin:0 auto;
	width:374px;
}
.formBox {
	background:#EEE;
	border:1px solid #BBB;
	box-shadow:0 2px 4px #AAA;
		-moz-box-shadow:0 2px 4px #AAA;
		-webkit-box-shadow:0 2px 4px #AAA;
	margin:1em auto;
	padding:20px 40px;
	width:480px;
}
.formTitle {
	clear:left;
	margin:4px 0 18px 0;
}
</style>
<?php showBanner();?>
<?php if ($email_sent) { ?>


		<p>Your username and password have been sent to your email address.</p>
		<p>Click the link below to return to the login page.</p>
		<a href="login.php">Go To Login</a>


<?php } else { ?>
		<div class="formBox">
			<h2 class="formTitle">Retrieve Password</h2>
			<div class="infoMsg">
				<p>We were unable to retrieve your password.  Enter an email address below to try again.</p>
			</div>
			<form action="index.php" class="form" method="post">
				<div class="inputgrp">
					<label class="inputlbl" for="send_email">Email Address:</label>
					<input class="input" id="send_email" name="send_email" maxlength="50" type="text" value="<?php echo getValue('send_email') ?>" />
				</div>
				<div class="buttongrp">
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Send Password</span></span></button>
				</div>
			</form>
		</div>
	<script type="text/javascript">
		document.getElementById('send_email').focus();
	</script>

<?php } ?>

<div id="footer"></div>
<?php closePage(); ?>
