<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];
    $project_id = (int)getValue('lid', 0);

    $project = new ProjectList($account_id, $project_id);

	$people = $project->readPeople();
?>
<people>
<?php foreach ($people as $person) { ?>
	<person account_id="<?= $person['account_id'] ?>" name="<?= $person['display_name'] ?>" permission="<?= $person['permission'] ?>" />
<?php } ?>
</people>
<?php
}
?>