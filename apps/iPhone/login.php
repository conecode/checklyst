<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

if ($_REQUEST) {
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];
    $cookie = (empty($_REQUEST['cookie'])) ? false : true;

    if (validLoginForm($username, $password)) {
        $account->login($username, $password, $cookie);
    }
}

$loggedin = $account->confirmSession();

if ($loggedin) {
?>
YES
<?php } else { ?>
There was a problem logging you in.
<?php } ?>