<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');

$account = new Account();

$loggedin = $account->confirmSession();

if($loggedin) {
	require_once('library/cls.access.php');
	$objAccess  = new Access();
	// get the current account
	$my_account_id = (int)$_SESSION['account_id'];
	$my_people = $objAccess->read($my_account_id);
	$usr_account_id = getValue('prsn', 0);
	
	openPage('People');
?>
<div>
	<!-- projects --> 
	<div class="group">
		<ul id="people" class="groupItems">
<?php showManagePeople($my_people, $my_account_id, $usr_account_id); ?>
		</ul>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script>
</script>
<?php
} else {
	openPage('Not Logged In');
}
closePage();
?>