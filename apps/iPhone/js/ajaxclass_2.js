/* Asynchronous JavaScript and XML
 *
 * Created by Cone Code Development
 * http://conecode.com/tools/ajax_class/
 * version 2.0
 */
// global variables for pause option
var AJAX_OBJECT = null;
var AJAX_ARGUMENTS = null;
var AJAX_COUNT = 0;

/** ------ CLASS CONSTRUCTOR ------------------ */

function Ajax(args) {

	// set the class properties
	for (var n in args) {
		if (typeof(this[n]) === "undefined") {
			this.parameters[n] = args[n];
		} else {
			this[n] = args[n];
		}
	}

	// creates a new 'ajaxId' parameter to prevent caching
	this.parameters.ajaxId = new Date().getTime();

	// get the XmlHttpRequest object
	this.http = this.getHttp();
}

/** ------ CLASS PROPERTIES ------------------- */

Ajax.prototype = {

	// requesting page. defaults to the current page.
	url: window.location.href,

	// request method "GET" or "POST"
	method: "GET",

	// variables and values to send to the url
	parameters: {},

	// an id name of an HTML element to output response
	element: null,

	// "innerHTML" for a tag, "value" for a form
	outputtype: "innerHTML",

	// specifies a timer to slow down the request
	pause: null,

	// execute function on success
	onSuccess: null,

	// execute function on error
	onError: null,

	// result from the http call
	responseText: null,

	// error messages
	errorText: null,

	// formatted string of parameters [private]
	strParams: ""
};

/** ------ INSTANCE METHODS ------------------- */

// gets the XmlHttpRequest object
Ajax.prototype.getHttp = function() {

	// never forget who you are (the readystatechange chages the value of 'this')
	var self = this;
	var http = null;

	if (typeof XMLHttpRequest !== 'undefined') {
		http = new XMLHttpRequest();
	} else {
		var versions = ["Microsoft.XmlHttp",
						"MSXML2.XmlHttp",
						"MSXML2.XmlHttp.3.0",
						"MSXML2.XmlHttp.4.0",
						"MSXML2.XmlHttp.5.0"];
		for (var i = 0, len = versions.length; i < len; i++) {
			try {
				http = new ActiveXObject(versions[i]);
				break;
			}
			catch(e){}
		}
	}

	if (!http) {
		this.setError('failed to create an XMLHTTP instance');
		return false;
	}

	// sets the complete function to run when the http status is ready
	http.onreadystatechange = function() {
		if (self.http.readyState === 4) {
			self.complete();
		}
	};

	return http;
};

// setup and execute the http request
Ajax.prototype.run = function() {

	// sets the strParams property
	for (var i in this.parameters) {
		if (this.strParams.length > 0) {
			this.strParams += "&";
		}
		this.strParams += encodeURIComponent(i) + "=" +
                    encodeURIComponent(this.parameters[i]);
	}

	// if using GET, combine the url and the string of parameters
	if (this.method.toUpperCase() === "GET") {
		if (this.strParams.length > 0) {
			this.url += ((this.url.indexOf("?")>-1) ? "&" : "?") +
                            this.strParams;
		}
		this.strParams = null;
	}

	// initialize the http request
	this.http.open(this.method, this.url, true);

	// if using POST, set the request header.
	if (this.method.toUpperCase() === "POST") {
		this.http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	}

	// perform the request
	this.http.send(this.strParams);
};

// sends an output to the user
Ajax.prototype.complete = function() {

	// sets the responseText property
	this.responseText = this.http.responseText;

		// check if the http request was a success
	if (this.http.status === 200) {
		if (this.element) {
			// element property is set, find it!
			var el = document.getElementById(this.element);

			// output the reponse to the element
			if (this.outputtype.toUpperCase() === "INNERHTML") {
				el.innerHTML = this.responseText;

			} else if (this.outputtype.toUpperCase() === "INPUT") {
				el.value = this.responseText;
			}
		}

		// if onSuccess property is set, run it!
		if (typeof(this.onSuccess) === "function") {
			this.onSuccess(this);
		}

	// the http request has given an error
	} else {
		if (this.http.status === 404) {
			this.setError("URL not found");
		} else {
			this.setError("HTTP status = "+this.http.status);
		}
	}

	// clean up
	AJAX_COUNT = 0;
	delete this.http.onreadystatechange;
	this.http = null;
};

// attempts to handle any errors
Ajax.prototype.setError = function(msg) {
	// sets the error property
	this.errorText = "AJAX ERROR:\n\n" + msg;

	// sends this ajax object to the user specified function
	if (typeof(this.onError) === "function") {
		this.onError(this);
	}
};

/** ------ PRIVATE FUNCTIONS ------------------ */

// insert a spinner into the specified element
function _ajaxSpinner (args) {
	if (args.element){
		var spin = args.spinner.split(",");
		var img = '<img src="' + spin[0] + '" alt="wait..." />';
		if (typeof(spin[1]) != "undefined") {
			img += ' ' + spin[1];
		}
		document.getElementById(args.element).innerHTML = img;
	}
}

// executed with timer in Ajax.pause
function _ajaxPause (count) {
	if (count === AJAX_COUNT) {
		// cancel existing object
		if (AJAX_OBJECT) {
			if (AJAX_OBJECT.http) {
				AJAX_OBJECT.http.abort();
				delete AJAX_OBJECT.http.onreadystatechange;
				AJAX_OBJECT.http = null;
			}
		}
		// start a new object
		AJAX_OBJECT = new Ajax(AJAX_ARGUMENTS);
		AJAX_OBJECT.run();
		return AJAX_OBJECT;
	}
        return false;
}

/** ------ CLASS METHODS ---------------------- */

Ajax.pause = function (args) {
	if (args.spinner && AJAX_COUNT === 0) {
		_ajaxSpinner(args);
	}
	AJAX_ARGUMENTS = args;
	var span = (args.pause) ? args.pause : 500;
	++AJAX_COUNT;
	var t = setTimeout("_ajaxPause(" + AJAX_COUNT + ")", span);
};

Ajax.get = function (args) {
	if (args.spinner) {
		_ajaxSpinner(args);
	}
	var obj = new Ajax(args);
	obj.run();
	return obj;
};

Ajax.post = function (args) {
	if (args.spinner) {
		_ajaxSpinner(args);
	}
	var obj = new Ajax(args);
	obj.method = 'POST';
	obj.run();
	return obj;
};