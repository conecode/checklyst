<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

$loggedin = $account->confirmSession();

if ($loggedin) {
    $user = $account->get();
?>
<settings>
    <setting username="<?php echo getValue('account_username', '', $user) ?>" email="<?php echo getValue('account_email', '', $user) ?>" displayName="<?php echo getValue('display_name', '', $user) ?>" />
</settings>
<?php } else { ?>
<settings>
    <setting username="NLI" email="NLI" displayName="NLI" />
</settings>
<?php } ?>