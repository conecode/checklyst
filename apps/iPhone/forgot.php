<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();
$account->logout();
$email_sent = false;
$formOk = false;

openPage('Password Sent');

if ($_POST) {
	$formOk = true;
	if (!validEmail($_POST['send_email'])) {
?>
	<div id="wrap">
		<!-- form to login -->
		<br /><br /><br />
		<p>You must enter a valid email address.</p>
	</div>
<?php
		$formOk = false;
	}

	if ($formOk) {
		$email_sent = $account->sendReminder($_POST['send_email']);
?>
	<div id="wrap">
		<!-- form to login -->
		<br /><br /><br />
		<p>Your username and password have been sent to your email address.</p>
		<form action="login.php" class="signin" method="post">
			<div class="formgrp">
				<input class="input" id="username" maxlength="50" name="username" size="25" type="text" placeholder="Username" />
			</div>
			<div class="formgrp">
				<input class="input" id="password" maxlength="32" name="password" size="25" type="password" autocomplete="off" placeholder="Password" />
			</div>
			<div class="formgrp">
				Remember Me<br />
				<?php
					if (getValue('cookie', false)) {
						echo '<img id="switchimg" src="images/switch-on.png" width="90" onClick="changeSwitch()" /><input type="hidden" id="cookie" name="cookie" value="1" />';
					} else {
						echo '<img id="switchimg" src="images/switch-off.png" width="90" onClick="changeSwitch()" /><input type="hidden" id="cookie" name="cookie" value="0" />';
					}
				?>
			</div>
			<div class="formgrp">
				<button class="button" type="submit">Login</button>
			</div>
		</form>
	</div>
<?php
	}
} else {
?>
	<br /><br /><br />
<?php
}
if (!$formOk) {
?>
	<div id="wrap">
		<!-- form to retrieve password -->
		<form action="forgot.php" class="signin" method="post">
			<div class="formgrp">
				<input class="input" id="send_email" maxlength="50" name="send_email" size="25" type="text" placeholder="Email Address" value="<?php echo getValue('send_email') ?>" />
			</div>
			<div class="formgrp">
				<button class="button" type="submit">Send</button>
			</div>
		</form>
	</div>
<?php
}
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script language="javascript">
function changeSwitch(){
	if($('#cookie').val()==1){
		$('#cookie').val(0);
		$("#switchimg").attr("src","images/switch-off.png");
	} else {
		$('#cookie').val(1);
		$("#switchimg").attr("src","images/switch-on.png");
	}
}
</script>
<?php
closePage();
?>