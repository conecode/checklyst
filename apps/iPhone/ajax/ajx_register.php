<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

$loggedin = $account->confirmSession();

if ($_POST) {
    if (validRegisterForm($_POST)) {
        $args['account_username'] = $_POST['username'];
        $args['account_password'] = $_POST['password'];
        $args['account_email'] = $_POST['email'];

        if ($account->create($args)) {
            $account->createSettings();
            echo "YES";
        } else {
			echo mobileErrors();
		}
    } else {
		echo mobileErrors();
	}
}