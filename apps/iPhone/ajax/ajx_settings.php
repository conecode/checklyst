<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

$loggedin = $account->confirmSession();

if ($loggedin) {
    $user = $account->get();

    if ($_POST) {
        // check that the account form was sent
        if ($_POST['formtype'] == 'account') {
            // check the submitted data against the existing data
            if ($_POST['email'] <> $user['account_email']) {
                $args['account_email'] = $_POST['email'];
            }
            if ($_POST['username'] <> $user['account_username']) {
                $args['account_username'] = $_POST['username'];
            }
            if ($_POST['displayName'] <> $user['display_name']) {
                $args['display_name'] = $_POST['displayName'];
            }

            // if the data has changed
            if (isset($args)) {
                // check that the account information is valid
                if (validAccountForm($_POST)) {
                    // update the account
                    if ($account->update($args)) {
                        echo 'Account Saved';
                    } else {
						echo mobileErrors();
                    }
                } else {
                    echo mobileErrors();;
                }
            } else {
                echo mobileErrors();;
            }
        } elseif ($_POST['formtype'] == 'password') {
            // check that the password is valid
            if (validPasswordForm($_POST)) {
                // update the password
                $args['account_password'] = $_POST['password'];
                if ($account->update($args)) {
                    echo 'Password Changed';
                } else {
                    echo mobileErrors();;
                }
            } else {
                echo mobileErrors();;
            }
        }
    }
}
?>