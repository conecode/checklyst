<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

if ($_POST) {
    $email = $_POST['email'];


    if (validEmail($email)) {
        if ($account->sendReminder($email)) {
            echo "Your login information has been sent to your email.";
        } else {
			echo mobileErrors();
        }
    } else {
		echo mobileErrors();
    }
}