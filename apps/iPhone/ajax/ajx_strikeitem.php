<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];

    // check if the name is valid
    $project_id = (int)getValue('lid', 0);
    echo $project_id;
    $strike = (int)getValue('strike', 0);
    $iid = (int)getValue('iid', 0);

    $project = new ProjectList($account_id, $project_id);

    // create a new project in the database
    if (!$project->strikeListItem($iid, $strike)) {
        echo 0;
    } else {
        $project->accessTimeStamp();
        echo 1;
    }
}
?>