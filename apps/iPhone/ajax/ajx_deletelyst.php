<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];
    
    $project_id = (int)getValue('lid', 0);

    $project = new ProjectList($account_id, $project_id);
    
    // verify the project id is valid
    if ($project->project_id == 0) {
        catchErr('A project identifier could not be acquired.');
    }
    $return = $project->remove();
}
?>