<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];

    // get a list of projects for the current account
    $project = new ProjectList($account_id);

    // set the specified project or the last accessed project
    $project_id = (!empty($_REQUEST['lid'])) ? (int)$_REQUEST['lid'] : $project->getLastAccess();

    // set the specified project or the last accessed project
    $item_id = $_REQUEST['iid'];

    // get the information and checklist for the specified project
    if (!empty($project_id)) {
        $project->setProject($project_id);
    }

    // verify the project id is valid
    if ($project->project_id == 0) {
        echo 'X';
    } else {
        if ($project->removeListItem($item_id)){
            echo 'YES';
        } else {
            echo 'X';
        }
    }
}
?>