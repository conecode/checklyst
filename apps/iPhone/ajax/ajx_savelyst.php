<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];

    // check if the name is valid
    $name = cleanString(getValue('nam', '', $_POST));
    $action = getValue('act', '', $_POST);
    $project_id = (int)getValue('lid', 0);

    $project = new ProjectList($account_id, $project_id);

    // fix any html entities
    $name = cleanString($name);

    if ($action == 'create') {
        // create a new project in the database
        if (!$project->create($name)) {
            catchErr('Could not save lyst.');
        }

        if ($project_id == 0) {
            $project->accessTimeStamp();
        }
    } else {
        // verify the project id is valid
        if ($project->project_id == 0) {
            catchErr('A project identifier could not be acquired.');
        }

        // update the database
        if (!$project->update($name)) {
            catchErr('Could not update lyst');
        }
        $project_id = $project->project_id;
    }
}
?>