<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];

    // get a list of projects for the current account
    $project = new ProjectList($account_id);

    // get a list of projects for the current account
    $projectlist = $project->read();

    // set the specified project or the last accessed project
    $project_id = (!empty($_REQUEST['id'])) ? (int)$_REQUEST['id'] : $project->getLastAccess();
	if(empty($_REQUEST['sort'])) {
		$sort_by = "item_priority";
	} else {
		$sort_by = $_REQUEST['sort'];
	}

    // get the information and checklist for the specified project
    if (!empty($project_id)) {
        $project->setProject($project_id);
        $projectinfo = $project->getFromArray($projectlist);
        list($itemlist, $itemcount, $completed, $priorities) = $project->readList($sort_by);
    } else {
        $itemlist = array();
    }

    $thisTime = strtotime('now');
    $thisDate = date('M d Y', $thisTime);
    $thisYear = date('Y', $thisTime);
    $compDate = strtotime(date('M d Y', $thisTime));

	$todo_count = 0;
	$done_count = 0;

	$lefttodo = $itemcount - $completed;
	echo '<input type="hidden" id="lefttodo" value="'.$lefttodo.'" />';

    foreach ($itemlist as $item) {
		if(!empty($_REQUEST['iid'])) {
			if($_REQUEST['iid'] == $item['item_id']) {
				$show = true;
			} else {
				$show = false;
			}
		} else {
			$show = true;
		}

		if($show){
			// fix the due date
			if (empty($item['due_dt'])) {
				$dueDate = 'No Due Date';
			} else {
				$tmpDate = (date('M d, Y', $item['due_dt']));
				$dueDate = (substr($tmpDate, -4) == $thisYear) ? substr($tmpDate, 0, -6) : $tmpDate;
				$compDDate = strtotime($tmpDate);
			}
			$tmpDate = (date('M d, Y h:i a', strtotime($item['strike_dt'])));

			if ($item['strike'] == 0) {
				$lysttype = "lyst_todo";
				$inpStrike = 1;
				$completedate = '';
				$checkbox = "cb_todo";
				++$todo_count;
			} else {
				$lysttype = "lyst_done";
				$inpStrike = 0;
				$completedate = (substr($tmpDate, -4) == $thisYear) ? substr($tmpDate, 0, -6) : $tmpDate;
				$checkbox = "cb_done";
				++$done_count;
			}

			$rowClass = '';
			if (($dueDate != 'No Due Date') && ($compDDate == $compDate) && ($item['strike'] != 1)) {
				$rowClass = ' notify';
			} else if (($dueDate != 'No Due Date') && ($compDDate < $compDate) && ($item['strike'] != 1)) {
				$rowClass = ' urgent';
			}

?>
			<li id="item<?= $item['item_id'] ?>" class="<?= $lysttype ?><?= $rowClass ?>">
				<input type="hidden" id="struck<?= $item['item_id'] ?>" value="<?= $inpStrike ?>" />
				<div class="dates">
					<div class="duedate"><?= $dueDate ?></div>
				<?php if ($item['strike'] == 1) { ?>
					<div class="completedate">Completed on: <?= $completedate ?></div>
				<?php } ?>
				</div>
				<div class="right_side">
					<a href="lystitems.php?id=<?= $project_id ?>&iid=<?= $item['item_id'] ?>&strike=<?= $item['strike'] ?>">
						<img src="/apps/iPhone/images/edit-icon@2x.png" width="29" height="29" />
					</a>
				</div>
			<?php if ($project->permission == 'WRITE' || $project->permission == 'ADMIN') { ?>
				<div class="item"><?= $item['item_text'] ?></div>
				<div class="left_side <?= $checkbox ?>" onclick="checkItem(<?= $item['item_id'] ?>,<?= $project_id ?>)">&nbsp;</div>
			<?php } else { ?>
				<div class="item"><?= $item['item_text'] ?></div>
			<?php } ?>
			</li>
<?php
		}
    }

	if ($done_count == 0) {
		echo "<li class='redText lyst_done'><p>There is nothing done.</p></li>";
	}
	if ($todo_count == 0) {
		if ($project->permission == 'WRITE' || $project->permission == 'ADMIN') {
			echo "<div class='noitems lyst_todo'>To add an item, tap this Plus button.<img src='\images\arrow_up.png' /></div>";
		} else {
			echo "<li class='redText lyst_todo'><p>There is nothing to do.</p></li>";
		}
	}
}
?>