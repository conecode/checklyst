<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

if ($_POST) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $cookie = true;

    if (validLoginForm($username, $password)) {
        if ($account->login($username, $password, $cookie)) {
            echo "YES";
        } else {
			echo mobileErrors();
        }
    } else {
		echo mobileErrors();
    }
}