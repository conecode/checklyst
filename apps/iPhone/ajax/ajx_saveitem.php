<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];

    // check if the name is valid
    $project_id = (int)getValue('lid', 0);
    $iid = (int)getValue('iid', 0);
    $args['item_text'] = str_replace("!%#","<br>", cleanString(getValue('item', '', $_POST)));
    $args['due_dt'] = getValue('thedate', '', $_POST);

    $project = new ProjectList($account_id, $project_id);

    if ($iid == 0) {
        // create a new project in the database
        if (!$project->createListItem($args)) {
            echo 'Could not save lyst.';
        } else {
            $project->accessTimeStamp();
            echo 'Item added!';
        }
    } else {
        // verify the project id is valid
        if (!$project->updateListItem($iid, $args)) {
            echo 'Could not update lyst';
        } else {
            $project->accessTimeStamp();
            echo 'Item updated!';
        }
    }
}
?>