<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];

    // get a list of projects for the current account
    $project = new ProjectList($account_id);

    // set the specified project or the last accessed project
    $project_id = (!empty($_REQUEST['lid'])) ? (int)$_REQUEST['lid'] : $project->getLastAccess();

    // get the information and checklist for the specified project
    if (!empty($project_id)) {
        $project->setProject($project_id);
    }

    // set the specified project or the last accessed project
    $itemInfo = $project->getListItem($_REQUEST['iid']);
    
    $iteText = getValue('item_text', '', $itemInfo);
    $dueDate = getValue('due_dt', '', $itemInfo);
    if ($dueDate == "")
        $dueDate = "X";
    else
        $dueDate = date('Y/m/d',$dueDate);
?>
<items>
    <item itemText="<?php echo str_replace('&', '^$%', str_replace('\\', '#$%', str_replace('<', '!^*', str_replace('>', '*^!', str_replace('"', '^^', $iteText))))) ?>" dueDate="<?php echo $dueDate ?>" />
</items>
<?php } else { ?>
<items>
    <item itemText="NLI" dueDate="NLI" />
</items>
<?php } ?>