<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();
$account->logout();

if ($_POST) {
    if (validRegisterForm($_POST)) {
        $args['account_username'] = $_POST['username'];
        $args['account_password'] = $_POST['password'];
        $args['account_email'] = $_POST['email'];
        $args['display_name'] = $_POST['displayName'];

        if ($account->create($args)) {
            $account->createSettings();
        }
    }
}

$loggedin = $account->confirmSession();
if (!$loggedin) {
    openPage('Register');
    ?>
    <div id="wrap">
        <br /><br />
        <!-- form to create an account -->
        <form class="signin" action="register.php" method="post">
            <div class="formgrp">
                <input class="input" id="username" maxlength="50" name="username" type="text" value="<?php echo getValue('username', '', $tmpVals) ?>" placeholder="Username" />
            </div>
            <div class="formgrp">
                <input class="input" id="password" maxlength="32" name="password" type="password" placeholder="Password" />
            </div>
            <div class="formgrp">
                <input class="input" id="confirm" maxlength="32" name="confirm" type="password" placeholder="Confirm Password" />
            </div>
            <div class="formgrp">
                <input class="input" id="email" maxlength="50" name="email" type="email" value="<?php echo getValue('email') ?>" placeholder="Email Address" />
            </div>
            <div class="formgrp">
                <input class="input" id="your_name" maxlength="50" name="your_name" type="text" value="<?php echo getValue('your_name') ?>" placeholder="Your Name" />
            </div>
            <div class="formgrp">
                <button class="button" type="submit">Create Account</button>
            </div>
        </form>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <?php
} else {
    openPage('Logged In');
}
closePage();
?>