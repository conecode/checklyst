<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    require_once('library/cls.projectlist.php');
    $account_id = (int) $_SESSION['account_id'];
    // get a list of projects for the current account
    $project = new ProjectList($account_id);
    
    $project->setProject($_GET['id']);
    $projectinfo = $project->get();
    $peoplelist = $project->readPeople();
    
    $owner = getValue('owner_id', 0, $projectinfo);

    $count = sizeOf($peoplelist);
    if ($count == 0) {
            echo "<li>Not available</li>\n";
    }
    openPage($projectinfo['project_name']);
?>
    <div>
        <!-- projects --> 
        <div class="group">
            <ul id="people" class="groupItems">
<?php
    foreach ($peoplelist as $person) {
        $id = $person['account_id'];
        if ($id == $owner) {
                $span = '<span class="permission">owner</span>';
        } else {
                $span = '<span class="permission">' . strtolower($person['permission']) . '</span>';
        }
?>
                <li>
                    <div class="lyst">
                        <?= $person['display_name'] ?>
                        <?= $span ?>
                    </div>
                </li>
<?php } ?>
            </ul>
        </div>
    </div>
    </script>
    <?php
} else {
    openPage('Not Logged In');
}
closePage();
?>