<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');

$account = new Account();
$loggedin = $account->confirmSession();

if($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];

    // get a list of projects for the current account
    $project = new ProjectList($account_id);

    // set the specified project or the last accessed project
    $project_id = (!empty($_GET['lid'])) ? (int) $_GET['lid'] : $project->getLastAccess();

    // get the information and checklist for the specified project
    if (!empty($project_id)) {
        $project->setProject($project_id);
    }

    openPage($projectinfo['project_name']);
?>
    <!-- list items -->
    <div class="group">
        <ul id="projects" class="groupItems">
<?php include 'ajax/ajx_lystitems.php'; ?>
        </ul>
    </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script language="javascript">
    showItems(<?= $_REQUEST['strike'] ?>);
    function getURLVars(field) {
        return getVars()[field];
    }

    function getVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    function checkItem(iid,lid) {
        strike = $("#struck"+iid).val();
        $.get("ajax/ajx_strikeitem.php", { iid: iid, strike: strike, lid: lid },
        function(good){
            if (good) {
                if (strike == 1) {
                    $("#struck"+iid).val(0);
                    $('#item' + iid + ' .left_side').removeClass('cb_todo').addClass('cb_done');
                } else {
                    $("#struck"+iid).val(1);
                    $('#item' + iid + ' .left_side').removeClass('cb_done').addClass('cb_todo');
                }
            }
        });
    }

    function showItems(strike) {
        $('.singleout').removeClass('singleout');
        if (strike == 1) {
            $('.lyst_done').show();
            $('.lyst_todo').hide();
        } else {
			if ($('#lefttodo').val() == 0) {
				$('body').css('background-color', '#EBE6CF');
			}
            $('.lyst_done').hide();
            $('.lyst_todo').show();
        }
    }

    function sortItems(strike,sort) {
		$.get("ajax/ajx_lystitems.php", { id: <?= $_GET['id'] ?>, sort: sort, strike: strike },
        function(data){
            $('#projects').html(data);
			showItems(strike);
        });
    }

    function hideItems(strike) {
        $('.lyst_done').hide();
        $('.lyst_todo').hide();
    }

    function deleteItem(iid,lid) {
        $.get("ajax/ajx_deleteitem.php", { iid: iid, lid: lid });
    }

    function getPermission(){
        return "<?= $project->permission ?>";
    }
</script>
<?php
} else {
    openPage('Not Logged In');
}
closePage();
?>