<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/fnc.validate.php');

$account = new Account();

$loggedin = $account->confirmSession();

if ($loggedin) {
	require_once('library/cls.projectlist.php');
	// get the current account
	$account_id = (int) $_SESSION['account_id'];

	// get a list of projects for the current account
	$project = new ProjectList($account_id);
	$projectlist = $project->read();

	openPage('My Lysts');
	?>
	<div id="wrap">
		<ul>
			<li class="edit_message">Tap a lyst to edit.</li>

			<li id="new_lyst">
				<form id="prjform" action="javascript:saveLyst();">
					<input class="new_input" name="prjname0" id="prjname0" placeholder="Lyst Name" />
					<div id="ed0" class="edit_bar_n">
						<div class="btn_cancel" onclick="closeEdit(0)"></div>
						<div class="btn_save" onclick="saveLyst(0);"></div>
					</div>
				</form>
			</li>

			<?php
			$count = sizeOf($projectlist);
			if ($count == 0) {
				echo "<div class='noitems'>To add a lyst, tap this Plus button.<img src='\images\arrow_up.png' /></div><br />";
			}

			foreach ($projectlist as $aproject) {
				$id = $aproject['project_id'];
				if ($_GET['lid'] == $id) {
					$selected = " highlight";
				} else {
					$selected = "";
				}

				$complete = $project->completePercent($id);
			?>
			<li>
				<?php if ($aproject['permission'] != 'READ') { ?>

				<a id="ne<?= $id ?>" class="no_edit<?= $selected ?>" href="mylysts.php?lid=<?= $id ?>&lname=<?= $aproject['project_name'] ?>" onclick="$(this).addClass('highlight')">
					<?= $aproject['project_name'] ?>
					<div class="progress"><div class="progreess_fill" style="width:<?= $complete['CompPercent'] ?>%"></div></div>
					<div class="greyarrow"></div>
				</a>
				<div id="em<?= $id ?>" class="edit_mode" onclick="editLyst(<?= $id ?>)">
					<?= $aproject['project_name'] ?>
					<div class="progress"><div class="progreess_fill" style="width:<?= $complete['CompPercent'] ?>%"></div></div>
				</div>
				<form id="prjform" action="javascript:updateLyst(<?= $id ?>);">
					<div id="el<?= $id ?>" class="edit_lyst">
						<div class="delete_icon" onclick="deleteMode(this, <?= $id ?>)"></div>
						<input class="edit_input" name="prjname<?= $id ?>" id="prjname<?= $id ?>" placeholder="Lyst Name" value="<?= $aproject['project_name'] ?>" />
						<div id="delbutton<?= $id ?>" class="btn_delete" onclick="deleteLyst(<?= $id ?>)"></div>
					</div>
					<div id="ed<?= $id ?>" class="edit_bar">
						<div class="btn_cancel" onclick="closeEdit(<?= $id ?>)"></div>
						<div class="btn_save" onclick="updateLyst(<?= $id ?>);"></div>
					</div>
				</form>

				<?php } else { ?>

				<a id="ne<?= $id ?>" class="no_edit<?= $selected ?>" href="mylysts.php?lid=<?= $id ?>&lname=<?= $aproject['project_name'] ?>" onclick="$(this).addClass('highlight')">
					<?= $aproject['project_name'] ?>
					<div class="progress"><div class="progreess_fill" style="width:<?= $complete['CompPercent'] ?>%"></div></div>
					<div class="greyarrow"></div>
				</a>

				<?php } ?>

				<input type="hidden" id="lname<?= $id ?>" value="<?= $aproject['project_name'] ?>" />
			</li>
			<?php } ?>
		</ul>
		<div id="push"></div>
	</div><!-- #wrap -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script language="javascript">
	function getURLVars(field) {
		return getVars()[field];
	}

	function getVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
	}

	function getLystName(id) {
		return $('#lname' + id).val();
	}

	function addLyst() {
		window.scrollTo(0, 0);
		editDone();
		$('#new_lyst').slideDown('fast');
		return "Yes";
	}

	function closeEdit(lid) {
		if (lid === 0) {
			$('#new_lyst').slideUp('fast');
		} else {
			$('#em' + lid).show();
			$('#el' + lid).hide();
			$('#ed' + lid).slideUp('fast');
		}
	}

	function editLysts() {
		$('#new_lyst').slideUp('fast');
		$('.no_edit').hide();
		$('.edit_mode').show();
		$('.edit_message').slideDown('fast');
		return "Yes";
	}

	function editDone() {
		window.scrollTo(0, 0);
		$('.no_edit').show();
		$('.delete_icon').css('background-position', '0px 0px');
		$('.btn_delete').hide();
		$('.edit_mode').hide();
		$('.edit_lyst').hide();
		$('.edit_message').slideUp('fast');
		$('.edit_bar').slideUp('fast');
		return "Yes";
	}

	function editLyst(lid) {
		$('.edit_lyst').hide();
		$('.edit_bar').slideUp('fast');
		$('.delete_icon').css('background-position', '0 0');
		$('.btn_delete').css('width', '0').hide();
		$('.edit_mode').show();
		$('#em' + lid).hide();
		$('#el' + lid).show();
		$('#ed' + lid).slideDown('fast');
	}

	var rotate = {
		ele: null,
		pos: 0,
		tmr: null,

		icon: function (dir) {
			var ths = rotate
			ths.pos = (dir == '+') ? ths.pos + 29 : ths.pos - 29;
			if (ths.pos >= 0 && ths.pos <= 261) {
				ths.ele.css('background-position', '0 -' + ths.pos + 'px');
			} else {
				clearTimeout(ths.tmr);
			}
		}
	}

	function deleteMode(el, lid) {
		var rot = rotate;
		rot.ele = $(el);

		if (rot.ele.css('background-position') == '0px -261px') {
			rot.pos = 261;
			rot.tmr = setInterval('rotate.icon("-")', 10);
			$('#delbutton' + lid).animate({'width': '0'}, 'fast', function() {
				$(this).hide();
			});
		} else {
			rot.pos = 0;
			rot.tmr = setInterval('rotate.icon("+")', 10);
			$('#delbutton' + lid).show().animate({'width': '61px'}, 'fast');
		}
	}

	function deleteLyst(lid) {
		$('#delbutton' + lid).hide().animate({'width': '0'}, 'fast');
		$.post('ajax/ajx_deletelyst.php', { 'lid': lid },
		function() {
			$('#ne' + lid).parent().remove();
		});
	}

	function saveLyst() {
		var nam = $('#prjname0').val();
		if(nam != '') {
			$.post('ajax/ajx_savelyst.php', { 'act': 'create', 'lid': 0, 'nam': nam },
			function() {
				$('#new_lyst').slideUp('fast');
				window.location = "mylysts.php";
			});
		} else {
			$('#new_lyst').slideUp('fast');
		}
	}

	function shareLyst(lid) {
		window.location = "mylysts.php?edit=<?= $edit ?>&share="+lid+"&el="+lid;
	}

	function updateLyst(lid) {
		var nam = $('#prjname'+lid).val();
		$.post('ajax/ajx_savelyst.php', { 'act': 'updaet', 'lid': lid, 'nam': nam },
		function() {
			$('#new_lyst').slideUp('fast');
			window.location = "mylysts.php?edit=<?= $edit ?>";
		});
	}
	<?php if ($_GET['edit']) { ?>
		editLysts();
	<?php } ?>
	</script>
	<?php
} else {
	openPage('Not Logged In');
}
closePage();
?>