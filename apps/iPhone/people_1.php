<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.access.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];
	$access = new Access;

	$people = $access->read($account_id);
?>
<people>
<?php foreach ($people as $person) { ?>
	<person link_id="<?= $person['people_link_id'] ?>" account_id="<?= $person['account_id'] ?>" name="<?= $person['display_name'] ?>" />
<?php } ?>
</people>
<?php
}
?>