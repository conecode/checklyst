<?php
require_once('templates/tpl.iphone.php');
require_once('library/cls.account.php');
require_once('library/cls.invite.php');

$account = new Account();
$loggedin = $account->confirmSession();

if ($loggedin) {
    // get the current account
    $account_id = (int)$_SESSION['account_id'];
	$invite = new Invite;

	$invites = $invite->read($account_id);
?>
<invites>
<?php foreach ($invites as $invite) { ?>
	<invite invite_id="<?= $invite['invite_id'] ?>" name="<?= $invite['name'] ?>" email="<?= $invite['email'] ?>" invite_dt="<?= date('M d, Y',$invite['invite_dt']) ?>" />
<?php } ?>
</invites>
<?php
}
?>