<?php
$browser = get_browser(null, true);
//$browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/fnc.handlers.php');
require_once('library/cls.device.php');

date_default_timezone_set("America/Los_Angeles");

// verify user is logged in
if ($_REQUEST['udid'] == '') {
	catchErr("No UDID sent.");
} else {
	$device = new Device($_REQUEST['udid'], "", "", $_REQUEST['name']);
	$args['account_username'] = $_REQUEST['username'];
	$args['account_password'] = $_REQUEST['password'];
	$args['account_email'] = $_REQUEST['email'];
	$args['display_name'] = $_REQUEST['name'];
	$checkdevice = $device->createFromUDID($args);
	$login_code = $checkdevice['login_code'];
	$account_id = $checkdevice['id'];
	$account_username = $checkdevice['username'];
	$account_email = $checkdevice['email'];
	$account_udid = $checkdevice['udid'];
	$display_name = $checkdevice['name'];
	if ($account_id > 0) {
		catchErr("YES");
	}
	header("Content-Type:text/xml");
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
		<dict>
			<key>errors</key>
			<string><?php echo mobileErrors(); ?></string>
			<key>login_code</key>
			<string><?php echo $login_code ?></string>
			<key>account_id</key>
			<string><?php echo $account_id ?></string>
			<key>account_username</key>
			<string><?php echo $account_username ?></string>
			<key>account_email</key>
			<string><?php echo $account_email ?></string>
			<key>account_udid</key>
			<string><?php echo $account_udid ?></string>
			<key>display_name</key>
			<string><?php echo $display_name ?></string>
		</dict>
	</plist>
<?php } ?>