<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.access.php');
require_once('library/cls.invite.php');
require_once('library/cls.device.php');
require_once('library/cls.projects.php');
require_once('library/fnc.handlers.php');

date_default_timezone_set("America/Los_Angeles");

// $browser = get_browser(null, true);
// $browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');

// MY INFORMATION :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$device = new Device($_REQUEST['udid'], $_REQUEST['aid'], $_REQUEST['lcode']);
if ($device->checkDevice()) {
	$account_id = $_REQUEST['aid'];

	// get a list of my admin projects
	$objProject  = new Projects($account_id);
	$my_projects = $objProject->read('project_name', 'ADMIN');

	// get a list of people i work with
	$objAccess = new Access();
	$my_people = $objAccess->read($account_id);

	// get a list of invites
	$objInvite  = new Invite();
	$my_invites = $objInvite->read($account_id);
} else {
	catchErr("Incorrect Login");
}

// PAGE FUNCTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * convert an invite permissions string into an array
 *
 * @param string $permissions
 * @return array
 */
function inviteAccess($permissions)
{
	$arrTmp = explode(',', $permissions);
	$access = array();

	foreach ($arrTmp as $strTmp) {
		$project_id = (int)substr($strTmp, 0, -2);
		switch ((int)substr($strTmp, -1)) {
			case 1:
				$access[$project_id] = 'READ';
				break;
			case 2:
				$access[$project_id] = 'WRITE';
				break;
			case 3:
				$access[$project_id] = 'ADMIN';
				break;
		}
	}

	return $access;
}

header ("Content-Type:text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>errors</key>
	<string><?php echo mobileErrors(); ?></string>
	<key>mylysts</key>
	<array>
<?php
foreach ($my_projects as $project) { ?>
		<dict>
			<key>lystID</key>
			<integer><?php echo $project['project_id'] ?></integer>
			<key>lystName</key>
			<string><?php echo $project['project_name'] ?></string>
			<key>permission</key>
			<string></string>
		</dict>
<?php
}
?>
	</array>
	<key>people</key>
	<array>
<?php

foreach ($my_people as $person) {

	// get a list of permissions for the user
	$usr_access = $objAccess->readAccess($person['account_id']);
	?>
		<dict>
			<key>userID</key>
			<integer><?php echo $person['account_id'] ?></integer>
			<key>userName</key>
			<string><?php echo $person['display_name'] ?></string>
			<key>code</key>
			<string>none</string>
			<key>lysts</key>
			<array>
<?php foreach ($my_projects as $project) { ?>
				<dict>
					<key>lystID</key>
					<integer><?php echo $project['project_id'] ?></integer>
					<key>lystName</key>
					<string><?php echo $project['project_name'] ?></string>
					<key>permission</key>
					<string><?php echo getValue($project['project_id'], '', $usr_access); ?></string>
				</dict>
<?php } ?>
			</array>
		</dict>
<?php
}

foreach ($my_invites as $person) {

	// get a list of permissions for the user
	$inv_info = $objInvite->getById($person['invite_id']);
	$inv_access = inviteAccess($inv_info['permissions']);
	?>
		<dict>
			<key>userID</key>
			<integer><?php echo $person['invite_id'] ?></integer>
			<key>userName</key>
			<string><?php echo $person['name'] ?></string>
			<key>userEmail</key>
			<string><?php echo $person['email'] ?></string>
			<key>code</key>
			<string><?php echo $person['code'] ?></string>
			<key>lysts</key>
			<array>
<?php foreach ($my_projects as $project) { ?>
				<dict>
					<key>lystID</key>
					<integer><?php echo $project['project_id'] ?></integer>
					<key>lystName</key>
					<string><?php echo $project['project_name'] ?></string>
					<key>permission</key>
					<string><?php echo getValue($project['project_id'], '', $inv_access); ?></string>
				</dict>
<?php } ?>
			</array>
		</dict>
<?php
}
?>
	</array>
</dict>
</plist>