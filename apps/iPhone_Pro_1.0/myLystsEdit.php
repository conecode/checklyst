<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.projects.php');
require_once('library/fnc.handlers.php');

date_default_timezone_set("America/Los_Angeles");

// get the specified ajax action
$action = getValue('act');

// verify user is logged in
$account = new Account();
if (true) { // ($account->confirmSession()) {
	// get the account and project ids
	// $account_id = (int)$_SESSION['account_id'];
	$account_id = 13;

	// get the specified lyst id
	$project_id = (int)getValue('prj', 0);
	$project = new Projects($account_id);
} else {
	catchErr('Could not acquire account information.');
	$action = 'err';
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * run the commands needed to create or update a project
 *
 * @global Projects $project
 * @param array $post
 * @param string $action ['CREATE' or 'UPDATE']
 * @return string
 */
function ajxSaveProject($post, $action)
{
	global $project;

	// check if the name is valid
	$name = getValue('nam', '', $post);
	if (empty($name)) {
		catchErr('The project name cannot be empty');
		return false;
	}

	// fix any html entities
	$name = cleanString($name);

	if ($action == 'CREATE') {
		// create a new project in the database
		if (!$project->create($name)) {
			return false;
		}
		$project_id = getValue('cur', 0);
		if ($project_id == 0) {
			$project->accessTimeStamp();
		}
	} else {
		// verify the project id is valid
		if ($project->project_id == 0) {
			catchErr('A project identifier could not be acquired.');
			return false;
		} else {
			$project->setProject($project_id);
		}

		// update the database
		if (!$project->update($name)) {
			return false;
		}
		$project_id = $project->project_id;
	}

	return $project_id;
}

/**
 * run the command needed to switch the deleted flag
 *
 * @global Projects $project
 * @return string
 */
function ajxFlagProject()
{
	global $project;

	// verify the project id is valid
	if ($project->project_id == 0) {
		catchErr('A project identifier could not be acquired.');
		return false;
	} else {
		$project->setProject($project_id);
	}

	// update the database
	if (!$project->flag(true)) {
		return false;
	}

	return $project->project_id;
}

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($action) {
	case 'cp': // create a new project
		$rslt = ajxSaveProject($_POST, 'CREATE');
		echo (!$rslt) ? mobileErrors() : $rslt;
		break;
	case 'up': // update a project
		$rslt = ajxSaveProject($_POST, 'UPDATE');
		echo (!$rslt) ? mobileErrors() : $rslt;
		break;
	case 'tp': // toggle a project flag
		$rslt = ajxFlagProject();
		echo (!$rslt) ? mobileErrors() : $rslt;
		break;
	case 'err':
		echo mobileErrors();
		break;
	default:
		catchErr('An action could not be acquired.');
		echo mobileErrors();
}
