<?php
$browser = get_browser(null, true);
//$browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');
require_once('library/fnc.handlers.php');
require_once('library/cls.device.php');
// verify user is logged in
if ($_REQUEST['udid'] == '') {
	catchErr("No UDID sent.");
} else {
	$device = new Device($_REQUEST['udid'], $_REQUEST['aid'], $_REQUEST['lcode']);
	if ($device->checkDevice()) {
		$action = getValue('act', '', $_REQUEST);
		$project_id = (int) getValue('lid', 0, $_REQUEST);
		$account_id = getValue('aid', '', $_REQUEST);

		$project = new ProjectList($account_id, $project_id);

		// fix any html entities
		$name = cleanString(getValue('nam', '', $_REQUEST));
		$name = cleanString($name);

		if ($action == 'create') {
			// create a new project in the database
			if (!$project->create($name)) {
				catchErr("Could not save lyst.");
			}

			if ($project_id == 0) {
				$project->accessTimeStamp();
			}
		} else if ($action == 'update') {
			// verify the project id is valid
			if ($project->project_id == 0) {
				catchErr("A project identifier could not be acquired.");
			}

			// update the database
			if (!$project->update($name)) {
				catchErr("Could not update lyst");
			}
			$project_id = $project->project_id;
		} else if ($action == 'delete') {
			// verify the project id is valid
			if ($project->project_id == 0) {
				catchErr("A project identifier could not be acquired.");
			}

			// update the database
			if (!$project->remove()) {
				catchErr("Could not delete lyst");
			}
		}
	} else {
		catchErr("Incorrect Login");
	}
}
require_once('checklystPLIST.php');
?>