<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.projects.php');
require_once('library/fnc.handlers.php');

date_default_timezone_set("America/Los_Angeles");

// verify user is logged in
$account = new Account();
if (true) { // ($account->confirmSession()) {
	// get the account and project list
	// $account_id = (int)$_SESSION['account_id'];
	$account_id  = 13;

	$objProject  = new Projects($account_id);
	$projectlist = $objProject->read();
} else {
	catchErr('Could not acquire account information.');
}

header ("Content-Type:text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>errors</key>
	<string><?php echo mobileErrors(); ?></string>
	<key>lysts</key>
	<array>
<?php
foreach ($projectlist as $project) {
	$items = $objProject->itemsCompleted($project['project_id']);
	if ($items['total'] > 0) {
		$percent = $items['completed'] / $items['total'];
	} else {
		$percent = 0;
	}
	?>
		<dict>
			<key>title</key>
			<string><?php echo $project['project_name'] ?></string>
			<key>progress</key>
			<string><?php echo round($percent, 2) ?></string>
			<key>id</key>
			<integer><?php echo $project['project_id'] ?></integer>
		</dict>
<?php
}
?>

	</array>
</dict>
</plist>