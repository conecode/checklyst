<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/fnc.handlers.php');
require_once('library/fnc.validate.php');

date_default_timezone_set("America/Los_Angeles");

// verify user is logged in
$account = new Account();
if (true) { // ($account->confirmSession()) {
	// get the account
	// $account_id = (int)$_SESSION['account_id'];
	$account_id  = 13;

	$user = $account->get($account_id);
} else {
	catchErr('Could not acquire account information.');
}

header ("Content-Type:text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>errors</key>
	<string><?php echo mobileErrors(); ?></string>
	<key>email</key>
	<string><?php echo $user['account_email'] ?></string>
	<key>username</key>
	<string><?php echo $user['account_username'] ?></string>
	<key>display_name</key>
	<string><?php echo $user['display_name'] ?></string>
</dict>
</plist>