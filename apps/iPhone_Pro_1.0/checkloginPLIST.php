<?php
$browser = get_browser(null, true);
//$browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/fnc.handlers.php');
require_once('library/cls.device.php');

date_default_timezone_set("America/Los_Angeles");

// verify user is logged in
if ($_REQUEST['udid'] == '') {
	catchErr("No UDID sent.");
} else {
	if (isset($_REQUEST['aid'])) {
		$device = new Device($_REQUEST['udid'], $_REQUEST['aid'], $_REQUEST['lcode']);
		$checkdevice = $device->get();
		if (!$device->checkDevice()) {
			if ($checkdevice['status']) {
				$login_code = $checkdevice['login_code'];
				$account_id = $checkdevice['id'];
				$account_username = $checkdevice['username'];
				$account_email = $checkdevice['email'];
				$account_udid = $checkdevice['udid'];
				$display_name = $checkdevice['name'];
			} else {
				catchErr("An error occurred.");
				$login_code = 0;
				$account_id = 0;
				$account_username = "";
				$account_email = "";
				$account_udid = "";
				$display_name = "";
			}
		} else {
			$login_code = $checkdevice['login_code'];
			$account_id = $checkdevice['id'];
			$account_username = $checkdevice['username'];
			$account_email = $checkdevice['email'];
			$account_udid = $checkdevice['udid'];
			$display_name = $checkdevice['name'];
		}
		header("Content-Type:text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		?>
		<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
		<plist version="1.0">
			<dict>
				<key>errors</key>
				<string><?php echo mobileErrors(); ?></string>
				<key>login_code</key>
				<string><?php echo $login_code ?></string>
				<key>account_id</key>
				<string><?php echo $account_id ?></string>
				<key>account_username</key>
				<string><?php echo $account_username ?></string>
				<key>account_email</key>
				<string><?php echo $account_email ?></string>
				<key>account_udid</key>
				<string><?php echo $account_udid ?></string>
				<key>display_name</key>
				<string><?php echo $display_name ?></string>
			</dict>
		</plist>
		<?php
	} else {
		$device = new Device($_REQUEST['udid'], '', '', $_REQUEST['name']);
		$username = $_REQUEST['username'];
		$password = $_REQUEST['password'];
		$account_id = $device->checkLogin($username, $password);
		if (!$account_id) {
			catchErr("NO");
			$username_exist = $device->usernameExists($username);
			header("Content-Type:text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<dict>
					<key>errors</key>
					<string><?php echo mobileErrors(); ?></string>
					<key>username</key>
					<string><?php echo ($username_exist == 1) ? 1 : 0 ?></string>
				</dict>
			</plist>
			<?php
		} else {
			catchErr("YES");
			$checkdevice = $device->getAccount();
			if(strlen($_REQUEST['newpass']) > 0) {
				$device->changePassword($_REQUEST['newpass'],$checkdevice['email']);
			}
			$login_code = $checkdevice['login_code'];
			$account_id = $checkdevice['id'];
			$account_username = $checkdevice['username'];
			$account_email = $checkdevice['email'];
			$account_udid = $checkdevice['udid'];
			$display_name = $checkdevice['name'];
			header("Content-Type:text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<dict>
					<key>errors</key>
					<string><?php echo mobileErrors(); ?></string>
					<key>login_code</key>
					<string><?php echo $login_code ?></string>
					<key>account_id</key>
					<string><?php echo $account_id ?></string>
					<key>account_username</key>
					<string><?php echo $account_username ?></string>
					<key>account_email</key>
					<string><?php echo $account_email ?></string>
					<key>account_udid</key>
					<string><?php echo $account_udid ?></string>
					<key>display_name</key>
					<string><?php echo $display_name ?></string>
				</dict>
			</plist>
			<?php
		}
	}
}
?>