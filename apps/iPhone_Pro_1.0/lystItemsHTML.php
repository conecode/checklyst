<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');
require_once('library/fnc.handlers.php');

date_default_timezone_set("America/Los_Angeles");

// verify user is logged in
$account = new Account();
if (true) { // ($account->confirmSession()) {
	// get the current account
	// $account_id = (int) $_SESSION['account_id'];
	$account_id = 13;

	// get the specified lyst id
	$project_id = getValue('id', 0);

	if (!empty($project_id)) {
		// get the information and checklist for the specified project
		$project = new ProjectList($account_id, $project_id);
		list($itemlist, $itemcount, $completed, $priorities) = $project->readList();
	} else {
		$itemlist = array();
		echo "Error: Unable to determine lyst identifier";
	}
} else {
	echo "Error: The user is not logged in";
}

//header ("Content-Type:text/xml");
?>
<html>
<body>
	<ul id="projects" class="groupItems">
<?php
foreach ($itemlist as $item) { ?>
		<li>
			<p><strong><?= $item['item_id'] ?></strong> - <?= $item['item_text'] ?></p>
		</li>
<?php
}
?>

	</ul>
</body>
</html>