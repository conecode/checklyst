<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.device.php');
require_once('library/cls.projects.php');
require_once('library/fnc.handlers.php');

date_default_timezone_set("America/Los_Angeles");

// $browser = get_browser(null, true);
// $browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');

// MY INFORMATION :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$device = new Device("X", 0, $_SERVER['HTTP_LCODE']);
$account_id = $device->checkDevice();
if ($account_id) {
	// check for a since date
	$since_dt = getValue('since');
	
	// get a list of my admin projects
	$objProject  = new Projects($account_id);
	$my_projects = $objProject->read('project_name', 'ADMIN', $since_dt);
} else {
	catchErr("Incorrect Login");
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	$arrOutput = array();
	
	$my_project_count = sizeOf($my_projects);
	for ($i = 0; $i < $my_project_count; $i++) {
		$project = $my_projects[$i];
		$project['created_dt'] = date("Y-m-d H:i:s", $project['created_dt']);
		$project['modified_dt'] = date("Y-m-d H:i:s", $project['modified_dt']);
		
		$items = $objProject->itemsCompleted($project['project_id']);
		if ($items['total'] > 0) {
			$percent = $items['completed'] / $items['total'];
			$project['completed'] = "{$items['completed']}";
			$project['total']     = "{$items['total']}";
			$project['progress']  = strval(round($percent, 2));
		} else {
			$project['completed'] = "0";
			$project['total']     = "0";
			$project['progress']  = "0";
		}
		
		$my_projects[$i] = $project;
	}
	$arrOutput['projects'] = $my_projects;
	
	echo json_encode($arrOutput);
}
