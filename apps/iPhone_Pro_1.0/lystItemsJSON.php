<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.device.php');
require_once('library/cls.projectlist.php');
require_once('library/fnc.handlers.php');

date_default_timezone_set("America/Los_Angeles");

// $browser = get_browser(null, true);
// $browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');

// MY INFORMATION :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$device = new Device("X", 0, $_SERVER['HTTP_LCODE']);
$account_id = $device->checkDevice();
if ($account_id) {
	$project_id = getValue('id', 0);
	
	// check for a since date
	$since_dt = getValue('since');
	
	if (!empty($project_id)) {
		// get the information and checklist for the specified project
		$project = new ProjectList($account_id, $project_id);
		list($itemlist, $itemcount, $completed, $priorities) = $project->readList("item_priority", $since_dt);
	} else {
		$itemlist = array();
		catchErr('Unable to determine lyst identifier.');
	}
} else {
	catchErr("Incorrect Login");
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	$arrOutput = array();
	
	$itemlist_count = sizeOf($itemlist);
	for ($i = 0; $i < $itemlist_count; $i++) {
		$item = $itemlist[$i];
		if ($item['due_dt']) {
			$item['due_dt'] = date("Y-m-d H:i:s", $item['due_dt']);
		} else {
			$item['due_dt'] = "";
		}
		if ($item['strike_dt']) {
			$item['strike_dt'] = date("Y-m-d H:i:s", $item['strike_dt']);
		} else {
			$item['strike_dt'] = "";
		}
		
		$item['created_dt']  = date("Y-m-d H:i:s", $item['created_dt']);
		$item['modified_dt'] = date("Y-m-d H:i:s", $item['modified_dt']);
		
		$itemlist[$i] = $item;
	}
	$arrOutput['items'] = $itemlist;
	
	echo json_encode($arrOutput);
}
