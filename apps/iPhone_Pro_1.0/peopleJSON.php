<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.access.php');
require_once('library/cls.device.php');
require_once('library/cls.invite.php');
require_once('library/cls.projects.php');
require_once('library/fnc.handlers.php');

date_default_timezone_set("America/Los_Angeles");

// $browser = get_browser(null, true);
// $browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');

// MY INFORMATION :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$device = new Device("X", 0, $_SERVER['HTTP_LCODE']);
$account_id = $device->checkDevice();
if ($account_id) {
	// get a list of my admin projects
	$objProject  = new Projects($account_id);
	$my_projects = $objProject->read('project_name', 'ADMIN');
	
	// check for a since date
	$since_dt = getValue('since');
	
	// get a list of people i work with
	$objAccess = new Access();
	$my_people = $objAccess->read($account_id, "display_name", $since_dt);
	
	// get a list of invites
	$objInvite  = new Invite();
	$my_invites = $objInvite->read($account_id, $since_dt);
} else {
	catchErr("Incorrect Login");
}

// PAGE FUNCTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * convert an invite permissions string into an array
 *
 * @param string $permissions
 * @return array
 */
function inviteAccess($permissions)
{
	$arrTmp = explode(',', $permissions);
	$access = array();

	foreach ($arrTmp as $strTmp) {
		$project_id = (int)substr($strTmp, 0, -2);
		switch ((int)substr($strTmp, -1)) {
			case 1:
				$access[$project_id] = 'READ';
				break;
			case 2:
				$access[$project_id] = 'WRITE';
				break;
			case 3:
				$access[$project_id] = 'ADMIN';
				break;
		}
	}

	return $access;
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	$arrOutput = array();
	
	$my_project_count = sizeOf($my_projects);
//	for ($i = 0; $i < $my_project_count; $i++) {
//		$project = $my_projects[$i];
//		$project['created_dt'] = date("Y-m-d H:i:s", $project['created_dt']);
//		$project['modified_dt'] = date("Y-m-d H:i:s", $project['modified_dt']);
//		$my_projects[$i] = $project;
//	}
//	$arrOutput['projects'] = $my_projects;
	
	$my_people_count = sizeOf($my_people);
	for ($i = 0; $i < $my_people_count; $i++) {
		$person = $my_people[$i];

		// get a list of permissions for the user
		$usr_access = $objAccess->readAccess($person['account_id']);
		
		for ($j = 0; $j < $my_project_count; $j++) {
			$project = $my_projects[$j];
			$project['permission'] = getValue($project['project_id'], '', $usr_access);
			$person['lysts'][$j] = $project;
		}
		$my_people[$i] = $person;
	}
	$arrOutput['people']   = $my_people;
	
	$my_invites_count = sizeOf($my_invites);
	for ($i = 0; $i < $my_invites_count; $i++) {
		$person = $my_invites[$i];
		$person['invite_dt'] = date("Y-m-d H:i:s", $person['invite_dt']);
		// get a list of permissions for the user
		$inv_info = $objInvite->getById($person['invite_id']);
		$inv_access = inviteAccess($inv_info['permissions']);
		
		for ($j = 0; $j < $my_project_count; $j++) {
			$project = $my_projects[$j];
			$project['permission'] = getValue($project['project_id'], '', $inv_access);
			$person['lysts'][$j] = $project;
		}
		$my_invites[$i] = $person;
	}
	$arrOutput['invites'] = $my_invites;
	
	echo json_encode($arrOutput);
}
