<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/fnc.handlers.php');
require_once('library/cls.device.php');
require_once('library/cls.invite.php');

date_default_timezone_set("America/Los_Angeles");

if ($_REQUEST['udid'] == '') {
	exit("iOS device was not detected.");
}

// verify user is logged in
$device = new Device($_REQUEST['udid'], $_REQUEST['aid'], $_REQUEST['lcode']);
if ($device->checkDevice()) {
	$action = getValue('act');		
	$invite = new Invite();

	if ($action == 'create') {
		$name  = getValue('name');
		$code  = getValue('code');
		$email = getValue('email');
		$perms = trim(getValue('perms'), ",");

		// create a new invite
		if (!$invite->create($name, $email, $code, $perms, $_REQUEST['aid'])) {
			catchErr("Could not create a new invitation.");
		}
	} else if ($action == 'update') {
		$invite_id = getValue('id');
		$perms = trim(getValue('perms'), ",");

		// verify the invite id is valid
		if ($invite_id == 0) {
			catchErr("An invite identifier could not be acquired.");
		}

		// update the database
		if (!$invite->update($invite_id, $perms)) {
			catchErr("Could not update the  invitation.");
		}
	} else if ($action == 'delete') {
		$invite_id = getValue('id');

		// verify the invite id is valid
		if ($invite_id == 0) {
			catchErr("An invite identifier could not be acquired.");
		}

		// update the database
		if (!$invite->remove($invite_id)) {
			catchErr("Could not delete the invitation.");
		}
	}
} else {
	catchErr("Incorrect Login");
}

header ("Content-Type:text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>errors</key>
	<string><?php echo mobileErrors(); ?></string>
</dict>
</plist>