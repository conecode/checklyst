<?php
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/fnc.handlers.php');
require_once('library/cls.device.php');
require_once('library/cls.access.php');

date_default_timezone_set("America/Los_Angeles");

function permissionsStringToArray($permissions)
{
	$arrTmp = explode(',', $permissions);
	$access = array();

	foreach ($arrTmp as $strTmp) {
		$project_id = (int)substr($strTmp, 0, -2);
		switch ((int)substr($strTmp, -1)) {
			case 1:
				$access[$project_id] = 'READ';
				break;
			case 2:
				$access[$project_id] = 'WRITE';
				break;
			case 3:
				$access[$project_id] = 'ADMIN';
				break;
			default:
				$access[$project_id] = 'NONE';
				break;
		}
	}

	return $access;
}

if ($_REQUEST['udid'] == '') {
	exit("iOS device was not detected.");
}

// verify user is logged in
$device = new Device($_REQUEST['udid'], $_REQUEST['aid'], $_REQUEST['lcode']);
if ($device->checkDevice()) {
	$action = getValue('act');		
	$objAccess  = new Access();

	if ($action == 'update') {
		$person_id = getValue('id');
		$strPerms = trim(getValue('perms'), ",");
		
		$arrPerms = permissionsStringToArray($strPerms);

		// get a list of my permissions
		$my_access  = $objAccess->readAccess($_REQUEST['aid']);
		$usr_access = $objAccess->readAccess($person_id);

		// loop to modify the users permissions
		foreach($my_access as $project_id => $my_prj_access) {

			// verify my permission to edit this project
			if ($my_prj_access === 'ADMIN') {
				$usr_prj_access = getValue($project_id, '', $usr_access);

				$permission = getValue($project_id, 'NONE', $arrPerms);

				// remove a permission
				if ($permission === 'NONE' && $usr_prj_access !== '') {
					$objAccess->remove($project_id, $person_id);
				}

				// create or update a permission
				if ($permission !== 'NONE' && $usr_prj_access !== $permission) {
					if ($usr_prj_access === '') {
						$objAccess->create($permission, $project_id, $person_id);
					} else {
						$objAccess->update($permission, $project_id, $person_id);
					}
				}
			}
		} // end foreach
	} else if ($action == 'delete') {
		$person_id = getValue('id');

		// verify the invite id is valid
		if ($person_id == 0) {
			catchErr("A person identifier could not be acquired.");
		}

		// update the database
		if (!$objAccess->removeLink($_REQUEST['aid'], $person_id)) {
			catchErr("Could not remove the person.");
		}
	}
} else {
	catchErr("Incorrect Login");
}


header ("Content-Type:text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>errors</key>
	<string><?php echo mobileErrors(); ?></string>
</dict>
</plist>