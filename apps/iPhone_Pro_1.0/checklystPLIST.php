<?php
$browser = get_browser(null, true);
//$browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/cls.projectlist.php');
require_once('library/fnc.handlers.php');
require_once('library/cls.device.php');

date_default_timezone_set("America/Los_Angeles");

// verify user is logged in
if ($_REQUEST['udid'] == '') {
	catchErr("No UDID sent.");
} else {
	$device = new Device($_REQUEST['udid'], $_REQUEST['aid'], $_REQUEST['lcode']);
	if ($device->checkDevice()) {
		$objProject = new ProjectList($_REQUEST['aid']);
		$projectlist = $objProject->read('project_name', 'ACTIVE');
	} else {
		catchErr("Incorrect Login");
	}
}
header("Content-Type:text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>errors</key>
		<string><?php echo mobileErrors(); ?></string>
		<?php if (count($projectlist) > 0) { ?>
			<key>lysts</key>
			<array>
				<?php
				foreach ($projectlist as $project) {
					$items = $objProject->itemsCompleted($project['project_id']);
					if ($items['total'] > 0) {
						$percent = $items['completed'] / $items['total'];
					} else {
						$percent = 0;
					}

					$deletedt = date('Y-m-d\Th:i:s\Z', $project['delete_dt']);
					$createdt = date('Y-m-d\Th:i:s\Z', $project['created_dt']);
					$modifieddt = date('Y-m-d\Th:i:s\Z', $project['modified_dt']);

					// get the checklist for the specified project
					$objProject->setProject($project['project_id']);
					list($itemlist, $itemcount, $completed, $priorities) = $objProject->readList();
					?>
					<dict>
						<key>projectname</key>
						<string><?php echo $project['project_name'] ?></string>
						<key>projectid</key>
						<integer><?php echo $project['project_id'] ?></integer>
						<key>progress</key>
						<string><?php echo round($percent, 2) ?></string>
						<key>permission</key>
						<string><?php echo $project['permission'] ?></string>
						<key>ownerid</key>
						<integer><?php echo $project['owner_id'] ?></integer>
						<key>deleted</key>
						<integer><?php echo ($project['deleted'] == '') ? 0 : $project['deleted'] ?></integer>
						<key>deletedt</key>
						<date><?php echo ($item['delete_dt'] == '') ? '1900-01-01T00:00:00Z' : $deletedt ?></date>
						<key>createddt</key>
						<date><?php echo $createdt ?></date>
						<key>modifieddt</key>
						<date><?php echo $modifieddt ?></date>
						<key>items</key>
						<array>
							<?php
							foreach ($itemlist as $item) {
								$duedt = date('Y-m-d\Th:i:s\Z', $item['due_dt']);
								$strikedt = date('Y-m-d\Th:i:s\Z', $item['strike_dt']);
								$createdt = date('Y-m-d\Th:i:s\Z', $item['created_dt']);
								$modifieddt = date('Y-m-d\Th:i:s\Z', $item['modified_dt']);
								?>
								<dict>
									<key>itemid</key>
									<integer><?php echo $item['item_id'] ?></integer>
									<key>itemtext</key>
									<string><?php echo str_replace("<br>", "\r\n", str_replace(">", "-*", str_replace("<", "*-", str_replace("<br />", "\r\n", $item['item_text'])))) ?></string>
									<key>itempriority</key>
									<integer><?php echo $item['item_priority'] ?></integer>
									<key>assignedto</key>
									<integer><?php echo $item['assigned_to'] ?></integer>
									<key>duedt</key>
									<date><?php echo $duedt ?></date>
									<key>strike</key>
									<integer><?php echo $item['strike'] ?></integer>
									<key>strikedt</key>
									<date><?php echo ($item['strike_dt'] == '') ? '1900-01-01T00:00:00Z' : $strikedt ?></date>
									<key>createddt</key>
									<date><?php echo $createdt ?></date>
									<key>modifieddt</key>
									<date><?php echo $modifieddt ?></date>
								</dict>
								<?php
							}
							?>
						</array>
					</dict>
				<?php } ?>
			</array>
		<?php } ?>
	</dict>
</plist>