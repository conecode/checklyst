<?php
$browser = get_browser(null, true);
//$browser['browser'] == "CFNetwork" or exit('You are not using an iOS Device');
require_once('checklyst.php');
require_once('library/cls.account.php');
require_once('library/fnc.handlers.php');
require_once('library/cls.device.php');

date_default_timezone_set("America/Los_Angeles");

// verify user is logged in
if ($_REQUEST['udid'] == '') {
	catchErr("No UDID sent.");
} else {
	$device = new Device($_REQUEST['udid']);
	if ($device->sendReminder($_REQUEST['username'])) {
		$email_sent = 1;
	} else {
		$email_sent = 0;
	}
}
header("Content-Type:text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>errors</key>
		<string><?php echo mobileErrors(); ?></string>
		<key>emailsent</key>
		<string><?php echo $email_sent ?></string>
	</dict>
</plist>