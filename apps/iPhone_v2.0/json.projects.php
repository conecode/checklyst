<?php
require_once('conf.checklyst.php');
require_once('clas.projects.php');
require_once('func.handlers.php');

date_default_timezone_set("America/Los_Angeles");

$json = file_get_contents('php://input');
$data = json_decode($json, true);

/*
 * CREATE
 *  - add a project to the database
 */
function action_create($data)
{
	$account_key  = $_SERVER['HTTP_KEY'];
	$project_name = $data['name'];
	$number       = $data['number'];
	$objProject   = new Projects($account_key);

	// add the project to the database
	$rslt = $objProject->create($project_name, $number);
	if (!$rslt) {
		return array('result' => false);
	}

	// output the project id
	return array('result' => $rslt);
}

/*
 * READ
 *  - get the projects that match a since date and account key
 */
function action_read($data)
{
	$account_key = $_SERVER['HTTP_KEY'];;
	$since_dt    = $data['since'];
	$objProject  = new Projects($account_key);

	$remove = $objProject->readRemoved($data['projects']);
	if (!$remove) {
		$remove = array();
	}
	
	// get a list of my projects
	$rslt = $objProject->read($since_dt);
	if (!$rslt) {
		$rslt = array();
	}

	// output the data
	return array('result' => $rslt, 'remove' => $remove);
}

/*
 * UPDATE
 *  - update a projects name
 */
function action_update($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objProject  = new Projects($account_key);
	$objProject->setProject($data['number']);
	$project_name = $data['name'];
	
	$rslt = $objProject->update($project_name);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

/*
 * DELETE
 *  - delete a specified project
 */
function action_delete($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objProject  = new Projects($account_key);
	$objProject->setProject($data['number']);

	$rslt = $objProject->flag(true);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

$action = strtoupper(getValue('action'));
if ($action == 'CREATE') {
	$arrOutput = action_create($data);
} else if ($action == 'READ') {
	$arrOutput = action_read($data);
} else if ($action == 'UPDATE') {
	$arrOutput = action_update($data);
} else if ($action == 'DELETE') {
	$arrOutput = action_delete($data);
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= (strlen($msg) > 0) ? "\n" . $value : $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	echo str_replace("\\/", "/", json_encode($arrOutput));
}
