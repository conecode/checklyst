<?php
require_once('conf.checklyst.php');
require_once('clas.items.php');
require_once('func.handlers.php');

date_default_timezone_set("America/Los_Angeles");

$json = file_get_contents('php://input');
$data = json_decode($json, true);

/*
 * CREATE
 *  - add an item to the database
 */
function action_create($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$items = $data['items'];
	$objItem = new Items($account_key);

	$item_ids = array();
	foreach ($items as $item) {
		$objItem->setProject($item['number']);

		$args = array();
		$args['item_text']     = $item['text'];
		$args['item_priority'] = $item['priority'];
		$args['created_by']    = $item['account_id'];
		$args['strike']        = $item['strike'];
		$args['strike_dt']     = date("Y-m-d H:i:s", $item['strike_dt']);
		$args['due_dt']        = date("Y-m-d H:i:s", $item['due_dt']);
		$args['created_dt']    = date("Y-m-d H:i:s", $item['created_dt']);
		$args['modified_dt']   = date("Y-m-d H:i:s", $item['modified_dt']);

		// add the item to the database
		$rslt = $objItem->createItem($args);
		if (!$rslt) {
			array_push($item_ids, 0);
			return array('result' => false);
		} else {
			array_push($item_ids, $rslt);
		}
	}

	// output the item id
	return array('result' => $item_ids);
}

/*
 * READ
 *  - get the items that match a since date
 */
function action_read($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objItem = new Items($account_key);
	$objItem->setProject($data['number']);

	$remove = $objItem->readRemovedItems($data['items']);
	if (!$remove) {
		$remove = array();
	}

	// get a list of my projects
	$rslt = $objItem->readItem($data['since']);
	if (!$rslt) {
		$rslt = array();
	}

	// output the data
	return array('result' => $rslt, 'remove' => $remove);
}

/*
 * UPDATE
 *  - update an items data
 */
function action_update($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$items = $data['items'];
	$objItem = new Items($account_key);

	$item_ids = array();
	foreach ($items as $item) {
		$objItem->setProject($item['number']);

		$args = array();
		$args['item_text']     = $item['text'];
		$args['item_priority'] = $item['priority'];
		$args['created_by']    = $item['account_id'];
		$args['strike']        = $item['strike'];
		$args['strike_dt']     = date("Y-m-d H:i:s", $item['strike_dt']);
		$args['due_dt']        = date("Y-m-d H:i:s", $item['due_dt']);
		$args['created_dt']    = date("Y-m-d H:i:s", $item['created_dt']);
		$args['modified_dt']   = date("Y-m-d H:i:s", $item['modified_dt']);

		// add the item to the database
		$rslt = $objItem->updateItem($item['item_id'], $args);
		if (!$rslt) {
			array_push($item_ids, 0);
			return array('result' => false);
		} else {
			array_push($item_ids, $rslt);
		}
	}

	return array('result' => $item_ids);
}

/*
 * STRIKE
 *  - set the strike value for an item
 */
function action_strike($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objItem = new Items($account_key);
	$objItem->setProject($data['number']);

	$item_id   = $data['item_id'];
	$strike    = $data['strike'];
	$strike_dt = date("Y-m-d H:i:s", $data['strike_dt']);

	$rslt = $objItem->strikeItem($item_id, $strike, $strike_dt);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

/*
 * DELETE
 *  - delete a specified item
 */
function action_delete($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objItem = new Items($account_key);
	$objItem->setProject($data['number']);

	$rslt = $objItem->removeItem($data['item_id']);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

/*
 * DELETEALL
 *  - delete a specified item
 */
function action_deleteall($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objItem = new Items($account_key);
	$objItem->setProject($data['number']);

	$rslt = $objItem->removeAll($data['project_id']);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

/*
 * DELETEALL
 *  - delete a specified item
 */
function action_uncheckall($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objItem = new Items($account_key);
	$objItem->setProject($data['number']);

	$rslt = $objItem->uncheckAll($data['project_id']);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

$action = strtoupper(getValue('action'));
if ($action == 'CREATE') {
	$arrOutput = action_create($data);
} else if ($action == 'READ') {
	$arrOutput = action_read($data);
} else if ($action == 'UPDATE') {
	$arrOutput = action_update($data);
} else if ($action == 'STRIKE') {
	$arrOutput = action_strike($data);
} else if ($action == 'DELETE') {
	$arrOutput = action_delete($data);
} else if ($action == 'DELETEALL') {
	$arrOutput = action_deleteall($data);
} else if ($action == 'UNCHECKALL') {
	$arrOutput = action_uncheckall($data);
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= (strlen($msg) > 0) ? "\n" . $value : $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	echo str_replace("\\/", "/", json_encode($arrOutput));
}

