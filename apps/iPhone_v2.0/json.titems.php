<?php
require_once('conf.checklyst.php');
require_once('clas.titems.php');
require_once('func.handlers.php');

date_default_timezone_set("America/Los_Angeles");

$json = file_get_contents('php://input');
$data = json_decode($json, true);

/*
 * CREATE
 *  - add an titem to the database
 */
function action_create($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$titems = $data['titems'];
	$objTItem = new TItems($account_key);

	$titem_ids = array();
	foreach ($titems as $titem) {
		$objTItem->setTemplate($titem['number']);

		$args = array();
		$args['titem_text']		= $titem['text'];
		$args['created_dt']		= date("Y-m-d H:i:s", $titem['created_dt']);
		$args['modified_dt']	= date("Y-m-d H:i:s", $titem['modified_dt']);

		// add the titem to the database
		$rslt = $objTItem->createTItem($args);
		if (!$rslt) {
			array_push($titem_ids, 0);
			return array('result' => false);
		} else {
			array_push($titem_ids, $rslt);
		}
	}

	// output the titem id
	return array('result' => $titem_ids);
}

/*
 * READ
 *  - get the titems that match a since date
 */
function action_read($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objTItem = new TItems($account_key);
	$objTItem->setTemplate($data['number']);

	$remove = $objTItem->readRemovedTItems($data['titems']);
	if (!$remove) {
		$remove = array();
	}

	// get a list of items
	$rslt = $objTItem->readTItem($data['since']);
	if (!$rslt) {
		$rslt = array();
	}

	// output the data
	return array('result' => $rslt, 'remove' => $remove);
}

/*
 * UPDATE
 *  - update an titems data
 */
function action_update($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$titems = $data['titems'];
	$objTItem = new TItems($account_key);

	$titem_ids = array();
	foreach ($titems as $titem) {
		$objTItem->setTemplate($titem['number']);

		$args = array();
		$args['titem_text']     = $titem['text'];
		$args['created_dt']    = date("Y-m-d H:i:s", $titem['created_dt']);
		$args['modified_dt']   = date("Y-m-d H:i:s", $titem['modified_dt']);

		// add the titem to the database
		$rslt = $objTItem->updateTItem($titem['titem_id'], $args);
		if (!$rslt) {
			array_push($titem_ids, 0);
			return array('result' => false);
		} else {
			array_push($titem_ids, $rslt);
		}
	}

	return array('result' => $titem_ids);
}

/*
 * STRIKE
 *  - set the strike value for an titem
 */
function action_strike($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objTItem = new TItems($account_key);
	$objTItem->setTemplate($data['number']);

	$titem_id   = $data['titem_id'];
	$strike    = $data['strike'];
	$strike_dt = date("Y-m-d H:i:s", $data['strike_dt']);

	$rslt = $objTItem->strikeTItem($titem_id, $strike, $strike_dt);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

/*
 * DELETE
 *  - delete a specified titem
 */
function action_delete($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objTItem = new TItems($account_key);
	$objTItem->setTemplate($data['number']);

	$rslt = $objTItem->removeTItem($data['titem_id']);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

/*
 * DELETEALL
 *  - delete a specified titem
 */
function action_deleteall($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objTItem = new TItems($account_key);
	$objTItem->setTemplate($data['number']);

	$rslt = $objTItem->removeAll($data['template_id']);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

$action = strtoupper(getValue('action'));
if ($action == 'CREATE') {
	$arrOutput = action_create($data);
} else if ($action == 'READ') {
	$arrOutput = action_read($data);
} else if ($action == 'UPDATE') {
	$arrOutput = action_update($data);
} else if ($action == 'STRIKE') {
	$arrOutput = action_strike($data);
} else if ($action == 'DELETE') {
	$arrOutput = action_delete($data);
} else if ($action == 'DELETEALL') {
	$arrOutput = action_deleteall($data);
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= (strlen($msg) > 0) ? "\n" . $value : $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	echo str_replace("\\/", "/", json_encode($arrOutput));
}

