<?php

require_once "clas.mysqli_database.php";

/**
 * @author Cone Code Development
 * @version 3.3
 */
class Account {
    /**
     * property contains unique account identifier
     * @var integer
     */
    var $account_key;
    /**
     * property contains the database object
     * @var object
     */
    var $mysqli;

    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function __construct($account_key = '') {
        $this->mysqli = new Mysqli_Database();

        if ($account_key != '') {
            $this->account_key = $account_key;
        }
    }

    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * send mail notification to user
     *
     * @param string $body email message
     * @param string $recipient email address
     * @return boolean
     * @access private
     */
    private function _sendEmail($body, $recipient) {
        // build email headers
        $header = "From: Checklyst <no-reply@checklyst.com>\r\n";
        $header .= "Reply-To: no-reply@checklyst.com\r\n";
        $header .= "X-Mailer: PHP/" . phpversion() . "\r\n";

        $subject = SITE_NAME . " Account Notification";

        // try to send mail
        $send = mail($recipient, $subject, $body, $header);
        if (!$send) {
            catchErr("Notification email could not be sent.");
            return false;
        }

        return true;
    }

    /**
     * update user login values
     *
     * @return boolean
     * @access private
     */
    private function _updateLogin() {
        // get remote user ip address
        $ip = $_SERVER["REMOTE_ADDR"];

        // lookup host name if available
        $host = gethostbyaddr($ip);

        $sql = "UPDATE
                accounts
            SET
                last_login_dt = (NOW()),
                last_login_ip = '" . $ip . "',
                last_login_host = '" . $host . "'
            WHERE
                account_key = '" . $this->account_key . "'";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        return true;
    }

    /**
     * verfiy that a username and password are correct
     *
     * @param string $username
     * @param string $encryped encrypted password
     * @return array session values
     * @access private
     */
    private function _validateUser($username, $encryped) {
        $sql = "SELECT
                account_id,
                account_email,
                account_password,
                account_status,
                display_name,
				account_key
            FROM
                accounts
            WHERE
                account_username = ?
            LIMIT 1";

        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param("s", $username);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return true;
        }
        $stmt->store_result();

        // check if the account exists
        if ($stmt->num_rows == 0) {
            catchErr("The username or password you entered does not match any account in our records.");
            return false;
        }

        $stmt->bind_result($id, $email, $password, $status, $name, $key);
        $stmt->fetch();

        // check if the account has been deleted
        if ($status < 1) {
            catchErr("The user account you entered has been deactivated.");
            return false;
        }

        // check if the account password is correct
        if (strcmp($encryped, $password)) {
            catchErr("The username or password you entered does not match any account in our records.");
            return false;
        }
		$stmt->close();
		
		if (empty($key)) {
			$key = md5(time());

			$sql = "UPDATE
						accounts
					SET 
						account_key = ?,
						modified_dt = NOW()
					WHERE
						account_id = ?";

			$stmt = $this->mysqli->prepare($sql);
			$stmt->bind_param("si", $key, $id);
			if (!$stmt->execute()) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			$stmt->close();
		}
		
		$return = array();
		$return["id"]    = $id;
		$return["key"]   = $key;
		$return["email"] = $email;
		$return["name"]  = $name;

        return $return;
    }

    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
     * log into a user account
     *
     * @param string $username
     * @param string $password not encrypted
     * @param boolean $cookie true creates a long term cookie
     * @return boolean
     * @access public
     */
    function login($username, $password) {
        $password = substr(md5($password . PASSWORD_SALT), 4, 16);

        if (!$data = $this->_validateUser($username, $password)) {
            return false;
        }

        $this->account_key = $data["key"];
        $this->_updateLogin();

        return $data;
    }

    /**
     * verify unique email address
     *
     * @param string $email email address
     * @return boolean true if email exists
     * @access public
     */
    function emailExists($email) {
        $sql = "SELECT
					account_key
				FROM
					accounts
				WHERE
					account_email = ?
				AND
					account_status > 0
				LIMIT 1";

        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param("s", $email);

        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return true;
        }
        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            $return = true;
        } else {
            $return = false;
        }

        $stmt->close();
        return $return;
    }

    /**
     * verify unique username
     *
     * @param string $username
     * @return boolean true if username exists
     * @access public
     */
    function usernameExists($username) {
        $sql = "SELECT
                account_key
            FROM
                accounts
            WHERE
                account_username = ?
            AND
                account_status > 0
            LIMIT 1";

        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param("s", $username);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return true;
        }
        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            $return = true;
        } else {
            $return = false;
        }

        $stmt->close();
        return $return;
    }

    /**
     * email an account password to the associated user
     *
     * @param string $email
     * @return boolean
     * @access public
     */
    function sendReminder($email) {
        $this->mysqli->escape($email);

        // get account info from username
        $sql = "SELECT
                account_email,
                account_remind,
                account_username
            FROM
                accounts
            WHERE
                account_email = '" . $email . "'
            AND
                account_status > 0";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        if ($result->num_rows < 1) {
            catchErr("Your account information could not be found.");
            return false;
        }

        $obj = $result->fetch_object();

        $body = SITE_NAME . " has received a request for your account login ";
        $body .= "information.\r\n\r\n";
        $body .= "Your Username: " . $obj->account_username . "\r\n";
        $body .= "Your Password: " . $obj->account_remind;

        $result->close();

        // send email
        return $this->_sendEmail($body, $email);
    }

    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * get a single record
     *
     * @param integer $accountKey
     * @return array
     * @access public
     */
    function get($accountKey = '') {
        if ($accountKey == '') {
            $accountKey = $this->account_key;
        }

        // get account values
        $sql = "SELECT
                *
            FROM
                accounts
            WHERE
                account_key = " . $accountKey;

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $row = $result->fetch_assoc();

        if (!empty($row)) {
            // convert string values to times
            $row["last_login_dt"] = strtotime($row["last_login_dt"]);
            $row["deleted_dt"] = strtotime($row["deleted_dt"]);
            $row["created_dt"] = strtotime($row["created_dt"]);
            $row["modified_dt"] = strtotime($row["modified_dt"]);
        }

        $result->close();

        return $row;
    }

    // INSERT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * create a new record
     *
     * @param array $args [email, password, username, display name]
     * @return boolean
     * @access public
     */
    function create($args) {
        $reminder = $args["account_password"];
        $password = substr(md5($reminder . PASSWORD_SALT), 4, 16);

        $email = $args["account_email"];
		if (!empty($email)) {
			if ($this->emailExists($email)) {
				catchErr("The email address already exists.");
				return false;
			}
		}

        $username = $args["account_username"];
        if ($this->usernameExists($username)) {
            catchErr("The username already exists.");
            return false;
        }

        $display = htmlentities($args["display_name"]);
        if ($display == "") {
            $display = $username;
        }

        // lock the tables
        $result = $this->mysqli->query("LOCK TABLES accounts WRITE");
        if (!$result) {
            catchSQL("", $this->mysqli);
            return false;
        }

		$key = md5(time() . $username);

        // insert new account record
        $sql = "INSERT INTO accounts (
					account_email,
					account_password,
					account_remind,
					account_username,
					account_key,
					display_name,
					created_dt,
					modified_dt
				) values (
					?, ?, ?, ?, ?, ?,
					NOW(),
					NOW()
				)";

        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param("ssssss", $email, $password, $reminder, $username, $key, $display);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return true;
        }
        $stmt->close();

		// set the account key
        $this->account_key = $key;
		
		$return = array();
		$return['id']  = mysqli_insert_id($this->mysqli);
		$return['key'] = $key;

        // unlock the tables
        $result = $this->mysqli->query("UNLOCK TABLES");
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        // update the account table with the login info
        $this->_updateLogin();

        // send an email message
		if ($email) {
	        $body = "Hello " . $display . "!\r\n";
			$body .= "Thank you for signing up with " . SITE_NAME . ".\r\n\n";
			$body .= "Your Username: " . $username . "\r\n";
			$body .= "Your Password: " . $reminder;

			$this->_sendEmail($body, $email);
		}

        return $return;
    }

    // UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * update an existing account
     *
     * @param array $args (account_email, account_username, account_password, display_name)
     * @return boolean
     * @access public
     */
    function update($args) {
        // get the current email address and display name
        $sql = "SELECT
					account_username,
					account_email,
					display_name
				FROM
					accounts
				WHERE
					account_key = '" . $this->account_key . "'";

        $result = $this->mysqli->query($sql);
        if ($result->num_rows == 0) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $current = $result->fetch_object();
        $result->close();

        // build the sql values and email info
        $values = "";
        $body = "";

        // check if we need to update the email address
        if (isset($args["account_email"])) {
			if ($args["account_email"] != $current->account_email) {
				if ($this->emailExists($args["account_email"])) {
					catchErr("That email already exists.");
					return false;
				}
				$values .= "account_email = '" . $args["account_email"] . "'";
				$body .= "Your New Email Address: " . $args["account_email"] . "\r\n";
			}
        }

        // check if we need to update the username
        if (isset($args["account_username"])) {
			if ($args["account_username"] != $current->account_username) {
				if ($this->usernameExists($args["account_username"])) {
					catchErr("That username already exists.");
					return false;
				}
				if (!empty($values)) {
					$values .= ", ";
				}
				$values .= "account_username = '" . $args['account_username'] . "'";
				$body .= "Your New Username: " . $args['account_username'] . "\r\n";
			}
        }

        // check if we need to update the password
        if (isset($args["account_password"])) {
            $password = substr(md5($args["account_password"] . PASSWORD_SALT), 4, 16);

            if (!empty($values)) {
                $values .= ", ";
            }

            $values .= "account_remind = '" . $args['account_password'] . "', ";
            $values .= "account_password = '" . $password . "'";
            $body .= "Your New Password: " . $args['account_password'] . "\r\n";
        }

        // check if we need to update the display name
        if (isset($args["display_name"])) {
            $display = $args["display_name"];
            if (!empty($values)) {
                $values .= ", ";
            }
            $values .= "display_name = '" . htmlentities($args['display_name']) . "'";
        } else {
            $display = $current->display_name;
        }

        if (empty($values)) {
            catchErr("The account information you entered has not changed.");
            return false;
        }

        // update the account record
        $sql = "UPDATE
					accounts
				SET
					" . $values . ",
					modified_dt = (NOW())
				WHERE
					account_key = '" . $this->account_key . "'";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
		$result->close();

        // update the session display name
        $_SESSION["display_name"] = $display;

        // send an update email
        if (!empty($current->account_email) && !empty($body)) {
            $msg = "Hello " . $display . ",\r\n\r\n";
            $msg .= SITE_NAME . " has received a request to update your account ";
            $msg .= "information.\r\n\r\n" . $body;

            $this->_sendEmail($msg, $current->account_email);
        }

        return true;
    }
}