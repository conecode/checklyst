<?php
require_once 'clas.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class Templates
{
	/**
	 * property contains the database object
	 * @var object
	 */
	var $mysqli;

	/**
	 * property contains the account identifier
	 * @var integer
	 */
	var $account_id;

	/**
	 * property contains the template identifier
	 * @var integer
	 */
	var $template_id;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 *
	 * @param integer $account_key
	 */
	function __construct($account_key = '')
	{
		$this->mysqli = new Mysqli_Database();
		if ($account_key != '') {
			$sql = "SELECT
						account_id
					FROM
						accounts
					WHERE
						account_key = '{$account_key}'
					LIMIT 1";

			$result = $this->mysqli->query($sql);
			if (!$result) {
				catchErr("Your account access key is not valid.");
				$this->account_id = 0;
				return false;
			}
			if ($result->num_rows == 0) {
				catchErr("Your account access key is not valid.");
				$this->account_id = 0;
				return false;
			}
			$row = $result->fetch_assoc();
			$result->close();

			$this->account_id = $row['account_id'];
		}
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function setAccount($account_id)
	{
		$this->account_id = (int)$account_id;
	}

	function setTemplate($number)
	{
		$sql = "SELECT
					template_id
				FROM
					templates
				WHERE
					number = '{$number}'
				LIMIT 1";
				
		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		if ($result->num_rows == 0) {
			catchErr("The template number is not valid.");
			return false;
		}
		$row = $result->fetch_assoc();
		$result->close();
		$this->template_id = (int)$row['template_id'];
	}

	function setTemplateById($template_id)
	{
		$this->template_id = $template_id;
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 * 
	 * @param string $template_name
	 * @param string $number
	 * @return integer
	 * @access public
	 */
	function create($template_name, $number)
	{
		// create a new template in the database
		$sql = "INSERT INTO templates (
					template_name,
					owner_id,
					number,
					created_dt,
					modified_dt
				) values (
					?,
					?,
					?,
					NOW(),
					NOW()
				)";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('sis', $template_name, $this->account_id, $number);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		// get the new id to the template
		$this->template_id = mysqli_insert_id($this->mysqli);

		return $this->template_id;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record
	 *
	 * @return array
	 * @access public
	 */
	function get()
	{
		return $this->mysqli->row_get('templates', 'template_id', $this->template_id);
	}

	/**
	 * read multiple records
	 *
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function read($since_dt)
	{
		$sql = "SELECT
					template_id,
					template_name,
					owner_id,
					number,
					created_dt,
					modified_dt
				FROM
					templates
				WHERE
					owner_id = {$this->account_id}
				AND
					deleted = 0";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]['template_name'] = stripslashes($row['template_name']);
			$return[$i]['created_dt'] = strtotime($row['created_dt']);
			$return[$i]['modified_dt'] = strtotime($row['modified_dt']);
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * read multiple records that the user should not be reading
	 *
	 * @param array $numbers
	 * @return array
	 * @access public
	 */
	function readRemoved($numbers)
	{
		$number_list = implode("', '", $numbers);
		
		$sql = "SELECT
					p.number
				FROM
					templates p
				WHERE
					p.number IN ('{$number_list}')
				AND
					p.deleted = 1";
		
		$result = $this->mysqli->query($sql);
		if (!$result) {
			return false;
		}
		
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			++$i;
		}
		$result->close();
		
		return $return;
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing record
	 *
	 * @param string $template_name
	 * @return boolean
	 * @access public
	 */
	function update($template_name)
	{
		$sql = "UPDATE
					templates
				SET 
					template_name = ?,
					modified_dt = NOW()
				WHERE
					template_id = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
        }
		$stmt->bind_param('si', $template_name, $this->template_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * toggle on or off the deleted flag on a record
	 *
	 * @param boolean $toggle
	 * @return boolean
	 * @access public
	 */
	function flag($toggle = true)
	{
		if ($toggle) {
			$value = 1;
			$date  = 'NOW()';
		} else {
			$value = 0;
			$date  = 'NULL';
		}

		$sql = "UPDATE
					templates
				SET
					deleted = {$value},
					deleted_dt = {$date}
				WHERE
					template_id = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('i', $this->template_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}
}
