<?php
require_once('conf.checklyst.php');
require_once('clas.projects.php');
require_once('clas.people.php');
require_once('clas.invite.php');
require_once('clas.access.php');
require_once('func.handlers.php');
require_once('func.validate.php');

date_default_timezone_set("America/Los_Angeles");

$json = file_get_contents('php://input');
$data = json_decode($json, true);

global $ERRORS;
global $SUCCESSES;

/*
 * INVITE
 *  - add a invite to the database
 */
function action_invite($data)
{
	$account_key  = $_SERVER['HTTP_KEY'];
	$objPerson = new People($account_key);
	if ($objPerson->account_id == 0) {
		return array('result' => false);
	}

	$person_name = $data['name'];
	$person_email = $data['email'];
	$permissions = $data['permissions'];
	// add the project to the database
	$rslt = $objPerson->invite($person_name, $person_email, $permissions);
	if (!$rslt) {
		return array('result' => false);
	}

	// output true or false
	return array('result' => $rslt);
}

/*
 * SHARE
 *  - add user to project access
 */
function action_share($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objPerson = new People($account_key);
	if ($objPerson->account_id == 0) {
		return array('result' => false);
	}

	$person_id = $data['person_id'];
	$code = $data['code'];
	$project_id = $data['project_id'];
	$permission = $data['permission'];

	$objProjects = new Projects($account_key);
	$objProjects->setProjectById($project_id);
	if ($objProjects->permission != "ADMIN") {
		catchErr("You do not have permission to share this Lyst.");
		return array('result' => false);
	}
	
	if ($code != "0") {
		$objInvite = new Invite();
		if ($permission == "NONE") {
			$permission = 0;
		} else if ($permission == "READ") {
			$permission = 1;
		} else if ($permission == "WRITE") {
			$permission = 2;
		} else if ($permission == "ADMIN") {
			$permission = 3;
		}
		$rslt = $objInvite->addPermission($permission, $project_id, $code);
	} else {
		$objAccess = new Access();
		// add the project to the database
		if ($objAccess->checkAccess($project_id, $person_id)) {
			if ($permission == "NONE") {
				$rslt = $objAccess->remove($project_id, $person_id);
			} else {
				$rslt = $objAccess->update($permission, $project_id, $person_id);
			}
		} else {
			$rslt = $objAccess->create($permission, $project_id, $person_id);
		}
	}
	if (!$rslt) {
		return array('result' => false);
	}

	// output true or false
	return array('result' => $rslt);
}

/*
 * ACCEPT
 *  - accepting an invite
 */
function action_accept($data)
{
	$account_key  = $_SERVER['HTTP_KEY'];
	$objPerson = new People($account_key);
	if ($objPerson->account_id == 0) {
		return array('result' => false);
	}
	
	$code = $data['code'];
	$objInvite = new Invite();
	$person = $objPerson->get();
	
	if (validInput($code, 8, 8)) {
		$info = $objInvite->get($code);
	} else {
		catchErr('Error processing invitation code.');
	}
	
	$objAccess = new Access();

	// check that invitee and inviter are different
	if ($person['account_id'] == $info['created_by']) {
		catchErr('You cannot accept your own invitation.');
		return array('result' => false);
	} else {
		// check if this person is already friends with the inviter
		if (!$objAccess->checkLink($person['account_id'], $info['created_by'])) {
			$objAccess->createLink($person['account_id'], $info['created_by']);
		}
		// check if the inviter is already friends with this person
		if (!$objAccess->checkLink($info['created_by'], $person['account_id'])) {
			$objAccess->createLink($info['created_by'], $person['account_id']);
		}
	
		// get a list of my existing permissions
		$my_access = $objAccess->readAccess($person['account_id']);
	
		// explode the list of permissions being granted
		$arrAccess = explode(',', $info['permissions']);
	
		// apply granted permissions to the new account
		foreach ($arrAccess as $strAccess) {
			$project_id = (int)substr($strAccess, 0, -2);
			$permission = getValue($project_id, '', $my_access);
			switch ((int)substr($strAccess, -1)) {
				case 1:
					if ($permission == '') {
						$objAccess->create('READ', $project_id, $person['account_id']);
					}
					break;
				case 2:
					if ($permission == '') {
						$objAccess->create('WRITE', $project_id, $person['account_id']);
					} elseif ($permission == 'READ') {
						$objAccess->update('WRITE', $project_id, $person['account_id']);
					}
					break;
				case 3:
					if ($permission == '') {
						$objAccess->create('ADMIN', $project_id, $person['account_id']);
					} else {
						$objAccess->update('ADMIN', $project_id, $person['account_id']);
					}
					break;
			}
		}
	
		// remove the used invite code
		$rslt = $objInvite->remove($info['code']);
		
		if ($rslt) {
			return array('success' => $rslt);
		} else {
			catchErr("Could not accept invite.");
			return array('result' => false);
		}
	}
}

/*
 * CREATE
 *  - add person link
 */
function action_create($data)
{
	$account_key  = $_SERVER['HTTP_KEY'];
	$objPerson = new People($account_key);
	if ($objPerson->account_id == 0) {
		return array('result' => false);
	}

	$person_id = $data['person_id'];
	$code = $data['code'];
	// add person link
	$rslt = $objPerson->create($peson_id,$code);
	if (!$rslt) {
		return array('result' => false);
	}

	// output true or false
	return array('result' => $rslt);
}

/*
 * READ
 *  - get the people attached to account key
 */
function action_read()
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objPerson = new People($account_key);
	if ($objPerson->account_id == 0) {
		return array('result' => false);
	}

	// get a list of my projects
	$rslt = $objPerson->read();
	if (!$rslt) {
		$rslt = array();
	}

	// get a list of my projects
	$rslt2 = $objPerson->readInvites();
	if (!$rslt2) {
		$rslt2 = array();
	}

	// output the data
	return array('result' => $rslt,'invites' => $rslt2);
}

/*
 * PERMISSIONS
 *  - get the people attached to account key with permissions
 */
function action_readPermissions($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objPerson = new People($account_key);
	if ($objPerson->account_id == 0) {
		return array('result' => false);
	}

	$project_id = $data['project_id'];
	// get a list of my projects
	$rslt = $objPerson->readPermissions($project_id);
	if (!$rslt) {
		$rslt = array();
	}

	// get a list of my projects
	$rslt2 = $objPerson->readInvites($project_id);
	if (!$rslt2) {
		$rslt2 = array();
	}

	// output the data
	return array('result' => $rslt,'invites' => $rslt2);
}

/*
 * DELETE
 *  - delete a specified project
 */
function action_delete($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objPerson = new People($account_key);
	if ($objPerson->account_id == 0) {
		return array('result' => false);
	}
	
	$person_id = $data['person_id'];
	$code = $data['code'];
	$objInvite = new Invite();
	if ($code == "0") {
		$rslt = $objPerson->delete(intval($person_id));
	} else {
		$rslt = $objInvite->remove($code);
	}
	if (!$rslt) {
		catchErr("Could not delete person.");
		return array('result' => false);
	}
	
	return array('result' => true);
}

$action = strtoupper(getValue('action'));
if ($action == 'CREATE') {
	$arrOutput = action_create($data);
} else if ($action == 'READ') {
	$arrOutput = action_read($data);
} else if ($action == 'DELETE') {
	$arrOutput = action_delete($data);
} else if ($action == 'INVITE') {
	$arrOutput = action_invite($data);
} else if ($action == 'ACCEPT') {
	$arrOutput = action_accept($data);
} else if ($action == 'PERMISSIONS') {
	$arrOutput = action_readPermissions($data);
} else if ($action == 'SHARE') {
	$arrOutput = action_share($data);
}

header ("Content-Type:application/json");

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= (strlen($msg) > 0) ? "\n" . $value : $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else if (count($SUCCESSES)) {
	$msg = '';
	foreach ($SUCCESSES as &$value) {
		$msg .= (strlen($msg) > 0) ? "\n" . $value : $value;
	}
	$arrSuccesses = array("successes" => $msg);
	echo json_encode($arrSuccesses);
} else {
	echo str_replace("\\/", "/", json_encode($arrOutput));
}
