<?php
/**
 * verify a number is valid
 *
 * @param integer $value
 * @param integer $min
 * @param integer $max
 * @return boolean
 */
function validNumber($value, $min = 0, $max = 1)
{
	return preg_match("/^\\d{".$min.",".$max."}$/", $value);
}

// ------------------------------------------------------------------------

/**
 * verify an inupt string is valid
 *
 * @param string $value
 * @param integer $min
 * @param integer $max
 * @return boolean
 */
function validInput($value, $min, $max)
{
	if (preg_match("/^(.){".$min.",".$max."}$/", $value)) {
		if (strpos($value, '<') OR strpos($value, '>')) {
			return false;
		}
	} else {
		return false;
	}

	return true;
}

// ------------------------------------------------------------------------

/**
 * verify an email address is valid
 *
 * @param string $value
 * @return boolean
 */
function validEmail($value)
{
	return preg_match("/[a-z0-9|_|-|\.]+\@[a-z0-9|\.|-]+\.[a-z]{2,3}$/i", $value);
}

// ------------------------------------------------------------------------

/**
 * verify a username is valid
 *
 * @param string $value
 * @return boolean
 */
function validUsername($value)
{
	return preg_match("/^[a-z0-9|_|-]{1,50}$/i", $value);
}

// ------------------------------------------------------------------------

/**
 * verify a password is valid
 *
 * @param string $value
 * @return boolean
 */
function validPassword($value)
{
	return preg_match("/^[a-z0-9|_|-]{6,32}$/i", $value);
}

// ------------------------------------------------------------------------

/**
 * verify a url is valid
 *
 * @param string $value
 * @return boolean
 */
function validUrl($value)
{
	return preg_match("/^http(s?):\/\/[a-z0-9|\.|-]+\.+[a-z]{2,3}/i", $value);
}

// ------------------------------------------------------------------------

/**
 * verfiy a file upload is valid
 *
 * @param string $value
 * @return boolean
 */
function validFile($value)
{
	$filemax = 1500000;
	$filetypes = array("jpg", "gif", "png");
	$extension = strtolower(array_pop(explode(".", $_FILES[$value]["name"])));

	if (!in_array($extension, $filetypes)) {
		catchErr("File type is invalid. Acceptable types are: JPG, GIF, or PNG");
		return false;
	} else if ($_FILES[$value]["name"] > $filemax) {
		catchErr("The File is too large, it must be " . $filemax . " bytes or smaller in size");
		return false;
	}

	return true;
}

// ------------------------------------------------------------------------

/**
 * verify a date is valid
 *
 * @param string $value
 * @return boolean
 */
function validDate($value)
{
	if (!preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", trim($value))) {
		catchErr("Enter a valid date format");
		return false;
	} else {
		$aDate = explode("/", $value);
		
		if (!checkdate($aDate[0], $aDate[1], $aDate[2])) {
			return false;
		}
	}

	return true;
}

// ------------------------------------------------------------------------

/**
 * check the submitted form values used to change a password
 *
 * @param array $post
 * @return boolean
 */
function validPasswordForm($post)
{
	if (!validPassword($post["password"])) {
		catchErr("You entered an invalid password.");
		return false;
	}
	if (strcmp($post["password"], $post["confirm"])) {
		catchErr("The password and confirmation you entered did not match.");
		return false;
	}
	return true;
}

// ------------------------------------------------------------------------

/**
 * check the submitted form values used to edit an account
 *
 * @param array $post
 * @return boolean
 */
function validAccountForm($post)
{
	if (!validUsername($post["username"])) {
		catchErr("You entered an invalid username.");
		return false;
	}
	if (!validEmail($post["email"])) {
		catchErr("You entered an invalid email address.");
		return false;
	}
	if (!validInput($post["your_name"], 0, 50)) {
		catchErr("You entered an invalid display name.");
		return false;
	}
	return true;
}

// ------------------------------------------------------------------------

/**
 * check the submitted form values used to register an account
 *
 * @param array $post
 * @return boolean
 */
function validRegisterForm($post)
{
	if (!validUsername($post["username"])) {
		catchErr("You entered an invalid username.");
		return false;
	}
	if (!validPassword($post["password"])) {
		catchErr("You entered an invalid password.");
		return false;
	}
	if (!validInput($post["name"], 0, 50)) {
		catchErr("You entered an invalid display name.");
		return false;
	}
	return true;
}

// ------------------------------------------------------------------------

/**
 * check the submitted form values used to login
 *
 * @param string $username
 * @param string $password
 * @return boolean
 */
function validLoginForm($username, $password)
{
	if (empty($username)) {
		catchErr("You did not enter a username.");
		return false;
	}
	if (empty($password)) {
		catchErr("You did not enter a password.");
		return false;
	}
	if (!validUsername($username)) {
		catchErr("You entered an invalid username.");
		return false;
	}
	if (!validPassword($password)) {
		catchErr("You entered an invalid password.");
		return false;
	}
	return true;
}

?>