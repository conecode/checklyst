<?php
require_once 'clas.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class Invite
{
	/**
	 * property contains the database object
	 * @private object
	 */
	private $mysqli;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function __construct()
	{
		$this->mysqli = new Mysqli_Database();
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 *
	 * @param string $name
	 * @param string $email
	 * @param string $code
	 * @param string $permissions
	 * @return integer
	 * @access public
	 */
	function create($name, $email, $code, $permissions, $account_id)
	{
		$sql = "INSERT INTO invites (
					name,
					email,
					`code`,
					permissions,
					created_by,
					invite_dt
				) values (
					?,
					?,
					?,
					?,
					?,
					NOW()
				)";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('ssssi', $name, $email, $code, $permissions, $account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		// get the new id
		$code = mysqli_insert_id($this->mysqli);

		return $code;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record by code
	 *
	 * @param string $code
	 * @return array
	 * @access public
	 */
	function get($code)
	{
		$this->mysqli->escape($code);
		return $this->mysqli->row_get('invites', '`code`', "'$code'");
	}

	/**
	 * get a single record by id
	 *
	 * @param integer $invite_id
	 * @return array
	 * @access public
	 */
	function getById($invite_id)
	{
		return $this->mysqli->row_get('invites', 'invite_id', (int)$invite_id);
	}

	/**
	 * read multiple records created by a specified account
	 *
	 * @param integer $account_id
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function read($account_id)
	{
		$sql = "SELECT
					invite_id,
					name,
					email,
					`code`,
					permissions,
					invite_dt
				FROM
					invites
				WHERE
					created_by = " . (int)$account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]["invite_dt"] = strtotime($row["invite_dt"]);
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * count the total number of records
	 *
	 * @return integer
	 * @access public
	 */
	function total()
	{
		return $this->mysqli->row_count('invites', 'invite_id');
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing record
	 *
	 * @param integer $code
	 * @param string $permissions
	 * @return boolean
	 * @access public
	 */
	function update($code, $permissions)
	{
		$sql = "UPDATE
					invites
				SET
					permissions = ?
				WHERE
					`code` = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
        }
		$stmt->bind_param('ss', $permissions, $code);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * update an existing record
	 *
	 * @param string $permission
	 * @param integer $project_id
	 * @param string $code
	 * @return boolean
	 * @access public
	 */
	function addPermission($permission, $project_id, $code)
	{
		$sql = "SELECT
				permissions
			FROM
				invites
			WHERE
				`code` = '$code'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			if (strlen($row['permissions']) > 0) {
				$permissions = explode(",",$row['permissions']);
				$x = 0;
				$str_permission = "";
				$proj_found = 0;
				foreach($permissions as $apermission) {
					if ($x > 0) {
						$str_permission .= ",";
					}
					$values = explode(":",$apermission);
					if ($value[0] == $project_id) {
						$proj_found = 1;
						$str_permission .= $project_id.":".$permission;
					} else {
						$str_permission .= $value[0].":".$value[1];
					}
					$x++;
				}
				if (!$proj_found) {
					$str_permission .= ",".$project_id.":".$permission;
				}
			} else {
				$str_permission .= $project_id.":".$permission;
			}
			$permissions = $str_permission;
			
			$sql = "UPDATE
						invites
					SET
						permissions = ?
					WHERE
						`code` = ?";
	
			if (!$stmt = $this->mysqli->prepare($sql)) {
				catchSQL($sql, $this->mysqli);
				return false;
	        }
			$stmt->bind_param('ss', $permissions, $code);
			if (!$stmt->execute()) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			$stmt->close();
	
			return true;
		} else {
			catchErr("Invite no longer exists");
		}
		$result->close();
	}

	// DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * permanently removes a record
	 *
	 * @param integer $code
	 * @return boolean
	 * @access public
	 */
	function remove($code)
	{
		return $this->mysqli->row_delete('invites', '`code`', $code);
	}
}