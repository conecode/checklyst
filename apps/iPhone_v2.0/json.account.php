<?php
require_once('conf.checklyst.php');
require_once('clas.account.php');
require_once('func.validate.php');
require_once('func.handlers.php');

date_default_timezone_set("America/Los_Angeles");

$json = file_get_contents('php://input');
$data = json_decode($json, true);

/*
 * CREATE
 *  - add an account to the database if it does not already exist
 */
function action_create($data)
{
	if(!validRegisterForm($data)) {
		return array('result' => false);
    }
    
	$args['account_username'] = $data['username'];
	$args['account_password'] = $data['password'];
	$args['account_email']    = $data['email'];
	$args['display_name']     = $data['username'];

	$account = new Account();

	if (!$rslt = $account->create($args)) {
		return array('result' => false);
	}
	
	// output the account data
	return array('result' => $rslt);
}

/*
 * LOGIN
 *  - verify the account credentials and return account data
 */
function action_login($data)
{
	$username = $data['username'];
	$password = $data['password'];

	$account = new Account();

	if (!validLoginForm($username, $password)) {
		return array('result' => false);
	}

	if (!$rslt = $account->login($username, $password)) {
		return array('result' => false);
	}
/* 	echo '{"errors":"There is an issue with logging in. It will be fixed soon. Sorry for the inconvenience."}'; */

	// output the data
	return array('result' => $rslt);
}

/*
 * UPDATE
 *  - update the account data
 */
function action_update($data)
{
	$key = $_SERVER['HTTP_KEY'];
	$account = new Account($key);
	if ($account->account_id == 0) {
		return array('result' => false);
	}

	$args["account_username"] = $data['username'];
	$args["account_password"] = $data['password'];
	$args["account_email"]    = $data['email'];
	$args["display_name"]     = $data['name'];
	
	if (!$account->update($args)) {
		return array('result' => false);
	}

	// output the data
	return array('result' => true);
}

$action = strtoupper(getValue('action'));
if ($action == 'CREATE') {
	$arrOutput = action_create($data);
} else if ($action == 'LOGIN') {
	$arrOutput = action_login($data);
} else if ($action == 'UPDATE') {
	$arrOutput = action_update($data);
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= (strlen($msg) > 0) ? "\n" . $value : $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	echo str_replace("\\/", "/", json_encode($arrOutput));
}
