<?php
require_once 'clas.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class People {
    /**
     * property contains unique account identifier
     * @var integer
     */
    var $account_key;
    
	/**
	 * property contains the database object
	 * @var object
	 */
	var $mysqli;

	/**
	 * property contains the account identifier
	 * @var integer
	 */
	var $account_id;

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 *
	 * @param integer $account_key
	 */
	function __construct($account_key = '')
	{
		$this->mysqli = new Mysqli_Database();
		if ($account_key == '') {
			$account_key = "xxxx";
		}
		$sql = "SELECT
					account_id
				FROM
					accounts
				WHERE
					account_key = '{$account_key}'
				LIMIT 1";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchErr("Database Error");
			$this->account_id = 0;
			return false;
		}
		if ($result->num_rows == 0) {
			catchErr("Your account access key is not valid.");
			$this->account_id = 0;
			return false;
		}
		$row = $result->fetch_assoc();
		$result->close();

		$this->account_id = $row['account_id'];
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record by code
	 *
	 * @param string $code
	 * @return array
	 * @access public
	 */
	function get()
	{
		return $this->mysqli->row_get('accounts', 'account_id', $this->account_id);
	}

	function setPerson($person_id)
	{
		$this->person_id = (int)$person_id;
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 * 
	 * @param string $project_name
	 * @param string $number
	 * @return integer
	 * @access public
	 */
	function invite($person_name, $person_email, $permissions)
	{
		$sql = "SELECT
				*
			FROM
				invites
			WHERE
				created_by = ".$this->account_id."
			AND
				email = '{$person_email}'";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		
		$addnew = 1;
		if ($result->num_rows > 0) {
			catchErr("You already invited this person. Sending email again.");
			$addnew = 0;
			$row = $result->fetch_assoc();
			$code = $row['code'];
		}
		$result->close();

		$sql = "SELECT
				display_name
			FROM
				accounts
			WHERE
				account_id = ".$this->account_id;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$row = $result->fetch_assoc();
		$result->close();

		$inviter = $row['display_name'];
		
		if ($addnew) {
			$code = substr(md5(uniqid()), 4, 8);
			$sql = "INSERT INTO invites (
						name,
						email,
						code,
						permissions,
						created_by,
						invite_dt
					) values (
						?,
						?,
						?,
						?,
						?,
						NOW()
					)";
	
			if (!$stmt = $this->mysqli->prepare($sql)) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			$stmt->bind_param('ssssi', $person_name, $person_email, $code, $permissions, $this->account_id);
			if (!$stmt->execute()) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			$stmt->close();
		}
		
		$message = "DO NOT REPLY TO THIS EMAIL\n\n";
		$message .= "Dear $person_name,\n\n";
		$message .= "$inviter would like to give you access to some or all of their lysts.\n\n";
		$message .= "Click the link below to accept:\n";
		$message .= "http://checklyst.com/invite/code/".$code."\n\n";
		$message .= "If you have the Checklyst iPhone app, tap the link below:\n";
		$message .= "checklyst://invite?code=".$code."\n\n";
		
		$header = "To: $person_name <$person_email>\r\n";
		$header .= "From: Checklyst <no-reply@checklyst.com>\r\n";
		$header .= "Reply-To: no-reply@checklyst.com\r\n";
		$header .= "X-Mailer: PHP/" . phpversion() . "\r\n";

		$subject = "Checklyst Invite";

		// try to send mail
		$send = mail($person_email, $subject, $message, $header);

		// get the new id
		$code = mysqli_insert_id($this->mysqli);
		
		catchSucc("Person Invited!");

		return $code;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * read multiple records created by a specified account
	 *
	 * @param integer $account_id
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function read()
	{
		$sql = "SELECT
					pl.partner_id,
					a.display_name,
					a.account_email
				FROM people_link pl
				INNER JOIN
					accounts a
				ON
					pl.partner_id = a.account_id
				WHERE
					pl.account_id = ".$this->account_id;
		
		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]['person_id'] = $row['partner_id'];
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * read multiple records created by a specified account
	 *
	 * @param integer $account_id
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function readInvites($project_id = 0)
	{
		$sql = "SELECT
					name,
					code,
					permissions
				FROM
					invites
				WHERE
					created_by = ".$this->account_id;
		
		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
		
			$proj_found = 0;
			if (strlen($row['permissions']) > 0) {
				$permissions = explode(",",$row['permissions']);
				foreach($permissions as $permission) {
					$values = explode(":",$permission);
					if ($values[0] == $project_id) {
						$proj_found = 1;
						if ($values[1] == "0") {
							$thepermission = "NONE";
						} else if ($values[1] == "1") {
							$thepermission = "READ";
						} else if ($values[1] == "2") {
							$thepermission = "WRITE";
						} else if ($values[1] == "3") {
							$thepermission = "ADMIN";
						} else {
							$thepermission = "NONE";
						}
					}
				}
			}
			if (!$proj_found) {
				$thepermission = "NONE";
			}
			$return[$i]['permission'] = $thepermission;
			
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * read multiple records created by a specified account
	 *
	 * @param integer $project_id
	 * @return array
	 * @access public
	 */
	function readPermissions($project_id)
	{
		$sql = "SELECT
					pl.partner_id,
					a.display_name,
					a.account_email,
					IFNULL(pa.permission,'NONE') AS permission,
					p.owner_id
				FROM
					people_link pl
				INNER JOIN
					accounts a
				ON
					pl.partner_id = a.account_id
				LEFT JOIN
					project_access pa
				ON
					pa.account_id = pl.partner_id AND pa.project_id = $project_id
				INNER JOIN
					projects p
				ON
					p.project_id = $project_id
				WHERE
					pl.account_id = ".$this->account_id;
		
		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]['person_id'] = $row['partner_id'];
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * toggle on or off the deleted flag on a record
	 *
	 * @param boolean $toggle
	 * @return boolean
	 * @access public
	 */
	function delete($person_id)
	{
		$sql = "DELETE FROM
					people_link
				WHERE
					(account_id = ?
				AND
					partner_id = ?)
				OR
					(account_id = ?
				AND
					partner_id = ?)";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('iiii', $this->account_id, $person_id, $person_id, $this->account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();
		
		$sql = "SELECT
					project_id
				FROM
					projects
				WHERE
					deleted = 0
				AND
					owner_id = ".$this->account_id;
		
		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		while ($row = $result->fetch_assoc()) {
			$sql = "DELETE FROM
						project_access
					WHERE
						project_id = ?
					AND
						account_id = ?";
	
			if (!$stmt = $this->mysqli->prepare($sql)) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			
			$stmt->bind_param('ii', $row['project_id'], $person_id);
			if (!$stmt->execute()) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			$stmt->close();
		}
		$result->close();
		
		$sql = "SELECT
					project_id
				FROM
					projects
				WHERE
					deleted = 0
				AND
					owner_id = ".$person_id;
		
		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}

		while ($row = $result->fetch_assoc()) {
			$sql = "DELETE FROM
						project_access
					WHERE
						project_id = ?
					AND
						account_id = ?";
	
			if (!$stmt = $this->mysqli->prepare($sql)) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			
			$stmt->bind_param('ii', $row['project_id'], $this->account_id);
			if (!$stmt->execute()) {
				catchSQL($sql, $this->mysqli);
				return false;
			}
			$stmt->close();
		}
		$result->close();

		return true;
	}
}
