<?php
require_once('conf.checklyst.php');
require_once('clas.templates.php');
require_once('func.handlers.php');

date_default_timezone_set("America/Los_Angeles");

$json = file_get_contents('php://input');
$data = json_decode($json, true);

/*
 * CREATE
 *  - add a template to the database
 */
function action_create($data)
{
	$account_key  = $_SERVER['HTTP_KEY'];
	$template_name = $data['name'];
	$number       = $data['number'];
	$objTemplate   = new Templates($account_key);

	// add the template to the database
	$rslt = $objTemplate->create($template_name, $number);
	if (!$rslt) {
		return array('result' => false);
	}

	// output the template id
	return array('result' => $rslt);
}

/*
 * READ
 *  - get the templates that match a since date and account key
 */
function action_read($data)
{
	$account_key = $_SERVER['HTTP_KEY'];;
	$since_dt    = $data['since'];
	$objTemplate  = new Templates($account_key);

	$remove = $objTemplate->readRemoved($data['templates']);
	if (!$remove) {
		$remove = array();
	}
	
	// get a list of my templates
	$rslt = $objTemplate->read($since_dt);
	if (!$rslt) {
		$rslt = array();
	}

	// output the data
	return array('result' => $rslt, 'remove' => $remove);
}

/*
 * UPDATE
 *  - update a templates name
 */
function action_update($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objTemplate  = new Templates($account_key);
	$objTemplate->setTemplate($data['number']);
	$template_name = $data['name'];
	
	$rslt = $objTemplate->update($template_name);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

/*
 * DELETE
 *  - delete a specified template
 */
function action_delete($data)
{
	$account_key = $_SERVER['HTTP_KEY'];
	$objTemplate  = new Templates($account_key);
	$objTemplate->setTemplate($data['number']);

	$rslt = $objTemplate->flag(true);
	if (!$rslt) {
		return array('result' => false);
	}

	return array('result' => $rslt);
}

$action = strtoupper(getValue('action'));
if ($action == 'CREATE') {
	$arrOutput = action_create($data);
} else if ($action == 'READ') {
	$arrOutput = action_read($data);
} else if ($action == 'UPDATE') {
	$arrOutput = action_update($data);
} else if ($action == 'DELETE') {
	$arrOutput = action_delete($data);
}

header ("Content-Type:application/json");
global $ERRORS;

if (count($ERRORS)) {
	$msg = '';
	foreach ($ERRORS as &$value) {
		$msg .= (strlen($msg) > 0) ? "\n" . $value : $value;
	}
	$arrErrors = array("errors" => $msg);
	echo json_encode($arrErrors);
} else {
	echo str_replace("\\/", "/", json_encode($arrOutput));
}
