<?php
require_once 'clas.mysqli_database.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class Projects
{
	/**
	 * property contains the database object
	 * @var object
	 */
	var $mysqli;

	/**
	 * property contains the account identifier
	 * @var integer
	 */
	var $account_id;

	/**
	 * property contains the project identifier
	 * @var integer
	 */
	var $project_id;

	/**
	 * property contains the users permission for the specified project
	 * @var string
	 */
	var $permission = 'NONE';

	// CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 *
	 * @param integer $account_key
	 */
	function __construct($account_key = '')
	{
		$this->mysqli = new Mysqli_Database();
		if ($account_key != '') {
			$sql = "SELECT
						account_id
					FROM
						accounts
					WHERE
						account_key = '{$account_key}'
					LIMIT 1";

			$result = $this->mysqli->query($sql);
			if (!$result) {
				catchErr("Your account access key is not valid.");
				$this->account_id = 0;
				return false;
			}
			if ($result->num_rows == 0) {
				catchErr("Your account access key is not valid.");
				$this->account_id = 0;
				return false;
			}
			$row = $result->fetch_assoc();
			$result->close();

			$this->account_id = $row['account_id'];
		}
	}

	// PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * verify an account has permission to access a project
	 *
	 * @return boolean
	 */
	private function _verifyPermission()
	{
		// set the default
		$this->permission = 'NONE';

		// query the database
		$sql = "SELECT
					permission
				FROM
					project_access
				WHERE
					project_id = ?
				AND
					account_id = ?
				LIMIT 1";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param("ii", $this->project_id, $this->account_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->store_result();

		// check if any permission results were returned
		if ($stmt->num_rows == 0) {
			return false;
		}

		$access = false;
		$stmt->bind_result($access);
		$stmt->fetch();
		$stmt->close();

		// check if the permission value exists
		if (empty($access)) {
			return false;
		}

		// set the permission
		$this->permission = $access;

		return $access;
	}

	// PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function setAccount($account_id)
	{
		$this->account_id = (int)$account_id;
		if (!empty($this->project_id)) {
			$this->_verifyPermission();
		} else {
			$this->permission = 'NONE';
		}
	}

	function setProject($number)
	{
		$sql = "SELECT
					project_id
				FROM
					projects
				WHERE
					number = '{$number}'
				LIMIT 1";

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		if ($result->num_rows == 0) {
			catchErr("The project number is not valid.");
			return false;
		}
		$row = $result->fetch_assoc();
		$result->close();
		$this->project_id = (int)$row['project_id'];
		$this->_verifyPermission();
	}

	function setProjectById($project_id)
	{
		$this->project_id = $project_id;
		$this->_verifyPermission();
	}

	// CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * create a new record
	 * 
	 * @param string $project_name
	 * @param string $number
	 * @return integer
	 * @access public
	 */
	function create($project_name, $number)
	{
		// create a new project in the database
		$sql = "INSERT INTO projects (
					project_name,
					owner_id,
					number,
					created_dt,
					modified_dt
				) values (
					?,
					?,
					?,
					NOW(),
					NOW()
				)";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('sis', $project_name, $this->account_id, $number);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		// get the new id and set permission to ADMIN to the project
		$this->project_id = mysqli_insert_id($this->mysqli);
		$this->addPermission('ADMIN');

		return $this->project_id;
	}

	/**
	 * add permission for an account to access a project
	 *
	 * @param string $permission [READ, WRITE, ADMIN]
	 * @return boolean
	 * @access public
	 */
	function addPermission($permission, $project_id = 0)
	{
		if (empty($project_id)) {
			$project_id = $this->project_id;
		}

		$sql = "INSERT INTO project_access (
					project_id,
					account_id,
					permission
				) values (
					?,
					?,
					?
				)";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('iis', $project_id, $this->account_id, $permission);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		$this->permission = $permission;
		return true;
	}

	// READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * get a single record
	 *
	 * @return array
	 * @access public
	 */
	function get()
	{
		return $this->mysqli->row_get('projects', 'project_id', $this->project_id);
	}

	/**
	 * read multiple records
	 *
	 * @param string $since_dt
	 * @return array
	 * @access public
	 */
	function read($since_dt)
	{
		$sql = "SELECT
					permission,
					project_id,
					project_name,
					owner_id,
					number,
					created_dt,
					modified_dt
				FROM
					projects
				LEFT JOIN
					project_access USING (project_id)
				WHERE
					account_id = {$this->account_id}
				AND
					permission IS NOT NULL
				AND
					deleted = 0";
				/*
AND
					UNIX_TIMESTAMP(modified_dt) > " . $since_dt;
*/

		$result = $this->mysqli->query($sql);
		if (!$result) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		
		$access = array(0 => 'READ', 1 => 'WRITE', 2 => 'ADMIN');

		// loop result and build return array
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]['permission'] = array_search($row['permission'], $access);
			$return[$i]['project_name'] = stripslashes($row['project_name']);
			$return[$i]['created_dt'] = strtotime($row['created_dt']);
			$return[$i]['modified_dt'] = strtotime($row['modified_dt']);
			++$i;
		}
		$result->close();

		return $return;
	}

	/**
	 * read multiple records that the user should not be reading
	 *
	 * @param array $numbers
	 * @return array
	 * @access public
	 */
	function readRemoved($numbers)
	{
		$number_list = implode("', '", $numbers);
		
		$sql = "SELECT
					p.number
				FROM
					projects p
				LEFT JOIN
					project_access a ON p.project_id = a.project_id
				AND
					a.account_id = {$this->account_id}
				WHERE
					p.number IN ('{$number_list}')
				AND
					(a.permission IS NULL
					OR p.deleted = 1)";
		
		$result = $this->mysqli->query($sql);
		if (!$result) {
			return false;
		}
		
		$return = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			++$i;
		}
		$result->close();
		
		return $return;
	}

	// UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/**
	 * update an existing record
	 *
	 * @param string $project_name
	 * @return boolean
	 * @access public
	 */
	function update($project_name)
	{
		// verify permission to update a project name
		if ($this->permission != 'ADMIN') {
			catchErr("Access to modify this project is denied.");
			return false;
		}

		$sql = "UPDATE
					projects
				SET 
					project_name = ?,
					modified_dt = NOW()
				WHERE
					project_id = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
        }
		$stmt->bind_param('si', $project_name, $this->project_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}

	/**
	 * toggle on or off the deleted flag on a record
	 *
	 * @param boolean $toggle
	 * @return boolean
	 * @access public
	 */
	function flag($toggle = true)
	{
		// verify permission to update a project name
		if ($this->permission != 'ADMIN') {
			catchErr("Access to modify this project is denied.");
			return false;
		}

		if ($toggle) {
			$value = 1;
			$date  = 'NOW()';
		} else {
			$value = 0;
			$date  = 'NULL';
		}

		$sql = "UPDATE
					projects
				SET
					deleted = {$value},
					deleted_dt = {$date}
				WHERE
					project_id = ?";

		if (!$stmt = $this->mysqli->prepare($sql)) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->bind_param('i', $this->project_id);
		if (!$stmt->execute()) {
			catchSQL($sql, $this->mysqli);
			return false;
		}
		$stmt->close();

		return true;
	}
}
