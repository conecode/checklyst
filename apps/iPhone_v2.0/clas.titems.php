<?php
require_once 'clas.templates.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class TItems extends Templates
{
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     *
     * @param integer $account_key
     * @param integer $template_id
     */
    function __construct($account_key = '', $template_id = 0) {
        parent::__construct($account_key);

        if ($template_id != 0) {
            $this->setTemplate($template_id);
        }
    }

    // CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * create a new list record
     *
     * @param array $args
     * @return integer
     * @access public
     */
    function createTItem($args) {
        $sql = "INSERT INTO template_items (
					template_id,
					titem_text,
					created_dt,
					modified_dt
				) VALUES (
					?, ?, ?, ?
				)";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('isss', $this->template_id, $args['titem_text'],
				$args['created_dt'], $args['modified_dt']);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        // get the new id
        $titem_id = mysqli_insert_id($this->mysqli);

        return $titem_id;
    }

    // READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * get a single record
     *
     * @param integer $titem_id
     * @return array
     * @access public
     */
    function getTItem($titem_id) {
        return $this->mysqli->row_get('template_items', 'titem_id', (int) $titem_id);
    }

    /**
     * read multiple records
     *
	 * @param string $since_dt
     * @return array
     * @access public
     */
    function readTItem($since_dt = '') {
        if ($since_dt != '') {
			$where = "AND UNIX_TIMESTAMP(modified_dt) > " . $since_dt;
		} else {
			$where = '';
		}

        // query database
        $sql = "SELECT
					titem_id,
					template_id,
					titem_text,
					created_dt,
					modified_dt
				FROM
					template_items
				WHERE
					template_id = {$this->template_id}
					{$where}";
					
/* 		catchErr(preg_replace("/\s+/", " ", $sql)); */
        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $return = array();
		$i = 0;
        while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]['titem_id']       = (int)$row['titem_id'];
			$return[$i]['template_id']    = (int)$row['template_id'];
			$return[$i]['titem_text']     = strip_tags(htmlspecialchars_decode(stripslashes($row['titem_text']), ENT_QUOTES));
			$return[$i]['created_dt']    = strtotime($row['created_dt']);
			$return[$i]['modified_dt']   = strtotime($row['modified_dt']);
            ++$i;
        }
        $result->close();

        return $return;
    }

	/**
     * read multiple records that the user should not be reading
     *
     * @param string $order order by field
     * @return array
     * @access public
     */
	function readRemovedTItems($titems) {
		if (count($titems) == 0) {
			return false;
		}

		// query database
        $sql = "SELECT
					titem_id
				FROM
					template_items
				WHERE
					template_id = {$this->template_id}";

		$result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

		$all_titems = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			array_push($all_titems, (int)$row['titem_id']);
            ++$i;
        }
		$result->close();

		$remove = array();
		foreach ($titems as $id) {
			if (!in_array($id, $all_titems, true) && $id != 0) {
				array_push($remove, $id);
			}
		}

        return $remove;
	}

    // UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * update an existing record
     *
     * @param integer $titem_id
     * @param array $args
     * @return boolean
     * @access public
     */
    function updateTItem($titem_id, $args) {
        $sql = "UPDATE
					template_items
				SET
					template_id = ?,
					titem_text = ?,
					created_dt = ?,
					modified_dt = ?
				WHERE
					titem_id = ?";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('isssi', $this->template_id, $args['titem_text'],
				$args['created_dt'], $args['modified_dt'],
				$titem_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        return true;
    }

    // DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * permanently removes a record
     *
     * @param integer $titem_id
     * @return boolean
     * @access public
     */
    function removeTItem($titem_id) {
        return $this->mysqli->row_delete('template_items', 'titem_id', (int) $titem_id);
    }

    /**
     * permanently removes a record
     *
     * @param integer $titem_id
     * @return boolean
     * @access public
     */
    function removeAll($template_id) {
        return $this->mysqli->row_delete('template_items', 'template_id', (int) $template_id);
    }

}