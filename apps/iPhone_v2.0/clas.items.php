<?php
require_once 'clas.projects.php';

/**
 * @author Cone Code Development
 * @version 0.1
 */
class Items extends Projects
{
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     *
     * @param integer $account_key
     * @param integer $project_id
     */
    function __construct($account_key = '', $project_id = 0) {
        parent::__construct($account_key);

        if ($project_id != 0) {
            $this->setProject($project_id);
        }
    }

    // CREATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * create a new list record
     *
     * @param array $args
     * @return integer
     * @access public
     */
    function createItem($args) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        $sql = "INSERT INTO project_items (
					project_id,
					item_text,
					item_priority,
					assigned_to,
					created_by,
					strike,
					strike_dt,
					due_dt,
					created_dt,
					modified_dt
				) VALUES (
					?, ?, ?, ?, ?,
					?, ?, ?, ?, ?
				)";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('isiiiissss', $this->project_id, $args['item_text'],
				$args['item_priority'], $this->account_id, $this->account_id,
				$args['strike'], $args['strike_dt'], $args['due_dt'],
				$args['created_dt'], $args['modified_dt']);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        // get the new id
        $item_id = mysqli_insert_id($this->mysqli);

        return $item_id;
    }

    // READ METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * get a single record
     *
     * @param integer $item_id
     * @return array
     * @access public
     */
    function getItem($item_id) {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }

        return $this->mysqli->row_get('project_items', 'item_id', (int) $item_id);
    }

    /**
     * read multiple records
     *
	 * @param string $since_dt
     * @return array
     * @access public
     */
    function readItem($since_dt = '') {
        // check permission
        if ($this->permission == 'NONE') {
            catchErr("Access to this project is denied.");
            return false;
        }

		if ($since_dt != '') {
			$where = "AND UNIX_TIMESTAMP(modified_dt) > " . $since_dt;
		} else {
			$where = '';
		}

        // query database
        $sql = "SELECT
					item_id,
					project_id,
					item_text,
					item_priority,
					created_by,
					modified_by,
					assigned_to,
					due_dt,
					strike,
					strike_dt,
					created_dt,
					modified_dt
				FROM
					project_items
				WHERE
					project_id = {$this->project_id}
					{$where}";

        $result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

        $return = array();
		$i = 0;
        while ($row = $result->fetch_assoc()) {
			$return[$i] = $row;
			$return[$i]['item_id']       = (int)$row['item_id'];
			$return[$i]['project_id']    = (int)$row['project_id'];
			$return[$i]['item_priority'] = (int)$row['item_priority'];
			$return[$i]['created_by']    = (int)$row['created_by'];
			$return[$i]['modified_by']   = (int)$row['modified_by'];
			$return[$i]['assigned_to']   = (int)$row['assigned_to'];
			$return[$i]['strike']        = (int)$row['strike'];
			$return[$i]['item_text']     = strip_tags(htmlspecialchars_decode(stripslashes($row['item_text']), ENT_QUOTES));
			$return[$i]['due_dt']        = strtotime($row['due_dt']);
			$return[$i]['created_dt']    = strtotime($row['created_dt']);
			$return[$i]['modified_dt']   = strtotime($row['modified_dt']);
			$return[$i]['strike_dt']     = strtotime($row['strike_dt']);
            ++$i;
        }
        $result->close();

        return $return;
    }

	/**
     * read multiple records that the user should not be reading
     *
     * @param string $order order by field
     * @return array
     * @access public
     */
	function readRemovedItems($items) {
		if (count($items) == 0) {
			return false;
		}

		// query database
        $sql = "SELECT
					item_id
				FROM
					project_items
				WHERE
					project_id = {$this->project_id}";

		$result = $this->mysqli->query($sql);
        if (!$result) {
            catchSQL($sql, $this->mysqli);
            return false;
        }

		$all_items = array();
		$i = 0;
		while ($row = $result->fetch_assoc()) {
			array_push($all_items, (int)$row['item_id']);
            ++$i;
        }
		$result->close();

		$remove = array();
		foreach ($items as $id) {
			if (!in_array($id, $all_items, true) && $id != 0) {
				array_push($remove, $id);
			}
		}

        return $remove;
	}

    // UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * update an existing record
     *
     * @param integer $item_id
     * @param array $args
     * @return boolean
     * @access public
     */
    function updateItem($item_id, $args) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        $sql = "UPDATE
					project_items
				SET
					project_id = ?,
					item_text = ?,
					item_priority = ?,
					assigned_to = ?,
					created_by = ?,
					strike = ?,
					strike_dt = ?,
					due_dt = ?,
					created_dt = ?,
					modified_dt = ?
				WHERE
					item_id = ?";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('isiiiissssi', $this->project_id, $args['item_text'],
				$args['item_priority'], $this->account_id, $this->account_id,
				$args['strike'], $args['strike_dt'], $args['due_dt'],
				$args['created_dt'], $args['modified_dt'],
				$item_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        return true;
    }

	/**
     * mark a list item as completed or restore
     *
     * @param integer $item_id
     * @param boolean $strike
	 * @param string $strike_dt
     * @return boolean
     */
    function strikeItem($item_id, $strike, $strike_dt) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

		$strike_value = ($strike) ? 1 : 0;

        $sql = "UPDATE
					project_items
				SET
					strike = ?,
					strike_dt = ?,
					modified_dt = NOW()
				WHERE
					item_id = ?";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('isi', $strike_value, $strike_dt, $item_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        return true;
    }

	/**
     * mark a list item as completed or restore
     *
     * @param integer $item_id
     * @param boolean $strike
	 * @param string $strike_dt
     * @return boolean
     */
    function uncheckAll($project_id) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        $sql = "UPDATE
					project_items
				SET
					strike = 0,
					strike_dt = NULL,
					modified_dt = NOW()
				WHERE
					project_id = ?";

        if (!$stmt = $this->mysqli->prepare($sql)) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->bind_param('i', $project_id);
        if (!$stmt->execute()) {
            catchSQL($sql, $this->mysqli);
            return false;
        }
        $stmt->close();

        return true;
    }

    // DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * permanently removes a record
     *
     * @param integer $item_id
     * @return boolean
     * @access public
     */
    function removeItem($item_id) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        return $this->mysqli->row_delete('project_items', 'item_id', (int) $item_id);
    }

    /**
     * permanently removes a record
     *
     * @param integer $item_id
     * @return boolean
     * @access public
     */
    function removeAll($project_id) {
        // check permission
        if ($this->permission != 'WRITE' && $this->permission != 'ADMIN') {
            catchErr("Access to modify this project is denied.");
            return false;
        }

        return $this->mysqli->row_delete('project_items', 'project_id', (int) $project_id);
    }

}