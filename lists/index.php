<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.projectlist.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');

$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

// get the current account
$account_id = (int)$_SESSION['account_id'];

// get a list of projects for the current account
$project = new ProjectList($account_id);
$projectlist = $project->read();

// set the specified project or the last accessed project
$project_id = (!empty($_GET['id'])) ? (int)$_GET['id'] : $project->getLastAccess();

if ($project_id) {
	$project->setProject($project_id);
	if ($project->permission == 'NONE') {
		$project_id = 0;
	}
}

// get the information and checklist for the specified project
if (!empty($project_id)) {
	$projectinfo = $project->getFromArray($projectlist);
	list($itemlist, $itemcount, $completed, $priorities) = $project->readList();
	$peoplelist = $project->readPeople();
} else {
	$peoplelist = array();
	$itemlist   = array();
	$project_id = 0;
	$itemcount  = 0;
	$completed  = 0;
	$priorities = '[]';
}

// set the block of html to show on page load
$showHTML = "table";
if ($project_id == 0) {
	if (sizeOf($projectlist) == 0) {
		// show the nolists div
		$showHTML = "nolists";
	} else {
		// show the noselect div
		$showHTML = "noselect";
	}
} elseif ($itemcount == 0) {
	if ($project->permission == 'READ') {
		// show the read only div
		$showHTML = "readonly";
	} else {
		// show the noitems div
		$showHTML = "noitems";
	}
}

openPage('My Lysts'); ?>
<?php showBanner();?>
<div id="groups">
	<!-- projects -->
	<div class="group">
		<h3>LYSTS</h3>
		<div class="groupBtns">
			<button accesskey="l" class="button" onclick="menu.click(this, 1)" type="button"><span class="btn_options">&nbsp;</span></button>
			<ul class="menu" id="menu1">
<?php if ($project->permission == 'ADMIN') { ?>
				<li><a href="javascript:project.setedit()">Rename</a></li>
				<li><a href="javascript:project.remove()">Move to Trash</a></li>
				<li><a href="/transfer/">Import/Export</a></li>
<?php } else { ?>
				<li><a href="javascript:project.noaccess()">Remove from My Lysts</a></li>
<?php } ?>
				<li><a href="javascript:trash.show()">Open Trash...</a></li>
			</ul>
			<button class="button" onclick="project.setcreate()" type="button"><span class="btn_plus">&nbsp;</span></button>
		</div>
		<ul id="projects" class="groupItems">
<?php showProjects($projectlist, $project_id); ?>
		</ul>
	</div>
	<div class="groupBreak"></div>
	<!-- people -->
	<div class="group">
		<h3>PEOPLE</h3>
		<div class="groupBtns">
			<button accesskey="p" class="button" onclick="menu.click(this, 2)" type="button"><span class="btn_options">&nbsp;</span></button>
			<ul class="menu" id="menu2">
				<li><a href="/invite/">Invite New People</a></li>
				<li><a href="/manage/">Manage...</a></li>
			</ul>
			<a class="button" href="/invite/"><span class="btn_plus">&nbsp;</span></a>
		</div>
		<ul id="people" class="groupItems">
<?php
$owner = getValue('owner_id', 0, $projectinfo);
showListPeople($peoplelist, $owner);
?>
		</ul>
	</div>
</div>

<div id="content">
	<!-- <div id="filter"></div> -->
	<div id="bundle">
		<form id="listEdit" class="<?php echo ($showHTML == 'table') ? 'bg_white" ' : 'bg_clear';
			?>" action="javascript:list.edit()" autocomplete="off">
			<table id="list"<?php echo ($showHTML != 'table') ? 'class="hidden" ' : ''; ?>>
				<colgroup>
					<col class="col_grp"><col class="col_chk"><col><col class="col_due">
				</colgroup>
				<thead>
					<tr><th colspan="2" >Done</th><th>List Item</th><th>Due Date</th></tr>
				</thead>
				<tbody id="listbody">
<?php showListItems($itemlist, $project->permission); ?>
				</tbody>
			</table>
			<div id="guides">
				<!-- no lists -->
				<div id="nolists" class="startBox<?php echo ($showHTML != 'nolists') ? ' hidden' : ''; ?>">
					<img src="/images/arrow_left.png" width="44" height="47" alt="<" />
					<p>Click the add button in the LYSTS box to begin.</p>
				</div>
				<!-- no selection -->
				<div id="noselect" class="startBox<?php echo ($showHTML != 'noselect') ? ' hidden' : ''; ?>">
					<img src="/images/arrow_left.png" width="44" height="47" alt="<" />
					<p>To open a checklyst, select it's name in the LYSTS box.</p>
				</div>
				<!-- no items -->
				<div id="noitems" class="startBox<?php echo ($showHTML != 'noitems') ? ' hidden' : ''; ?>">
					<img src="/images/arrow_down.png" width="46" height="45" alt="" />
					<p>Click the "New" button and complete the form to create a list item.</p>
				</div>
				<!-- read only -->
				<div id="noitems" class="startBox<?php echo ($showHTML != 'readonly') ? ' hidden' : ''; ?>">
					<img src="/images/empty_box.png" width="45" height="45" alt="" />
					<p>This lyst is empty.<br /> As new items are created, they will appear here.</p>
				</div>
			</div>
			<div id="push"></div>
		</form>
		<form id="newitemForm" action="javascript:list.create()" autocomplete="off">
			<table id="newitem">
				<colgroup>
					<col class="col_bnk"><col><col class="col_due">
				</colgroup>
				<thead>
					<tr><th><th>List Item</th><th>Due Date</th></tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td><div class="edt_box"><textarea class="edt_text"  id="edt_text_new" name="txt" cols="20" rows="1"></textarea></div></td>
						<td><input class="edt_input" maxlength="18" name="due" type="text" /><div class="calButton"></div></td>
					</tr>
					<tr>
						<td class="toolbar_row" colspan="3">&nbsp;
							<button class="button" type="reset"><span class="action-l"><span class="action-r">Reset</span></span></button><button class="button" type="submit"><span class="default-l"><span class="default-r">Save</span></span></button>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div><!-- bundle -->
</div><!-- content -->
<div id="footer">
	<p class="status"><span id="total"><?php echo $itemcount ?></span> items, <span id="done"><?php echo $completed ?></span> completed</p>
<?php if ($project->permission == 'WRITE' || $project->permission == 'ADMIN') { ?>
	<button accesskey="n" class="button" id="toggleForm" type="button"><span class="action-l"><span class="action-r">New</span></span></button>
<?php } ?>
</div>
<div id="popBox">
	<a class="popClose" href="javascript:actions.hide()">&nbsp;</a>
	<div id="popContent">
		<div class="popClick" onclick="actions.prep()">Edit</div>
		<div class="popClick" onclick="actions.wipe()">Delete</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<?php if (DB_SERVER == 'localhost') { ?>
<script src="/js/calendar.js"></script>
<script src="/js/jquery.fluidtext.js"></script>
<script src="/js/jquery.mousereact.js"></script>
<script src="/js/jquery.tablednd.js"></script>
<script src="/js/project.js"></script>
<?php } else { ?>
<script src="/js/20110710.js"></script>
<?php } ?>
<script>
	project.id = <?php echo $project_id ?>;
	list.priorities = <?php echo $priorities ?>;
	$('#list').tableDnD({onDrop: function(drag, drop) { list.move(drag ,drop) }});

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-39092479-2', 'auto');
	ga('send', 'pageview');
</script>
<?php closePage(); ?>