<?php
require_once('/home1/checkly1/public_html/checklyst.php');
require_once('/home1/checkly1/public_html/library/fnc.handlers.php');

/**
 * output the doctype, header, and php errors
 *
 * @param string $title page title
 */
function openPage($title = "")
{
	if (empty($title)) {
		$title_tag = SITE_NAME;
	} else {
		$title_tag = SITE_NAME . " | " . $title;
	}

	// if available, use gzip to compress the page
	if (substr_count($_SERVER["HTTP_ACCEPT_ENCODING"], "gzip")) {
		ob_start("ob_gzhandler");
	} else {
		ob_start();
	}
?>
<!DOCTYPE html>
<head lang="en">
	<title><?php echo $title_tag ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="/templates/admin.css" />
<?php
}


/**
 * output the banner and navigation menu
 *
 * @param string $tab navigation tab title
 */
function showBanner($tab = "")
{
?>
</head>
<?php flush(); ?>
<body>
<?php writeErrors(); ?>
	<div id="wrapper">
		<div id="banner">
			<h1>Administration</h1>
<?php
	if (!empty($_SESSION['account_id'])) {
		echo "\t\t\t<a id=\"logout\" href=\"" . SITE_ROOT . "account/logout.php\">Logout</a>\n";
	}
?>
			<div id="nav">
				<a <?php
				if ($tab == "home") {
					echo "class=\"nav_current\" ";
				}?>href="<?php echo SITE_ROOT . "admin" ?>">Home</a>
				<a <?php
				if ($tab == "accounts") {
					echo "class=\"nav_current\" ";
				}?>href="<?php echo SITE_ROOT . "admin/accounts.php" ?>">Accounts</a>
				<a id="site_name" href="<?php echo SITE_ROOT ?>"><?php echo SITE_NAME ?></a>
			</div>
		</div>
<?php
}


/**
 * output the footer information
 *
 */
function showFooter()
{
?>
		<div id="footer">
			<p>Copyright &#169; <?php echo date("Y"); ?> Cone Code Development. All Rights Reserved.</p>
		</div>
	</div><!-- close wrapper -->
	<script type="text/javascript" src="/js/admin.min.js"></script>
<?php
}


/**
 * output the close body and html tags
 *
 */
function closePage()
{
?>
</body>
</html>
<?php
}
?>