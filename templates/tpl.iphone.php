<?php
//$browser = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
//if ($browser == true) {

require_once('/home1/checkly1/public_html/checklyst.php');
require_once('/home1/checkly1/public_html/library/fnc.handlers.php');
date_default_timezone_set("America/Los_Angeles");

/**
 * output the doctype and header
 *
 * @param string $pageTitle page title
 * @param string $pageDesc page description
 */
function openPage($pageTitle = '')
{
    if (empty($pageTitle)) {
        $pageTitle = SITE_NAME;
    }

    // if available, use gzip to compress the page
    if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
        ob_start('ob_gzhandler');
    } else {
        ob_start();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $pageTitle ?></title>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <meta charset="utf-8" />
    <link rel="stylesheet" href="http://checklyst.com/templates/iphone.css?version=<?php echo time(); ?>" />
</head>
<body>
<?php
    writeErrors();
}

/**
 * output the close body and html tags
 * 
 */
function closePage() {
?>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-39092479-2', 'auto');
	ga('send', 'pageview');
</script>
</body>
</html>
<?php
    // end compression
    ob_end_flush();
}
?>