<?php
require_once('/home1/checkly1/public_html/_settings/checklyst.php');
require_once('/home1/checkly1/public_html/library/fnc.handlers.php');
date_default_timezone_set("America/Los_Angeles");

/**
 * output the doctype and header
 *
 * @param string $pageTitle page title
 * @param string $pageDesc page description
 */
function openPage($pageTitle = '', $pageDesc = '', $toolStyle = false)
{
	if (empty($pageTitle)) {
		$pageTitle = SITE_NAME;
	}

	// if available, use gzip to compress the page
	if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
		ob_start('ob_gzhandler');
	} else {
		ob_start();
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $pageTitle ?></title>
    <meta name="apple-itunes-app" content="app-id=986959770, affiliate-data=, app-argument=">
	<meta charset="utf-8" />
<?php	if (!empty( $pageDesc)) { ?>
	<meta name="description" content="<?php echo $pageDesc ?>" />
<?php	}
		if (DB_SERVER == 'localhost') { ?>
	<link rel="stylesheet" href="/templates/styles.css" />
	<link rel="stylesheet" href="/templates/calendar.css" />
	<link rel="stylesheet" href="/templates/dialog.css" />
<?php	} else { ?>
	<link rel="stylesheet" href="/templates/20110710.css" />
<?php	}

}


/**
 * output the banner and navigation menu
 *
 * @param string $page 'LISTS'
 */
function showBanner($page = '')
{ ?>
</head>
<?php flush(); ?>
<body>
<?php echo writeErrors() ?>
	<div id="header">
		<a id="site-name" href="/">checklyst</a>
<?php if (!empty($_SESSION['account_status'])) { ?>
		<ul id="site-nav">
			<li><a href="javascript:settings.show()">Settings</a></li>
			<li><a href="/account/logout.php">Logout</a></li>
		</ul>
		<p id="todaysDate"><?php echo date('l, F j, Y', strtotime('now')) ?></p>
<?php } ?>
	</div>
<?php
}


/**
 * output a list of projects
 *
 * @param array $projects project list
 * @param integer $selected id of the current project
 */
function showProjects($projects, $selected)
{
	$count = sizeOf($projects);
	if ($count == 0) {
		echo "<li id=\"no_projects\">Not available</li>\n";
	}

	foreach ($projects as $project) {
		$id  = $project['project_id'];
		$class = ($id == $selected) ? ' class="menuSelect"' : '';
		echo '<li id="prj' . $id . '"', $class, '><a href="?id=' . $id . '">' .
				$project['project_name'] . "</a></li>\n";
	}
}


/**
 * output a list of people on the list page
 *
 * @param array $people
 * @param integer $owner
 * @param integer $selected
 */
function showListPeople($people, $owner, $selected = 0)
{
	$count = sizeOf($people);
	if ($count == 0) {
		echo "<li>Not available</li>\n";
	}

	foreach ($people as $person) {
		$id = $person['account_id'];
		if ($id == $owner) {
			$span = '<span class="owner">owner</span>';
		} else {
			$span = '<span class="access">' .
						strtolower($person['permission']) . '</span>';
		}

		$class = ($id == $selected) ? ' class="menuSelect"' : '';
		echo '<li', $class, '><p>',
				$person['display_name'], '</p>', $span, "</li>\n";
	}
}


/**
 * output a list of people on the manage page
 *
 * @param array $people
 * @param integer $omit
 * @param integer $selected
 */
function showManagePeople($people, $omit, $selected = 0)
{
	$count = sizeOf($people);
	if ($count == 0) {
		echo "<li>Not available</li>\n";
	}

	foreach ($people as $person) {
		$id = $person['account_id'];
		if ($id != $omit) {
			$class = ($id == $selected) ? ' class="menuSelect"' : '';
			echo '<li', $class, '><a href="/manage/person/', $id, '">',
					$person['display_name'], "</a></li>\n";
		}
	}
}


/**
 * output a list of invitations on the manage page
 *
 * @param array $people
 * @param integer $selected
 */
function showInvites($people, $selected)
{
	foreach ($people as $person) {
		$id = $person['invite_id'];
		$class = ($id == $selected) ? ' class="menuSelect"' : '';
		echo '<li', $class, '><a href="/manage/invite/', $id, '">',
				$person['name'], "</a></li>\n";
	}
}


/**
 * output the list items for a project
 *
 * @param array $list
 * @param boolean $permission the users project permission
 * @param boolean $include_tr include the table tr tag
 */
function showListItems($list, $permission = 'READ', $include_tr = true)
{
	$thisTime = strtotime('now');
	$thisDate = date('M d', $thisTime);
	$thisYear = date('Y', $thisTime);
	foreach ($list as $item) {
		// fix the due date
		if (empty($item['due_dt'])) {
			$dueDate = 'None';
		} else {
			$tmpDate = (date('M d, Y', $item['due_dt']));
			$dueDate = (substr($tmpDate, -4) == $thisYear) ? substr($tmpDate, 0, -6) : $tmpDate;
		}

		// mark striked item
		$rowClass = '';
		if ($item['strike'] == 1) {
			$rowClass = ' class="strike"';
		}

		// mark with notify symbol
		$notify = '';
		if (($dueDate != 'None') && ($item['due_dt'] < $thisTime)) {
			$notify = '<div class="urgent"></div>';
		}
		if ($dueDate === $thisDate) {
			$notify = '<div class="notify"></div>';
		}

		// draw the table row
		if ($include_tr) {
			echo '<tr', $rowClass, ' id="itm',  $item['item_id'], '">';
		}
		if ($permission == 'WRITE' || $permission == 'ADMIN') {
			echo '<td class="cell_drag"></td><td class="cell_check"><input type="hidden" value="', $item['strike'],
				'" /><button class="btn_checkbox" onclick="list.strike(this, ',
				$item['item_id'], ');" type="button"></button>', $notify,'</td>',
				'<td class="cell_text" onclick="actions.view(this, ',
				$item['item_id'], ')"><p>', $item['item_text'],
				'</p></td><td class="cell_due"><p>', $dueDate, '</p></td>';
		} else {
			echo '<td colspan="2"></td><td><p>', $item['item_text'],
				'</p></td><td class="cell_due"><p>', $dueDate, '</p></td>';
		}
		if ($include_tr) {
			echo "</tr>\n";
		}
	}
}


/**
 * output the footer information
 *
 */
function showFooter() {
?>
<div id="footer">
<?php	if ($page == 'LISTS') { ?>
			<li class="menuName"><a href="javascript:menu.click(4)" onmouseover="menu.over(4)">View</a>
				<ul class="menu" id="menu4">
					<li><a href="#">Priority</a></li>
					<li><a href="#">Images</a></li>
					<li><a href="#">Created By</a></li>
					<li><a href="#">Modified By</a></li>
					<li><a href="#">Assigned To</a></li>
					<li><a href="#">Due Date</a></li>
					<li><a href="#">Time Estimate</a></li>
					<li><a href="#">Created Date</a></li>
					<li><a href="#">Modified Date</a></li>
				</ul>
			</li>
<?php	} ?>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-39092479-2', 'auto');
	ga('send', 'pageview');
</script>
<?php
}


/**
 * output the close body and html tags
 * 
 */
function closePage() {
?>
</body>
</html>
<?php
	// end compression
	ob_end_flush();
}
?>