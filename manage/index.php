<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projects.php');
require_once('/home1/checkly1/public_html/library/cls.access.php');
require_once('/home1/checkly1/public_html/library/cls.invite.php');

$objAccount = new Account();
if (!$objAccount->confirmSession()) {
	header('Location: /');
}

// get my account id
$my_account_id = (int)$_SESSION['account_id'];

// create more class objects
$objAccess  = new Access();
$objInvite  = new Invite();
$objProject = new Projects($objAccount->account_id);


// PAGE FUNCTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * convert an invite permissions string into an array
 *
 * @param string $permissions
 * @return array
 */
function inviteAccess($permissions)
{
	$arrTmp = explode(',', $permissions);
	$access = array();

	foreach ($arrTmp as $strTmp) {
		$project_id = (int)substr($strTmp, 0, -2);
		switch ((int)substr($strTmp, -1)) {
			case 1:
				$access[$project_id] = 'READ';
				break;
			case 2:
				$access[$project_id] = 'WRITE';
				break;
			case 3:
				$access[$project_id] = 'ADMIN';
				break;
		}
	}

	return $access;
}

/**
 * output the form to select permissions
 *
 * @param array $my_projects
 * @param array $usr_access
 * @param integer $usr_account_id
 * @return array
 */
function showAccess(&$my_projects, &$usr_access, &$usr_account_id = 0)
{
	foreach ($my_projects as $project) {
		$project_id = $project['project_id'];
		$permission = getValue($project_id, '', $usr_access);
		echo '<tr><td><p class="projectName">', $project['project_name'],'</p>';

		if ($usr_account_id == $project['owner_id']) {
			echo '</td>', "\n", '<td class="accessNote" colspan="3">',
					'can\'t edit the project owners permission.</td>', "\n",
					'<td><input disabled="disabled" checked="checked" name="',
					$project_id, '" type="radio" /></td>';
		} else {
			echo '<input name="project[]" type="hidden" value="', $project_id, 
					'" /></td>', "\n", '<td><label><input ';
			if ($permission == '') {
				echo 'checked="checked" ';
			}
			echo 'name="', $project_id, '" type="radio" value="0" /></label></td>', "\n",
				'<td><label><input ';
			if ($permission == 'READ') {
				echo 'checked="checked" ';
			}
			echo 'name="', $project_id, '" type="radio" value="1" /></label></td>', "\n",
				'<td><label><input ';
			if ($permission == 'WRITE') {
				echo 'checked="checked" ';
			}
			echo 'name="', $project_id, '" type="radio" value="2" /></label></td>', "\n",
				'<td><label><input ';
			if ($permission == 'ADMIN') {
				echo 'checked="checked" ';
			}
			echo 'name="', $project_id, '" type="radio" value="3" /></label></td>';
		}
		echo "</tr>\n";
	}
}


// USER INFORMATION :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$usr_account_id = getValue('prsn', 0);

if ($usr_account_id) {

	// verify this user is linked to me
	if ($objAccess->checkLink($my_account_id, $usr_account_id)) {
		$linkToMe = TRUE;

		// get the users info
		$usr_account = new Account($usr_account_id);
		$usr_info = $usr_account->get();

		// get a list of permissions for the user
		$usr_access = $objAccess->readAccess($usr_account_id);

	} else {
		$linkToMe = FALSE;
	}
}


// INVITE INFORMATION :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$invite_id = getValue('invi', 0);

if ($invite_id) {

	// get the invite info
	$inv_info = $objInvite->getById($invite_id);

	// verify this person was invited by me
	if ($inv_info['created_by'] == $my_account_id) {
		$linkToMe = TRUE;

		// get a list of permissions for this invite
		$inv_access = inviteAccess($inv_info['permissions']);

	} else {
		$linkToMe = FALSE;
	}

}


// PROCESS FORM DATA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_POST) {
	if ($linkToMe && $_POST['formtype'] === 'RemoveUser') {
		// remove a people link
		if ($usr_account_id) {
			$objAccess->removeLink($my_account_id, $usr_account_id);
			$usr_account_id = 0;
		}
	}

	if ($linkToMe && $_POST['formtype'] === 'RemoveInvite') {
		// remove an invite
		if ($invite_id) {
			$objInvite->remove($invite_id);
			$invite_id = 0;
		}
	}

	if ($linkToMe && $_POST['formtype'] === 'EditUserAccess') {
		// get a list of my permissions
		$my_access = $objAccess->readAccess($my_account_id);

		// loop to modify the users permissions
		foreach($_POST['project'] as $project_id) {
			$radio = (int)getValue($project_id, -1);

			// verify my permission to edit this project
			if ($my_access[$project_id] === 'ADMIN') {
				$usr_prj_access = getValue($project_id, '', $usr_access);

				switch ($radio) {
					case 1:
						$permission = 'READ';
						break;
					case 2:
						$permission = 'WRITE';
						break;
					case 3:
						$permission = 'ADMIN';
						break;
					default:
						$permission = 'NONE';
				}

				// remove a permission
				if ($permission === 'NONE' && $usr_prj_access !== '') {
					$objAccess->remove($project_id, $usr_account_id);
				}

				// create or update a permission
				if ($permission !== 'NONE' && $usr_prj_access !== $permission) {
					if ($usr_prj_access === '') {
						$objAccess->create($permission, $project_id, $usr_account_id);
					} else {
						$objAccess->update($permission, $project_id, $usr_account_id);
					}
				}
			}
		} // end foreach

		// get an updated list of permissions for the user
		$usr_access = $objAccess->readAccess($usr_account_id);
	}

	if ($linkToMe && $_POST['formtype'] === 'EditInviteAccess') {
		// get a list of my permissions
		$my_access = $objAccess->readAccess($my_account_id);
		$tmpArray = array();

		// loop to modify the invitation permissions
		foreach($_POST['project'] as &$project_id) {
			$radio = (int)getValue($project_id, 0);

			// verify my permission to edit this project
			if ($my_access[$project_id] === 'ADMIN' && $radio > 0) {
				$tmpArray[$project_id] = $project_id . ':' . $radio;
			}
		}
		$permissions = implode(',', $tmpArray);

		// update the permissions for the invite
		$objInvite->update($invite_id, $permissions);

		// get an updated list of permissions for the invite
		$inv_info = $objInvite->getById($invite_id);
		$inv_access = inviteAccess($inv_info['permissions']);
	}
}


// MY INFORMATION :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// get a list of people i work with
$my_people = $objAccess->read($my_account_id);
$intPeople = sizeOf($my_people);

// get a list of my admin projects
$my_projects = $objProject->read('project_name', 'ADMIN');
$intProjects = sizeOf($my_projects);

// get a list of people i've invited
$my_invites = $objInvite->read($my_account_id);
$intInvites = sizeOf($my_invites);

openPage('Manage People'); ?>
<style>
#accessForm {
	float:left;
}
#clearance label {
	display:block;
}
#clearance {
	margin-bottom:16px;
}
#clearance td {
	border-bottom:1px solid #DDD;
	padding:8px 2px;
	text-align:center;
}
#clearance th {
	border-bottom:1px solid #DDD;
	font-size:12px;
	padding:2px;
	text-align:center;
}
#formInfo {
	background:#f2f8ff;
	float:left;
	padding:5px;
	font-size:12px;
	margin-left:23px;
	width:210px;
}
#formInfo dt {
	float:left;
	font-style:italic;
	margin-right:1em;
}
#formInfo dd {
	color:#666;
	margin-bottom:6px;
}

.accessNote {
	color:#CCC;
	font-size:12px
}
.colRad {
	width:90px;
}
.colPrj {
	width:160px;
}
.projectName {
	text-align:left;
}
.invitelink, .invitelink:visited {
	color:#2877D0;
	margin-left:24px;
}
#bundleAlt {
	background: #FFF;
	position: absolute; top: 24px; right: 0; bottom: 0; left: 0px;
	overflow-x:hidden;
	overflow-y:auto;
}
#bundleItems {
	padding:10px;
}
.itemDivide {
	border-bottom:1px solid #DDD;
	clear:both;
	height:0;
	margin:1em 0;
}
</style>
<?php
showBanner();
?>

<div id="groups">
	<!-- people -->
	<div class="group">
		<h3>PEOPLE</h3>
		<div class="groupBtns">
			<a class="button" href="/invite/"><span class="btn_plus">&nbsp;</span></a>
		</div>
		<ul id="people" class="groupItems">
<?php showManagePeople($my_people, $my_account_id, $usr_account_id); ?>
		</ul>
	</div>
<?php	if ($intInvites) { ?>
	<div class="groupBreak"></div>
	<!-- invitations -->
	<div class="group">
		<h3>INVITATIONS</h3>
		<ul id="invites" class="groupItems">
<?php showInvites($my_invites, $invite_id); ?>
		</ul>
	</div>
<?php	} ?>
</div>
<div id="content">
	<div id="filter">
		<a class="button" href="/lists/">
			<span class="action-l"><span class="action-r">My Lysts</span></span>
		</a>
	</div>
	<div id="bundleAlt">
		<div id="bundleItems">
			<h1><?php
		if ($usr_account_id) {
			echo getValue('display_name', 'View People', $usr_info);
		} else {
			echo getValue('name', 'View People', $inv_info);
		} ?></h1>
		<div class="itemDivide"></div>
<?php
	// display the invite status
	if ($invite_id && $linkToMe) { ?>
		<h2>Invite Status</h2>
		<p class="liteNote">This invitation has not yet been accepted.
				Invitation was sent on <?php echo date('F d, Y.', getValue('invite_dt', strtotime('now'), $inv_info)) ?></p>
		<div class="itemDivide"></div>
<?php
	}
	// user is selected and not linked to me
	if ($usr_account_id && !$linkToMe) { ?>
		<img src="/images/arrow_left.png" width="44" height="47" alt="Note:" />
		<p class="liteAnno">The user information is not available. Select a name on the left to edit permissions.</p>
<?php
	// invite is selected and not linked
	} elseif ($invite_id && !$linkToMe) { ?>
		<img src="/images/arrow_left.png" width="44" height="47" alt="Note:" />
		<p class="liteAnno">The invite information is not available. Select a name on the left to edit permissions.</p>
<?php
	// user is selected and projects exist
	} elseif (($usr_account_id || $invite_id) && $intProjects) { ?>
		<form id="accessForm" action="/manage/" method="post">
			<h2>Permissions</h2>
<?php	if ($usr_account_id) { ?>
			<table id="clearance">
				<colgroup><col class="colPrj"><col class="colRad"><col class="colRad"><col class="colRad"><col class="colRad"></colgroup>
				<thead><tr><th>Project</th><th>None</th><th>Read Only</th><th>Read & Write</th><th>Admin</th></tr></thead>
				<tbody>
<?php showAccess($my_projects, $usr_access, $usr_account_id); ?>
				</tbody>
			</table>
			<div>
				<input name="formtype" type="hidden" value="EditUserAccess" />
				<input name="prsn" type="hidden" value="<?php echo $usr_account_id ?>" />
				<button class="button" type="submit"><span class="default-l"><span class="default-r">Save Changes</span></span></button>
			</div>
<?php	} else { ?>
			<table id="clearance">
				<colgroup><col class="colPrj"><col class="colRad"><col class="colRad"><col class="colRad"><col class="colRad"></colgroup>
				<thead><tr><th>Project</th><th>None</th><th>Read Only</th><th>Read & Write</th><th>Admin</th></tr></thead>
				<tbody>
<?php showAccess($my_projects, $inv_access); ?>
				</tbody>
			</table>
			<div>
				<input name="formtype" type="hidden" value="EditInviteAccess" />
				<input name="invi" type="hidden" value="<?php echo $invite_id ?>" />
				<button class="button" type="submit"><span class="default-l"><span class="default-r">Save Changes</span></span></button>
			</div>
<?php	} ?>
		</form>
		<dl id="formInfo">
			<dt>None</dt>
			<dd>Choose None if you do not want this person to access your project. The project will be completely inaccessible and hidden from this person.</dd>
			<dt>Read Only</dt>
			<dd>Choose Read Only if you want this person to view your project, but you don't want them to be able to modify it. This person will not be able to create or edit list items.</dd>
			<dt>Read & Write</dt>
			<dd>Choose Read & Write if you want this person to edit your project. This person will be able to view your project, create new list items, and edit existing items.</dd>
			<dt>Admin</dt>
			<dd>Choose Admin if you want this person to control your project. This person will be able to view, edit, and delete the project, plus invite and set permissions for others.</dd>
		</dl>
		<div class="itemDivide"></div>
<?php
	// user or invite is selected and no projects exist
	} elseif (($usr_account_id || $invite_id) && $intProjects == 0) { ?>
		<img src="/images/icn_info.png" width="50" height="50" alt="Information:" />
		<p class="liteAnno">You don't currently own any projects to set permissions.</p>
<?php
	// user is not selected and there are people to select
	} elseif ($usr_account_id == 0 && $intPeople) { ?>
		<img src="/images/arrow_left.png" width="44" height="47" alt="<" />
		<p class="liteAnno">Select a name on the left to edit permissions.</p>
<?php
	// there are no users to select
	} else { ?>
		<img src="/images/arrow_left.png" width="44" height="47" alt="<" />
		<p class="liteAnno">You don't currently have any links to other people.<br/>
			Click the invite link to invite people to your projects.</p>
<?php
	}
	// remove a user or an invitation
	if ($usr_account_id && $linkToMe) { ?>
		<h2>Remove</h2>
		<form action="/manage/" method="post" onsubmit="return verifyRemoveUser()">
			<p class="liteNote">Click the button below to remove this person from your list of people.</p>
			<div>
				<input name="formtype" type="hidden" value="RemoveUser" />
				<input name="prsn" type="hidden" value="<?php echo $usr_account_id ?>" />
				<button class="button" type="submit"><span class="default-l"><span class="default-r">Remove This Person</span></span></button>
			</div>
		</form>
<?php
	} elseif ($invite_id && $linkToMe) { ?>
		<h2>Remove</h2>
		<form action="/manage/" method="post" onsubmit="return verifyRemoveInvite()">
			<p class="liteNote">Click the button below to remove the invitation to this person.</p>
			<br/>
			<div>
				<input name="formtype" type="hidden" value="RemoveInvite" />
				<input name="invi" type="hidden" value="<?php echo $invite_id ?>" />
				<button class="button" type="submit"><span class="default-l"><span class="default-r">Remove This Invite</span></span></button>
			</div>
		</form>
<?php
	} ?>
		</div><!-- bundle items -->
	</div><!-- bundle -->
</div><!-- content -->
<div id="footer"></div>
<script>
function verifyRemoveUser() {
	return confirm('Are you sure you want to remove this person?');
}
function verifyRemoveInvite() {
	return confirm('Are you sure you want to remove this invitation?');
}
</script>
<?php closePage(); ?>