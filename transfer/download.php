<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projectlist.php');

$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

// get the current account
$account_id = (int)$_SESSION['account_id'];

// get the specified project id
$project_id = getValue('id');

if ($project_id) {
	$project = new ProjectList($account_id, $project_id);
	if ($project->permission == 'NONE') {
		$project_id = 0;
	}
}

// get the checklist for the current project
if (!empty($project_id)) {
	$projectinfo = $project->get();
	list($checklist, $listcount) = $project->readList();
} else {
	$projectinfo = array();
	$checklist  = array();
	$listcount  = 0;
}

$downloadfile = str_replace(' ', '_', getValue('project_name', 'unknown', $projectinfo));

// build headers
header("content-type: plain/text");
header("Content-disposition: attachment; filename=$downloadfile.csv");

// output the column titles
echo "\"Priority\",\"Done\",\"List Item\",\"Due Date\",\"Completed Date\",\"Created Date\",\"Modified Date\"\n";

// loop though the project
$thisYear = date('Y', strtotime('now'));
foreach ($checklist as $item) {
	// fix the due date format
	if (empty($item['due_dt'])) {
		$item['due_dt'] = '"None"';
	} else {
		$item['due_dt'] = (date('Y-m-d', $item['due_dt']));
	}

	// fix the quotes, carriage returns, and HTML tags
	$item['item_text'] = str_replace('&quot;', '""', $item['item_text']);
	$item['item_text'] = strip_tags($item['item_text']);
	$item['item_text'] = html_entity_decode($item['item_text'], ENT_QUOTES);

	// output the new line
	echo $item['item_priority'], ",", $item['strike'], ",\"", $item['item_text'], "\",",
			$item['due_dt'], ",", $item['strike_dt'], ",", $item['created_dt'], ",",
			$item['modified_dt'], "\n";
}
