<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projects.php');

function uploadErrorMsg($error_code) {
    switch ($error_code) {
        case UPLOAD_ERR_INI_SIZE:
            return "The file exceeds the max file size for the server.";
        case UPLOAD_ERR_FORM_SIZE:
            return "The file exceeds the max file size specified in the form.";
        case UPLOAD_ERR_PARTIAL:
            return "The uploaded file was only partially uploaded.";
        case UPLOAD_ERR_NO_FILE:
            return "No file was uploaded.";
        case UPLOAD_ERR_NO_TMP_DIR:
            return "Missing a temporary folder.";
        case UPLOAD_ERR_CANT_WRITE:
            return "Failed to write file to disk.";
        case UPLOAD_ERR_EXTENSION:
            return "File upload stopped by extension.";
        default:
            return "Error uploading file: " . $_FILES["file"]["error"];
    }
}

function fixDate($date, $time = true)
{
	if (strtoupper($date) == 'NONE' || empty($date)) {
		return '';
	} else {
		$tmp_time = strtotime($date);
		if ($tmp_time) {
			if ($time) {
				return date('Y-m-d G:i:s', strtotime($date));
			} else {
				return date('Y-m-d', strtotime($date));
			}
		} else {
			return '';
		}
	}
}

$formOk = TRUE;
if ($_FILES["file"]["error"] != UPLOAD_ERR_OK) {
	$error_message = uploadErrorMsg($_FILES['file']['error']);
	catchErr($error_message);
	$formOk = FALSE;
} else {
	// create a name for the new file
	$name  = substr(uniqid(mt_rand()), 0, 8);
	$parts = pathinfo($_FILES["file"]["name"]);
	$ext   = "." . strtolower($parts["extension"]);

	if ($ext === '.csv') {
		// give the file a new name and move it into position
		$filename = BASE_DIR . "transfer/temp/{$name}{$ext}";
		move_uploaded_file($_FILES["file"]["tmp_name"], $filename);
	} else {
	 catchErr('Invalid file type.');
		$formOk = FALSE;
	}
}


$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

// get the current account
$account_id = (int)$_SESSION['account_id'];

$project_id  = 0;
$file_handle = FALSE;

// get the specified project id	and open the import file
if ($formOk) {
	$project_id = getValue('id');

	$file_handle = fopen($filename, 'r');
}

if ($project_id) {

	// check the permission
	$project = new Projects($account_id);
	$project->setProject($project_id);
	if ($project->permission == 'NONE' || $project->permission == 'READ') {
		$project_id = 0;
	}
} else {
	catchErr('A project identifier could not be acquired.');
}

// get the column titles
if ($file_handle !== FALSE) {
	$data = fgetcsv($file_handle, 1000, ",");
} else {
	catchErr('Import was unable to open the uploaded file.');
	$file_handle = FALSE;
	$data = array();
}

// convert the columns to MySQL field names
$columns = array();
$colType = array();

for ($i = 0,$count = count($data); $i < $count; $i++) {
	switch (strtoupper($data[$i])) {
		case 'PRIORITY':
			$columns[$i] = 'item_priority';
			$colType[$i] = 'integer';
			break;
		case 'DONE':
			$columns[$i] = 'strike';
			$colType[$i] = 'integer';
			break;
		case 'LIST ITEM':
			$columns[$i] = 'item_text';
			$colType[$i] = 'string';
			break;
		case 'DUE DATE':
			$columns[$i] = 'due_dt';
			$colType[$i] = 'date';
			break;
		case 'COMPLETED DATE':
			$columns[$i] = 'strike_dt';
			$colType[$i] = 'datetime';
			break;
		case 'CREATED DATE':
			$columns[$i] = 'created_dt';
			$colType[$i] = 'datetime';
			break;
		case 'MODIFIED DATE':
			$columns[$i] = 'modified_dt';
			$colType[$i] = 'datetime';
			break;
		default:
			break;
	}
}
$colCount = count($columns);

// set the default values
$now = date(strtotime('now'));
$defaults = array(
		'item_priority'	=> 1,
		'strike'		=> 0,
		'item_text'		=> '',
		'due_dt'		=> '',
		'strike_dt'		=> $now,
		'create_dt'		=> $now,
		'modified_dt'	=> $now);

// set the MySQL query
$sql = "INSERT INTO project_items (
			project_id,
			item_priority,
			strike,
			item_text,
			due_dt,
			strike_dt,
			created_dt,
			modified_dt
		) values (
			{$project_id},
			?, ?, ?, ?, ?, ?, ?)";

$html = '';

if ($colCount > 0) {
	ob_start();

	$mysqli = new Mysqli_Database();
	if (!$stmt = $mysqli->prepare($sql)) {
		catchSQL($sql, $mysqli);
	}

	// loop though the csv file'
	while (($data = fgetcsv($file_handle, 1000)) !== FALSE) {
		$i = 0;
		$values = $defaults;
		foreach ($data as $item) {

			// get the value to be inserted into the database
			if (!empty($colType[$i])) {
				if ($colType[$i] == 'date') {
					$values[$columns[$i]] = fixDate($item, false);
				} elseif ($colType[$i] == 'datetime') {
					$values[$columns[$i]] = fixDate($item, true);
				} elseif ($colType[$i] == 'string') {
					$values[$columns[$i]] = cleanString($item);
				} else {
					$values[$columns[$i]] = (int)$item;
				}
			}

			// display the line being read
			echo $values[$columns[$i]]. ", ";

			$defaults['item_priority']++;

			$i++;
		}
		echo "<br />\n";

		// insert the new item in to the database
		$stmt->bind_param('iisssss', $values['item_priority'], $values['strike'],
				$values['item_text'], $values['due_dt'], $values['strike_dt'],
				$values['create_dt'], $values['modified_dt']);
		if (!$stmt->execute()) {
			catchSQL($sql, $mysqli);
		}
	}
	$stmt->close();

	echo '<br/><p>import complete.</p><br/><a href=/lists/?id=',
			$project_id,'>Open This Project</a>';

	$html = ob_get_contents();
	ob_end_clean();
} else {
	catchErr('An error occurred while importing the file.');
}

// close the open file
if ($file_handle !== FALSE) {
	fclose($file_handle);
}

// remove the existing file if necessary
if ($formOk) {
	if (file_exists($filename)) {
		unlink($filename);
	}
}

// output results
openPage('My Lists');
showBanner(); ?>
<div class="whiteBox">
	<h2 class="whiteBoxTitle">Import Content</h2>
	<?php echo $html; ?>
</div>
<?php
showFooter();
closePage();
?>