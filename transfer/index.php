<?php
require_once('/home1/checkly1/public_html/templates/tpl.default.php');
require_once('/home1/checkly1/public_html/library/cls.account.php');
require_once('/home1/checkly1/public_html/library/cls.projectlist.php');

$account = new Account();
if (!$account->confirmSession()) {
	header('Location: /');
}

// get the current account
$account_id = (int)$_SESSION['account_id'];

// get a list of projects for the current account
$project = new ProjectList($account_id);
$projectlist = $project->read();

// set the specified action
$action = getValue('act', 'import');

// set the specified project or the last accessed project
$project_id = (!empty($_GET['id'])) ? (int)$_GET['id'] : $project->getLastAccess();

if ($project_id) {
	$project->setProject($project_id);
	if ($project->permission == 'NONE') {
		$project_id = 0;
	}
}

// get the information for the specified project
if (!empty($project_id)) {
	$projectinfo = $project->getFromArray($projectlist);
	$itemCount = $project->totalItems();
} else {
	$itemCount = 0;
}

$projectName = getValue('project_name', '', $projectinfo);

openPage('My Lists'); ?>
<style>
#tab-bar {
	border-bottom: 1px solid #A3A3A3;
	height:23px;
}
#tab-bar a {
	color:#333;
	display:block;
	float:left;
	font-size:14px;
	height:17px;
	margin:1px;
	padding:3px 0;
	text-align:center;
	text-decoration:none;
	width:66px;
}
.selected {
	background:url('/images/tab_bg.png');
}
#download {
	background:url('/images/download_btn.png');
	color:#FFF;
	display:block;
	font-size:14px;
	height:16px;
	margin:10px 14px;
	padding:6px 11px;
	text-align:right;
	text-decoration:none;
	width:110px;
}
#uploadForm {
	margin:10px 14px;
}
</style>
<?php showBanner();?>
<div id="groups">
	<!-- projects -->
	<div class="group">
		<h3>LYSTS</h3>
		<ul id="projects" class="groupItems">
<?php
$count = sizeOf($projectlist);
if ($count === 0) {
	echo "<li id=\"no_projects\">Not available</li>\n";
}

foreach ($projectlist as $thisProj) {
	$id  = $thisProj['project_id'];
	if ($thisProj['project_id'] == $project_id) {
		echo '<li id="prj' . $id . '" class="menuSelect"><a href="?id=' . $id . 
				'&act=' . $action . '">' . $thisProj['project_name'] . "</a></li>\n";
	} else {
		echo '<li id="prj' . $id . '"><a href="?id=' . $id . '&act=' . $action . '">' .
				$thisProj['project_name'] . "</a></li>\n";
	}
}
?>
		</ul>
	</div>
</div>
<div id="content">
	<div id="filter">
		<a class="button" href="/lists/">
			<span class="action-l"><span class="action-r">My Lysts</span></span>
		</a>
	</div>
	<div id="bundleAlt">
		<div id="bundleItems">
			<h1>Transfer</h1>
<?php
if ($count === 0) { ?>
			<img src="/images/arrow_left.png" width="44" height="47" alt="<" />
			<p class="liteNote">Import and Export can only be applied to existing projects.</p>
			<p class="liteNote">Return the My Lists page to create a new project before continuing.</p>
<?php
// start export action
} elseif ($action === 'export') { ?>
			<div id="tab-bar">
				<a href="/transfer/?id=<?php echo $project_id ?>&act=import">Import</a>
				<a class="selected" href="/transfer/?id=<?php echo $project_id ?>&act=export">Export</a>
			</div>
<?php	if ($itemCount === 0) { ?>
			<br class="clear"/>
			<img src="/images/icn_info.png" width="50" height="50" alt="Information:" />
			<p class="liteAnno">The project "<?php echo $projectName ?>" has no list items to export.</p>
<?php	} else { ?>
			<p class="liteAnno">Click the download button to export "<?php echo $projectName ?>" to a comma delimited CSV file.</p>
			<a id="download" href="download.php?id=<?php echo $project_id ?>">Download File</a>
<?php
		}
// start import action
} else { ?>
			<div id="tab-bar">
				<a class="selected" href="/transfer/?id=<?php echo $project_id ?>&act=import">Import</a>
				<a href="/transfer/?id=<?php echo $project_id ?>&act=export">Export</a>
			</div>
<?php	if ($project->permission === 'NONE') { ?>
			<br class="clear"/>
			<img src="/images/arrow_left.png" width="44" height="47" alt="<" />
			<p class="liteAnno">To import items into a project, make a selection from the project list.</p>
<?php	} elseif ($project->permission === 'READ') { ?>
			<br class="clear"/>
			<img src="/images/icn_info.png" width="50" height="50" alt="Information:" />
			<p class="liteAnno">Your access to "<?php echo $projectName ?>" is limited to Read-Only.</p>
<?php	} else { ?>
			<p class="liteAnno">Choose a comma delimited CSV file to import into "<?php echo $projectName ?>".</p>
			<form id="uploadForm" action="upload.php" enctype="multipart/form-data" method="post">
				<div>
					<input name="MAX_FILE_SIZE" type="hidden" value="10000" />
					<input name="id" type="hidden" value="<?php echo $project_id ?>" />
					<input name="file" type="file" />
				</div>
				<div class="buttongrp">
					<br class="clear" />
					<button class="button" type="submit"><span class="default-l"><span class="default-r">Upload</span></span></button>
				</div>
			</form>
<?php	}
} ?>
		</div><!-- bundle items -->
	</div><!-- bundle -->
</div><!-- content -->
<div id="footer"></div>
<?php closePage(); ?>